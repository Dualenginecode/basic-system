<?php

use think\admin\extend\PhinxExtend;
use think\admin\model\SysDict;
use think\admin\model\SysConfig;
use think\admin\model\SysMenu;
use think\admin\model\SysUser;
use think\admin\service\ProcessService;
use think\helper\Str;
use think\migration\Migrator;

@set_time_limit(0);
@ini_set('memory_limit', -1);

/**
 * 数据安装包
 * @class InstallPackage
 */
class InstallPackage extends Migrator
{
    /**
     * 数据库初始化
     * @return void
     * @throws \think\db\exception\DbException
     */
    public function change()
    {
        $this->inserData();
        $this->insertUser();
        $this->insertMenu();
        $this->insertConfig();
        $this->insertDict();
    }

    /**
     * 安装扩展数据
     * @return void
     * @throws \think\db\exception\DbException
     */
    private function inserData()
    {
        // 待解析处理数据
        $json = '{
    "sys_code_rule": "20009999999999/sys_code_rule.data",
    "sys_config": "20009999999999/sys_config.data",
    "sys_data": "20009999999999/sys_data.data",
    "sys_dict": "20009999999999/sys_dict.data",
    "sys_feedback": "20009999999999/sys_feedback.data",
    "sys_file": "20009999999999/sys_file.data",
    "sys_file_group": "20009999999999/sys_file_group.data",
    "sys_gas_logs": "20009999999999/sys_gas_logs.data",
    "sys_gpc": "20009999999999/sys_gpc.data",
    "sys_industry": "20009999999999/sys_industry.data",
    "sys_menu": "20009999999999/sys_menu.data",
    "sys_merchant": "20009999999999/sys_merchant.data",
    "sys_mobile_menu": "20009999999999/sys_mobile_menu.data",
    "sys_module": "20009999999999/sys_module.data",
    "sys_oplog": "20009999999999/sys_oplog.data",
    "sys_package": "20009999999999/sys_package.data",
    "sys_public_category": "20009999999999/sys_public_category.data",
    "sys_queue": "20009999999999/sys_queue.data",
    "sys_region": "20009999999999/sys_region.data",
    "sys_relation": "20009999999999/sys_relation.data",
    "sys_robot": "20009999999999/sys_robot.data",
    "sys_role": "20009999999999/sys_role.data",
    "sys_sms": "20009999999999/sys_sms.data",
    "sys_spa": "20009999999999/sys_spa.data",
    "sys_standard_code": "20009999999999/sys_standard_code.data",
    "sys_tenant": "20009999999999/sys_tenant.data",
    "sys_user": "20009999999999/sys_user.data",
    "sys_world_code": "20009999999999/sys_world_code.data"
}';
        // 解析并写入扩展数据
        if (is_array($tables = json_decode($json, true)) && count($tables) > 0) {
            foreach ($tables as $table => $path) if (($model = m($table))->count() < 1) {
                $name = Str::studly($table);
                ProcessService::message(" -- Starting write {$table} table ..." . PHP_EOL);
                [$ls, $rs, $fp] = [0, [], fopen(__DIR__ . DIRECTORY_SEPARATOR . $path, 'r+')];
                while (!feof($fp)) {
                    if (empty($text = trim(fgets($fp)))) continue; else $ls++;
                    if (is_array($rw = json_decode($text, true))) $rs[] = $rw;
                    if (count($rs) > 100) [, $rs] = [$model->strict(false)->insertAll($rs), []];
                    ProcessService::message(" -- -- {$name}:{$ls}", 1);
                }
                count($rs) > 0 && $model->strict(false)->insertAll($rs);
                ProcessService::message(" -- Finished write {$table} table, Total {$ls} rows.", 2);
            }
        }
    }

    /**
         * 初始化字典参数
         * @return void
         * @throws \think\db\exception\DbException
         */
        private function insertDict()
        {
            $modal = SysDict::mk()->whereRaw('1=1')->findOrEmpty();
            $modal->isEmpty() && $modal->insertAll([
                [
               "id" => 10000,
               "category" => "FRM",
               "sort" => 1,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" => "用户性别类型",
               "dictValue" => "GENDER",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10001,
               "category" => "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10000,
               "dictLabel" => "男",
               "dictValue" => "男",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10002,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10000,
               "dictLabel" =>  "女",
               "dictValue" =>  "女",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10003,
               "category" =>  "FRM",
               "sort" => 2,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" => "系统菜单类型",
               "dictValue" =>  "MENU_TYPE",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10004,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10003,
               "dictLabel" =>  "目录",
               "dictValue" =>  "CATALOG",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10005,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10003,
               "dictLabel" =>  "菜单",
               "dictValue" =>  "MENU",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10006,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10003,
               "dictLabel" =>  "内链",
               "dictValue" =>  "IFRAME",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10007,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10003,
               "dictLabel" =>  "外链",
               "dictValue" =>  "LINK",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10008,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "系统通用状态",
               "dictValue" =>  "COMMON_STATUS",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10009,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10008,
               "dictLabel" => "启用",
               "dictValue" => "0",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10010,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10008,
               "dictLabel" => "停用",
               "dictValue" => "1",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10011,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" => "系统角色分类",
               "dictValue" => "ROLE_CATEGORY",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10012,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10011,
               "dictLabel" => "全局",
               "dictValue" => "GLOBAL",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10013,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10011,
               "dictLabel" => "商户",
               "dictValue" => "BIZ",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10078,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "登录设备类型",
               "dictValue" => "AUTH_DEVICE_TYPE",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10079,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10078,
               "dictLabel" => "PC网页端",
               "dictValue" => "WEB",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10080,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10078,
               "dictLabel" => "移动网页端",
               "dictValue" => "WAP",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10081,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10078,
               "dictLabel" =>  "微信小程序",
               "dictValue" =>  "WXAPP",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10082,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10078,
               "dictLabel" =>  "支付宝小程序",
               "dictValue" => "ALIPAYMINI",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10083,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10078,
               "dictLabel" =>  "微信端",
               "dictValue" =>  "WECHAT",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10084,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10078,
               "dictLabel" =>  "苹果",
               "dictValue" =>  "IOSAPP",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10085,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10078,
               "dictLabel" =>  "安卓",
               "dictValue" =>  "ANDROID",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10086,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "系统字典分类",
               "dictValue" =>  "DICT_CATEGORY",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10087,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10086,
               "dictLabel" =>  "框架",
               "dictValue" =>  "FRM",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10088,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10086,
               "dictLabel" =>  "业务",
               "dictValue" =>  "BIZ",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10089,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "短信发送引擎",
               "dictValue" => "SMS_ENGINE",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10090,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10089,
               "dictLabel" =>  "阿里云",
               "dictValue" =>  "ALIYUN",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10091,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10089,
               "dictLabel" =>  "腾讯云",
               "dictValue" =>  "TENCENT",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10092,
               "category" =>  "FRM",
               "sort" => 11,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "文件上传引擎",
               "dictValue" => "FILE_ENGINE",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10093,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10092,
               "dictLabel" =>  "本地",
               "dictValue" =>  "LOCAL",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10094,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10092,
               "dictLabel" =>  "阿里云",
               "dictValue" =>  "ALIYUN",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10095,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10092,
               "dictLabel" =>  "腾讯云",
               "dictValue" =>  "TENCENT",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10096,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10092,
               "dictLabel" =>  "七牛云",
               "dictValue" =>  "QINIU",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10097,
               "category" =>  "FRM",
               "sort" => 12,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "邮件发送引擎",
               "dictValue" => "EMAIL_ENGINE",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10098,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10097,
               "dictLabel" =>  "本地",
               "dictValue" =>  "LOCAL",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10099,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10097,
               "dictLabel" =>  "阿里云",
               "dictValue" =>  "ALIYUN",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10100,
               "category" =>  "FRM",
               "sort" => 0,
               "status" => 0,
               "parentId" => 10097,
               "dictLabel" =>  "腾讯云",
               "dictValue" =>  "TENCENT",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10101,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "系统通用开关",
               "dictValue" =>  "COMMON_SWITCH",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10102,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10101,
               "dictLabel" =>  "开",
               "dictValue" =>  "true",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10103,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10101,
               "dictLabel" =>  "关",
               "dictValue" =>  "false",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10118,
               "category" =>  "FRM",
               "sort" => 16,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "定时任务分类",
               "dictValue" => "JOB_CATEGORY",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10119,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10118,
               "dictLabel" =>  "框架",
               "dictValue" =>  "FRM",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10120,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10118,
               "dictLabel" =>  "业务",
               "dictValue" =>  "BIZ",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10121,
               "category" =>  "FRM",
               "sort" => 17,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "定时任务状态",
               "dictValue" => "JOB_STATUS",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10122,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10121,
               "dictLabel" =>  "运行",
               "dictValue" =>  "RUNNING",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10123,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10121,
               "dictLabel" =>  "停止",
               "dictValue" =>  "STOPPED",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10124,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "三方用户分类",
               "dictValue" => "THIRD_CATEGORY",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10125,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10124,
               "dictLabel" => "支付宝APIPAY",
               "dictValue" =>  "APIPAY",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10126,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10124,
               "dictLabel" => "微信WECHAT",
               "dictValue" =>  "WECHAT",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10127,
               "category" =>  "FRM",
               "sort" => 19,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "系统消息类型",
               "dictValue" => "MESSAGE_CATEGORY",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10128,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 10127,
               "dictLabel" =>  "系统",
               "dictValue" =>  "SYS",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10129,
               "category" =>  "FRM",
               "sort" => 20,
               "status" => 0,
               "parentId" => 10127,
               "dictLabel" =>  "业务",
               "dictValue" =>  "BIZ",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10130,
               "category" =>  "FRM",
               "sort" => 10,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "移动菜单状态",
               "dictValue" =>  "MOBILE_STATUS",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10131,
               "category" =>  "FRM",
               "sort" => 4,
               "status" => 0,
               "parentId" => 10130,
               "dictLabel" =>  "可用",
               "dictValue" =>  "ENABLE",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10132,
               "category" =>  "FRM",
               "sort" => 6,
               "status" => 0,
               "parentId" => 10130,
               "dictLabel" =>  "不可用",
               "dictValue" =>  "DISABLED",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10141,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "SSL证书类型",
               "dictValue" =>  "SSL_TYPE",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-22 15:45:20",
               "createdBy" => 10000
              ],
              [
               "id" => 10142,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10141,
               "dictLabel" =>  "暂不使用证书",
               "dictValue" =>  "none",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-22 15:45:56",
               "createdBy" => 10000
              ],
              [
               "id" => 10143,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10141,
               "dictLabel" =>  "PEM证书",
               "dictValue" =>  "pem",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-22 15:46:20",
               "createdBy" => 10000
              ],
              [
               "id" => 10144,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10141,
               "dictLabel" =>  "P12证书",
               "dictValue" =>  "p12",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-22 15:46:43",
               "createdBy" => 10000
              ],
              [
               "id" => 10145,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "文件命名方式",
               "dictValue" =>  "FILENAME_TYPE",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:33:37",
               "createdBy" => 10000
              ],
              [
               "id" => 10146,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10145,
               "dictLabel" =>  "文件哈希值",
               "dictValue" =>  "xmd5",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:34:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10147,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10145,
               "dictLabel" =>  "日期+随机",
               "dictValue" =>  "date",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:34:39",
               "createdBy" => 10000
              ],
              [
               "id" => 10148,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "文件链接类型",
               "dictValue" =>  "LINK_TYPE",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:35:52",
               "createdBy" => 10000
              ],
              [
               "id" => 10149,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10148,
               "dictLabel" => "简洁链接",
               "dictValue" =>  "none",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:36:12",
               "createdBy" => 10000
              ],
              [
               "id" => 10150,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10148,
               "dictLabel" => "完整链接",
               "dictValue" =>  "full",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:36:28",
               "createdBy" => 10000
              ],
              [
               "id" => 10151,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10148,
               "dictLabel" => "简洁并压缩图片",
               "dictValue" =>  "none+compress",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:36:51",
               "createdBy" => 10000
              ],
              [
               "id" => 10152,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10148,
               "dictLabel" => "完整并压缩图片",
               "dictValue" =>  "full+compress",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:37:16",
               "createdBy" => 10000
              ],
              [
               "id" => 10153,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" =>  "本地访问协议",
               "dictValue" =>  "LOCAL_HTTP_PROTOCOL",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:44:09",
               "createdBy" => 10000
              ],
              [
               "id" => 10154,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10153,
               "dictLabel" =>  "FOLLOW",
               "dictValue" =>  "follow",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:44:33",
               "createdBy" => 10000
              ],
              [
               "id" => 10155,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10153,
               "dictLabel" =>  "HTTP",
               "dictValue" =>  "http",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:44:49",
               "createdBy" => 10000
              ],
              [
               "id" => 10156,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10153,
               "dictLabel" => "HTTPS",
               "dictValue" => "https",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:45:04",
               "createdBy" => 10000
              ],
              [
               "id" => 10157,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10153,
               "dictLabel" => "PATH",
               "dictValue" => "path",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:45:20",
               "createdBy" => 10000
              ],
              [
               "id" => 10158,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10153,
               "dictLabel" => "AUTO",
               "dictValue" => "auto",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-24 13:45:35",
               "createdBy" => 10000
              ],
              [
               "id" => 10159,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 0,
               "dictLabel" => "云端访问协议",
               "dictValue" => "CLOUD_HTTP_PROTOCOL",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" => "2023-04-24 13:44:09",
               "createdBy" => 10000
              ],
              [
               "id" => 10160,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10159,
               "dictLabel" =>  "HTTP",
               "dictValue" =>  "http",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-25 12:11:04",
               "createdBy" => 10000
              ],
              [
               "id" => 10161,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10159,
               "dictLabel" =>  "HTTPS",
               "dictValue" =>  "https",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-25 12:11:17",
               "createdBy" => 10000
              ],
              [
               "id" => 10162,
               "category" =>  "FRM",
               "sort" => 9,
               "status" => 0,
               "parentId" => 10159,
               "dictLabel" =>  "AUTO",
               "dictValue" =>  "auto",
               "extJson" => NULL,
               "isDeleted" => 0,
               "createTime" =>  "2023-04-25 12:11:46",
               "createdBy" => 10000
              ]
            ]);
        }

        /**
             * 初始化配置参数
             * @return void
             * @throws \think\db\exception\DbException
             */
            private function insertConfig()
            {
                $modal = SysConfig::mk()->whereRaw('1=1')->findOrEmpty();
                $modal->isEmpty() && $modal->insertAll([
                     ["id"=> 10000,"category"=> "SYS_BASE","remark"=> "系统LOGO","sort"=> 1,"status"=> 0,"configKey"=> "SQM_SYS_LOGO","configValue"=> "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAACXBIWXMAAAsTAAALEwEAmpwYAABHp0lEQVR4nO29d5xlR3nn/X2qzrmp03RPHs1ogkbSKI5yAGUMEiIZDBhsbLy2wcu+2J/X62UX73px9trsmsXGa7+sjTPRRshEAUYoIQQIZY1GGo0mx87dN55QtX/USfd290xPkMD7uj6f2/f2iXWe+HueeqqOcHraRqXU1Vp7V2qtz1NKrVFKjYBURVCn6R7/f23WWtsxxk5ba47EcbQ9juPvx3H8ELANMKdycTmFc1dord/k+6W3lEqla5TSNaX+ldcvRbPWEsdxHEXhk0EQ3BlF4aeAZ0/mWicjAGs8z39vpVL5N75fWiUiWaf+tS2iCXAaSJXTHeI4bLXb7TuCIPgQ2EdOtDuLbVpr7/+pVqv/2fdLK0XkX5l+Mu00CUDXJRNhiKIoaLWaHw3D4DeB8cV2ZzHtnEql+qeVSvUVSqn/yxlvOTXP+INrqSB0Ou3nm83GL1pr7zreOXoRF31Nf//AnZVK9aLT0McTaz8QPsx3039ZQuF5/ojvl94eRWHbWvutYx17TAFQSr9zYGDwE75fGvy/V+vnY27vNjnGsafY5MURLK21KpXKr4zjaNgYs6AlWFAAlFLvHBgY+ivP8/QPPfOtXSQh0+coMHQOT6Xnu3iP3mN+uJtSilKpfE0cR8uNMV+e75h5BUBEbhsYGPrUvwjmYxdg4DzHWUl2p0hMsCL5GZYuQbLJHkkFZ44A9ArUD18TEUql8lVRFGKMubd3/3wCcFZ//8CXfb/U/8PJ/FRl7TG2dP9/LPkQFjowP0KQefb1bvjhFgLP828Kw+BJa+0zxX29mRtdqVQ/ViqVl/3wMd+Sc6rIMcm3WPe/FA4t/i7y6Lj8TK9uixcodqNX3H54m7UWrTW1Wt9HgXXFfV0CoLX+d9Vq7cYfTubP/QnWab+xKOM4IwVfLTYVEXeSZEa9wMNMpmz3/2kT5myfnzrznfzD06y1lErlZaVS+UPF7UUXsLKvb+DTnufVXuK+Lb71KJ0FbGx4x3llNg9qnpyI0ILz4wWrXcR5FnGCINlhSIID0m3z+okCNpSuC+cWSLpO+OFrIoLW3vlB0HkQeAEKFsDz/F8ulUo/MNMfxobIFP168XeP7c403DHy2QnDjukIRa7xGb9ShqXnJPvEJudjnfYX72IL17Ak+wutx3JYm8PF7gN+uCxC6grK5cqvk/A+7fGy/v6BZ35Qvj+KDWev7qPRNhycbONpmSe06+6XJCGcCASxEwRPBMTpeHrGfFDNJvZfSGBDdmDhvGSbTbU8cy3pMYWrd51fBBxJpLHYKPUlaYIxMTMzU7dYa7+pALTWP+b7PyjtdxrUX9JU/ITQGbETO1DoV67MkrmBkhI8EUS6bEXGE4UtWIWC+ZfEEqTuIrEEKtFeW+gH0g0IJQGbxfOlS0jdb2stJe8UuX9ahcdZAd8vvQsSM+D7pbfLSyaivUgeSp7ikV3TvHC0ia+7+zGfS07PT3mTKaAVsJKb+RQU4h5UYQvfTihU5hqKjHS/VSogNr9ON6O7I5P51cewddMSPFUcMT1BRXsR9LJUKt8GrBBg/eDg0DOe51d/EBbAYjNz27MDQYiTPmmV75DMyroT55r5nFnZxRAUNkn82IJRkYyN3QbcqXZqkECwiXsxySWtOMFKkERXwFDEE86iqfSh5nnYl75Za5mZmXqzp5S6Rmv9EjI/8YvJT2d57NxYXcAYw9KBEhZhcqaDUonZT8w6CaIvttSQOdNsM58v4rQ+Ti6uMhdjE3NuMT1ZwRTciUh2ZNpFEVeKkxj6LjGUQpdEwIrMD0h+gE0phdb+LZ7W3lUiL+UQbwEvF1UubT3qk2mzGNLqsnS3KphdwZlsg9NKEefrsYmZT5jrJX49vY073szRXFvsmzh1V+KsjyW/T44s5iSO3fYua1W8SdFm/GAkw/P05Z7W+vyX7pYpEQST+OOuyAnrtC3VWBEmZgKUWDyVsjuP4621WRybYhivcB/tdiAYxyxx17U2Z6BKGSaSnRvbRNQUmATFa4G4wNoujS88l6SokDyQSa1djmB7HU4uBGHsBFarF18otPY2eEqpM170OxUe0Fooe9BX9hivh6g8QHestYX4G4vWyc4iiicBZsmuFNgZycM3Xywm0dp0f3p8jMUr6KwltQJCDBmINImw2ewYQAST5A/SpJKhoOA2/ycVyozVc8BBN40iY7l+XZnRFmwfD/BfZCFQSpU8pdTwi3oXoGjiRJyUz7RitCTgSYpHFpndbVS7QrwCYk9NvpduS8y/L92I3yIYawrATiHK4kvKJIuHEInDBLG1xCiHRxLQlwqCsTljU3CZ9S15KIvFWsn626v3vU2JMNaImA3JopNjnnAKzVqLUsp6INXTf/k5tyNzvLifceKLix4zxwWJqZeus7P96W+doPLUJXhi0QJK0tDPopN9Yi2BscTGMlNv044ttbJPrVJClCO4pyCyCYOT6yosccJQp+k225cKauomJOmPTU2YTaOJ5CGyfb3NHaAFtk8aREAXH/5FaiKCkrlR1GluBbnveaY5N05sbZ6ntwUNJwFhjqm573ea7ovFS47VWCpiKStLRVl8wBhLsxNyYKLFbW+4lf/1v3+fwRUrGJtqEEQGY50p95SlpKCUfHvK4isoK4sniX8Wg6bQT2vxdcq01Np0WzZbeOC5BLfZX08VmH8yTQqf+W/W1byTv9NiW2pcJdcGUjfZbdqLgyxKbEYWEcdUiwNjaQIn1XadaI/CEdATQWPAuNAuNIbxepu21fzX//oe3vcffwmv/ywuvmgr73nv+3nsu4+zcrCClBRKCVo5AGoT5sZAZAWd4Ik4cfo6cQ1BbDh/7TCC4ondE/iezvqfDi51C33iT3qY0x1MniSpc7LO/T3fKQMDg+O+Xxo5Zhh4Un6oN7wpWIIMKBeTOm5vJ4zxtOCpHGkr6bYEOokWtIAnBi/x9VqgIjaL8WNjaUawf7zFsjXL+chHfo1b3/g2YCnGKB74Lz9O08R8er/hH//hiywpCUuqHmVPJRZIMBYiIDIQJG4sMoIRiBMsYWyu986IqRwYirtGFg1kBO0ORclyIqfQTpxPE4ubynNCF11Y/PKYPo/vUy0Ra7HGcOPmQVb1+5hkfD9jvuSATycmX4ulrNzvWmLuS8pQEksUw2wQs2usxVXXb+WrX/kLbn3jzwIrAM0LX/sMu+78B6bvuoOfHJziP77nx2l7NUZnOgSRQaxBi8UXSylxM6XE6pSUwcOik/54iVDmz5kPs9pkxLK4r0geaxMX4ye5GHsC6ptecD7NX2Q7vgCcoFAWLDu9PetKtFiygZT0UMGyZrBE2XPdSs268+sJ8cVk5r+iLGVxjK8oS1VilIVmZJloBuybDHjXv/0xPnfnx9h40e0Y+kkdS3PfMwyv6WfZWcvx9n6PN9d28+e/9/MMrFnHwckWrchirMVT4CvnWiqJ2XeA04FQXzmLI0rQ4j6OJ/mgU0qDvDopsRWJ+bPWYkweph4DJS1A8MUdOl/T5XL5P2mtF44EihK22LZAoWWe85dsjL4b6AlPHKhTD2N8LbnJT3xvivK9RBs9BVVl8ZVzAwDNWDg82aLuVfmDP/j3/OcPvA+/7wIMGknSPwKYqR2w/xEq1jKyZpCB8ixbh8e45fZbeGhXm117DlLWGlGCL05L3dlJ5JE8k0mvKu63ezpL+mRdeCwFwRZCI1nYmrqRnGjdBO91pvPTfFGc6W2t44PARZmVYoQrGQFy5F+Q7oJJVBQshnL7yiWdhF+u6QQzKJVqHVS107qyJMgcS2yEemDYOx6w9vwtfPiDv8T1t7wc65+VA9A87YQCIqOpjwfUp0OsMex8fJKRVWP8ys0X8Ynhfu7/1sMMRxG26lHSmrICZUGc08cqhwJ1igekMD5gSWOB5OmTcyyM1BTDVZ/nxwOUzEfibrX2xRIVw4iT5tPcdorTeefan+6hUuZ0rDs71iMYyfFpYkeLwwAlbVyYJ0loJlBKwZ61BLEw1ozYMx3xo+/8cb525we5/vL1xHo9ebK3m3hiY3SlxP7JNuONEKM1XrVMfXKWgd1Pc1ulzbt/5m2Y/hGOzgS0Y0snQX2pK/JwfUrjdlUcd5A0YnEPlT5nZC2rB3yuXteHNXFPn3J65edZ8nKC058UOA0YoNgpKXySLT3RTuqDJUHy+TE54geygk4Fic93hC8rB8x8sWANQWwZa4ZMxGV+60O/y0f/8N0sVTPE3jqUP0Qubj29tkJ/zcP3hKOjM+zZO82+g00mGzFLSm2qwRGWPnwv/+XfvY3hM8/kyLRLHoGzShqoqjQCcf1WyWIIfiIUQh7BSMLMkha2Henw8UfH8LxCSWaPJUhrUSzQMnPpuuh2nFOOLwDHFLpETucg14VOst2PYZ2Wk+TWHehzJj0Feg5hC34C9Dyx+GJcOBbDRDtmIvT5vf/x6/zbt10Ko3uIZRAZOb/r2eeQz1pKZZ+hlSNUh5dQHhnCVPuZpo8jnQpr1i2nMX2UI3d8ml9/92voW76csXpIPXLm2A0RSxJ5uOfSSU7CWqGYxtcF4QBQSlA6iRtkHrWxOWjOVeMk23GMxmlwAV1Qd+4RhQ5k2btiAiiJBFQBEPb5DuQpXCjmJaFYWVlKyZCsMZZmGDPaNPz7X/1F3vmWrTA9iokMduR8JGph4jbG5ma2SAulFdMT03i1flaes5Hlm9exYvM6Vp23npEzlrNk9AAXr+ujOT3J/s9/if/w86+mrcvMtGJakRPyVGB9cULgZ8+RP1ee0c2rCKVXETKUmKi9zBGJF62dYibQGaki/MsMV1E2rPPpKSTqvYRYkCTxI8kQb0xCUMhQuBI3umeNpRVZDk0H3PyKm3jfVRWCuz9HPFtHrn4zpeoSaE84LVUeVvlQHgYRjAlRqkyn1ebAY7th6VpKK/vRJsb3NbNTdQ4/tZMtlTqbynDBZT519jHcfpR977iND/35nXhaOTDqufxAhnqS30bSzCcYQzLCONdZdoXFBZq5717VPY4wFG4gSV8WAxlOwQLYnp/pHVPOp9vtPA+TdrRgLq3Nsnmd2KKVKwPzFJS1cwWegFhDJ4aZVsjgmjP5Lz99FcEXP0Hzf/0Z9XvuRq25AlElrFd2rikOkbAFwSzWGuhMAjC9e5rGkYhlfbOsW9FkoObRnGnx7LeeYcfeOo8eVhi/Sm1gOVuqmtVPP85/WDrK1ku2MDbbphm5waXIkowL2AQHpHglT2ApcZija1g6+y5QI7MEBVIej/YLsWORePEULEC3iCU8zKpwcs8gOaIpNJWcq8RkZjFNonjJpXWC9n0sVeU0JLSWZiyMtizvuf1qLhn7e1prq8T9l1C+9Vfwh1ZAexoRH+spiFpgDYR1rGgkbEPVMDDiIxpU0OT8M4/y3E6Pb363zoAfcvYNy9l848tZd/PrkL5zqb/vNZSCgP6nH+Q3b38Db3luL/Wgja89lLgEkS+JuYpBlAUHUzLt98SFipBnCFKLaLqGjAt2YUH6zbUGXUA7NyvHbccWgEWakZT7lZKmExrSoktbQMA9DsL9X9SS9FKJxvjiyr3dKJw7yRhLOxQm6202XHQpv3ith3nuMKXAYDffTPnGnwQs1q8iYQuFwnhVbNRBTAzBLFiFDWZYtSbgkqur7N3n02qXGJ8OefXtHhtvfh0D1/wcDF8MMgClPqLbf4HwEx+kbiNeMf4ob7j+Aj715Yeo+BqthLICbQ0xGq0ssSHLDRhxuQOTWLco4Y4Si7HumLJ2WccwRfvzuv9cMOYG393eo+ug47Rju4DjXiTvqcUJQDrIAyDJw1rJu2ZJ8+ZJgie9hrhf6VCvSsyoFsly8LGFZmSYNiV+5raLWHnw60SmTKTK6Bve565lBdE1bGUptjyIlAaQ2lJsZRjRPvhlrAhhs82GTR4XX1FFVYRrr/W5+BIPL+6nPbMUEw+6rE5nltrr3gUjSzEC4a4X+LXLB1i+ZhUz7YggtnRiJ7hlYkpiMjCoE5eQK6RkVtFmguCygrFNp6cVAHJKMeneshA8tCeBG4+fCj5GC+PEfItLjbY7UdLDQm2cLUzYsIVBHcmrXkQsnhI0Dk1rESraEbCqLVoMsYFWZDk4HXDhtdfyB9cb1N7HMWGEOffVlG79QAJ+BDv9HHbnndhOHRk8E9EVUB5gEKWR0hDBE98guvduBqSNt7KKxBZiDzm6jda3Pkd04AlMeQlqaB3e8CrCRgv16H3gw6pgjNkN5/K1x/ZT85Ubw4dCZZFLD0e4gguLdFUMFVmc0sUmdMvCvi5AmB+/qCYsLCXdrXXSAhAbw8ZVAxhj6YSxQ/AFEJP+TOfrIYVQT0BZlwhK8/tawFfSNfhSVWnCxxIYYapjmLY1fvtXbmXr0HbCjsKEHuon/go9tJp44hl47pPIC19AzeyG8aewhx/Ben2owXVgQySOwCtjnvsSsu07DmkurYJR2NgiJY2xhnj3NqIn7yR4/iHsqvOpXvladt3xCfbtn2XvwQ5XblnBvdPC4aPTVH2FVoqSTqqDEtDnhpMLjM+0PNVWyRC7FJls00LVnKNRHKOymvfjcLZLcI7ZWicZBViMMZy9aoD+qp9MoSqUaNt0SlcCajKRzmVfJRKaQkDBVd9AngBKs2wAnRgOz4Tc/IqX84YrYlg5TOmK9ai3/Dv8My8j2nkH8sgHkdHH3O10GdE+qnUAnv0MNmwhupz3Iw7whoEIbMdio+QTQKkCttQPqoS8cA/Nv3wnsUSU3/Z+ZqYtHatYvm8b73rZOlpG0QotYRzTidMCFcmEOk1np+Y9ryAm1/yMKmnhaRI5pYdaw/qV/W5eBPnpxTZHJopCcAxBOOkwsOR53PXIfg5ONNA6N2exhdDQVfHj9hU1wTE+T6HmAyplMVmqt5T4w04s1EODqfTzb15/LqrxPHTA6qXI1l8mPvBN9POfQ8QD8bHG1RZYY0D5SDgLjcMgPijBoLEqxD9zAH3OcojBxjE2NpjQJZpKfTFxKMT+EN7EC8z+0RtZ/cpbueCqLWxZauivtPipkRZbz1/HdDOkFQmRSYaQxeKJcaVjyUhmmj5OrWNqDVO6ZfURqWAYCKJ86kk2cbpHmUjP7ZWI7iTDgu0kBcBduex7rn49VSoLa/oVm4d9YtM9Hl7wDglR8vSvBjwMPlBSUFEGX7loIkh8/5HpgBtvuYFXXRBjGyE2ahCteQ12Yifq6Y8hqpRkYWIwMdbGWGuSXECACWcdHXQFM/4oUtmLbFyHf/5q1FANGzgBIBECr2xBWeLQEOs+9K5vEfztu/Bf+RaqA2VszWdo5zbeeelyGlaSwSKIE5OfJrGcNUtCXtIiEpu5wiLjJcmZGGsZqSpu3FBNqomE/aMNVyRji/RMrIt0p9e60i7zhQyFdtIWIE1tFs2MsYZlVWFlX1pl7+6cdtZJfqHCR8hqBLQIpYTpGlfg6fhhaEaW0KvyzldtQE0/jrWGuLoOU1uLfvIjiNVJNU3sVMfGiHEfTIyYFkw+D4ARjTz9/+E1G8T+CiSwqMFB54hjA7F1WMBCdcBiIogjgykNIDu+iTp4N/5F68FY2o2An1oTc/F5ZzLdDGjFliAmmTdAVq3kBMHgqbSEPI1+ChagoCgi7hpBlA+d6wXWYTa2YB0y3vT+WLidoADYrq+uEMUKJS08cTTkwX1tSjpN9WQQNxOINDvgYdFJVY9K6vrTMX9wFqUVCUemAm545St59WU+dnYWojbx4IV4e76KisKc+anmG+MsQSoI1kJ7DAHM/rtQB+7DY5bY1xjroaplGBrAhjE2oaiJLL5v8Uo4KxBaIq+Gv/8hbJ8Fr0IkMLTzKX7qoqXUjaIduSymSQZAPPKKZVd+nc9BSEvI8/GRlKQudJxpGx460M7rBFPJKADDxUL97gRBdztBAUh0eU5q1/kAi+Bphd87H15yOXE3tRQHRCw2qfFL8/3ukh3j6vrausLPvmkrnj6MNYKpngXeIHp2n3uEhPnpR2ycbRMMsSoh61+BiUPkkT9BzU4ihw6h9u6kowcQC97woOuZMYlauT7UBp1RMZHBRJbIKuLRo5hyGQx0Ztr83BkRl19wJlMtVzfQjNJ1lJ2r88QJg7WuwDQbFczIUwgMk8hAiVD2CkvXZH7/BMLBInsWaCflArq6kPQrwyYUZsMUtD47NzH9ad7cDe+6ip809SsWgqSi9+hMyK2vuY3bLy9BR2ErS4j7z8YbewQryhVV2AiM+4iNgcQChC3MxB7iZVegh85i/6f/N0/9j3/m8PcCmkcN/uwYXjCN0SV0tYoaGYIoyoTAxOCVoNwnxKGzCiZWRDN1jGqBr4gUDO7axnsuXUoHj1ZkacWWdpIK9sWZ/FIyv6BLkSV3iXNIKt3/d/93DJU+wXaCYwHzSGDSF0uexcpRSD4rx5LmBGySBJIELTshcObSZinSILbMdmJMdYBfeOsl6OYObGQwq1+OUEM1j2B1DZHIxc3pHPw4wLZbELZR7SaxVagtbyIMA0Y//qcEe2DX4Sm8qqZvRNG3aS/LXn0JfaqOXjqEGZ1wFiCrV4O+JUJ70rkCmyQ2gtkOus/Dmw1oNQJ+Yt0sn7x8Ew987zlKukwnTiuIXe1fZCUZOpaMjJFxBMwEwuXPk6RRN5NPmN3F048RDZxCFJB8kodx95vrk2wiHAonFyrxe1kkYNOyp3yiRSe2zIbC0emI1//o67jlghjbbIP2sX1noI98B8RLTH6EjSMEA2ETOzUKzWkk7GDDBmbrz+APnsn+z/wFzce34Q2CXwGJY5qHQ45+Y4yx+w5DbQBdqyBLhyGOSScQGuMmqPaNKOLAYiJD3IHmEUOgAU8RC1R2P897L+7Hlso0Q0M7MoTGYJJnlWxIO/X/efVTyhyLQ8UpaE53SELHbtoexxUsUmJOCgPMt8lKCu9s0QBkIpFW+WTINznGE6GUTIey1hLGlnoIU+0I6R/iXW/YCDPPYmJDNHwp1MdQnYkM+NnE59ugAbPjiIkAi0Qd4toqvAvfSmwijn7qz9B+oV8Cyge/H6a+t5t6vQJa8FYMO+BlciGwEfQvUXgVRdyydCYtUcfSmoyIqh7EQqMZ89ryBK+9diOjzYBW5MrT27Eb7XOXcvTJLCG5/08ZL4VtKRlNktPIlyg6QXvQnTboaidhAQrcTWx/r967kdE8hEln62YLNWU4wKV+lUBsLB0DjQimA8O+esRP/NRbufasNnEzwJYGMKWl6P33YaUEce7zbVCH2Um3zRoX/oVN7KU/h64u48Adf0fr8afoTXhbC1ZBNFrn6P27oW8Jqq+CLBuByJBODhVxzKsNQTAjRB2n2XHT0FYWqs4KmBd28x8vXcKKVcsYa4Q0Y6EVQTtO8x4kIW+hFD6hZW6lC3gJt4LaxStLvPnCEaI4LuxZZJvv0MK2UywJc82mUmnzByqkB1z4Reot8hm7bv6cTZJIllYsTHYse6cCXvuGN/Lr774UM/U8No4wK65Gxp9FgtkEpEXOArTrUJ9JNNYlciRsEQ9tQm95A7GNOfQ3H3ZTzVK8YnNFskB5EBqPv8D0dAnxNHrlUsRqiCymGdE50qbxQhMz1aF/mXNoUeBmEjeOxgQ1ZwWageXC0Rf461+4mpVnLGN0JqQVQyc2hMYxV3Uxu1BCltEn5ZFTMC1wYCbi0f2zPbmARViBhbBiYdtJCEDC4oSQORRwal3EHSkwTBdzSKU+H/JNsmTWZfwaYcyR2Q5v/Im38Te/+2qWth4CW8JWV2BtGb37G4h4iHE+37ZnoTmTJ4CMSwIRtrFXvhfPH+DwV++g9eSTmfYXw2ivDLUalPtAzzY4/M2dUOlH91UwI8M09zVp7W8RTgdg3Coj5WrMwEoXwUcRBE1Du2OxQx4G2PfCEc5+9Dt88uevYPPZa5hsRs4KJGni5HGzia3WFsPB3FrbhMZaCRNty47puGuhrDmcnK8dT0YElFILvzPiWOd7Sqh4UlzSLzvD2sLUECuZtqfTqlIXINYVT7RjaIWWidmAM7dcxEfefxODU9/Ett1QkVl7I+rgd1BxJ4mvY0xrBpoN0sxflgMIZohXbEVteiUGy4E//0NUSuXE95fK0F+DWoVkBRIo16D9xAvMzmjQCn/tMqSiEK/7vXcSQ7kcs2Q1aNFYA/UjEWFFaFpNXRRTh8c449vf5P03r6FuhFbk0sSRSZeaSunkND317XkJebIGobjFbUQEf07G6NitaFmOedyx3gFUWuAKxlqGq8L6IY/Y2G4hsDYPB91j5lOmCgUSqRtwU68t7dgwE1je+pbbGbHbMR2LtTFmcDN0QvS+ex1qsxG2OQvtpNTLGgfUErdgDdiXvQ9Plzl6313UH/kuuuoIUilDf79jvKfJrJdYUBr8RpNDd++Caj+6z6O0aSXKt0hVObCQ+jEDpYph5AyL9jRh29CuQ6e/RBRY8GBirMXlaoLNG5YnhSOGTuwEPg35nBXIGZa50sSXdkUDc4Dc3IgrPcBY6PcFX88pwZ3T5nUBFvCVZdOQLnYh64USGG0Ynh0P3bKu6T6BbB4gxYXVUmuQ/8a6lTjj2OW8W2FMqTbAbZdUYXqfSyaZDvHKa1C7vomyOJ/fqkPQSXxQmrVzkYBqzxCfeQP6jGswwO7/9Tt41lKrwWDCeK16XGNiFURDbQCaT+yhURfQmtKG5SjfQ2mD128RLWASRB+DXzaMnAF+WTNzKMKvgO8prIFIwZKpcX7s6lU0ophO7NYAiqz7FGmUTgkqsrTYx6Kr7er4nObOVgLTgZvJdEwrYBcQAMGVKW2fikFcgibfk2pzxutck7r6li7ekhSApnMCbb7okrVuDn8ntky3Yi6/+kouXjWLabQhbGBGLsZMHUIfeBCrPEy7iYShOzvR/CzlG4UYr4Jc96toUYw9dA889R2WrYS+svO5Rc+ZWiylk2IhC54HlWaDw/fsglofuurjrV8JHXewroKquCdLu+CXY5ae4cx1ZypmaLnnagwU1A9OcH1tFq9UIYgNgYHQ2GQAxxIjWVJsPl3OsVMhLJR8f7YE3gI8XJQLONbO9AJZJQq9giD5cUmsSxb+SXaoA4P5cmvpZYx1Vb4dY+kY4W1vuhm/cwATWzAhZukVeDu+gIjCtFsuTWttAvbcx5l+g3Smic95PXrpFlfKd8dvMeDH+PNBnOT+2nMCkG4TBdUlMPPoXlodJ9He+uXgedjAdVh5oGognjiXYMArx6xYD6buJpwoXyEWZhtwkd/hsnOX0ui4+sHQFKqDLS5HUOhWl4JnClbken6uCNR8taiAYKG2qKlhnezttHNlKuNnJg9pOshmHS2eF9nEDFu3+FIQQzuIGF62ius316F9EEyDaNll2CNPoQ59HxvFSBwhtpvxNhnyVVGHqDKCXPs+FNB+5n5GJu6nNCAY093XLAnkdVswJU4YSmXwppqMP3AQ2zeArin0huUQgo2AyBHNq4AqJ4A3Br8Us2xNSAlD/9ISxBAJ1CYmedvlywmtW/Y2NC4FHBZAcxo254NkNlOc1G3aokAUSFrvxCeUFuhtiwoD514/D1bmlvx3o5U0z22y+XKSlUM7YlhmO5Ybb7qGDUceI7jvGYJvPkbHbkI9+zl3qTgFeon2pxbAuJU8THsWs/XfoAfWEBlDcOevUfUjyn2SJfQgQd3KYcmUwABicksgQN8AzHxvF53YJzhUx98wjC77iEkuEluILNoTvCooJdhY0L6lrxzSP6jdUqwCrYkZrinNUqqUiWLn8iLjXGK6xIx0kaxoVZM/1lLR80wkIV+nYFFMnEeATnEsgG5CFq+ctLTbaczrwh738FHs8v74mjfcfCXqoe8SPDlK58zb0I3n0Ue3gfUTvNdt+sUmw75Bg3jJWeirfhEFBDvux9/9AFKBwWXu/pm/93J/nz2BzbU/lV1VEcb3NJl64gBGV4l2jyGbliBR14O5SSBYdBmUL7iVyg39fQH9y3xUJLQDOL/U4KJ1AzTDmNi49Qfz4p4EKgtuECi19rabZ0EKeSwkVaeklqOXM/O23igi+X1qmcBkMtzCMph2Ml8xy+J8f2QhtIZOGDO4fA1b1TjR5CR20EddfgP+83eCqjgfb3s+SW5cTIwNQ+y1v4wu9RObmOCLv0HJGIyBviWWUtndVPtO+3uplIaA0E30Rgij9+5CzlpBvGuWaDZAyiW0FTRuNTG3nLwgBrRv8aqgy2CmIxdmWo84Fgalw6suGHILT6Yf4yxaTCFUszZjskgPv7KK4iRunYfovTxeTPO6CHJCZ8/fCYcJJJv6VYxe0vo2i1uXLzLQCg1bt2xhTXMnnSYEN74JtevrqJl9WN1PagJ74K+jQVAnXHkZ3oVvByDc+238nfehSk5OtLZU+hXtmYK/Lzyq4OoQlO6ODDDgeYqJHQ0aO8fov3I90Te2E5Y0M5MaK4Z0CLoI4JQC5Qmtw5bqJcMsXQnBtjGiI3Vetu4MUBqTZASjxBq6kediwOw6kgmBLcYBvUzKEbUFStpZoTCtESuGjcK8/FVzrrlgsz3/9Rp8m0UBadiXghhTON8k5i9K0HDHwlWXX0ylMkh0y4/i3fTj8JW7CcYd0nbLsRTAXxL7i4kxVsP170cpjcUQfPO/4wduoIbEZA4uSxaimE89ErdQfDxj3MBUpSwEAYx943nknNXISBUlLuXcnLK06oZWw2afdsPSaVpm91lq5yxh2Ru2sOJCWPGyMlJWrOgEVColosQ62YQObuVRsjA6VSBXF1hgfAFkz6uxFsraleVZ27N7AeZD0QUcVwh6IYj7m9/LbbFpOCjpfdNZQZIJRGQSIGgMnhKu2LqF8fWXMLvhSrwy+G/6Zcy2OuFMG1FuLCGL9xPgJ41xos2vxt98KwDRge/gPfk1Z86TjJ2NoNrn6vrmEMW6NLCkUVSS14gid2ylbEHD6DN1mgea6Es3IZETqHJVEOXO95XLKpZ8sA0Y3jLI6rdvpj0uyPgElWGLd57ijFURS6oV4qRm0OC+bYKJyKyJo5eBLG1uu/Iwqefv4YdAvWOoB3G3tTsG8+GkMUCuTsUloYpWOhtxQ7LfRUQQG0sYG/r7+6ExSX38KDYWwiOH0JffiFz7o9hHJonD0IV/CeOViZDGOMHa69Gv+mBCLEP44B9TbrSzefHp+JDSFr8iWJPTwxWmFHIAadcNdJI8U8l3AtBpW6bufw69eTVqoIzGMrjUopAsFtcCcR36Ngyx4m3raU0MEctaTDvAtCFuxSwfUKxdMUSYMDqt5k0FM/2dAcOCFc/4mRcEzOFpvjbRPL7uGE3lT39izREyKw6n2xva7o6ImytnElWLE7dgjKHa38/Ox7axffseKmXfze07tBPe9B7M0EbiJycxGJQxSGygPkmw+bWon/gcetCtdB8ffhh57CvJW1mkK2ePhVo/+UTV5KMK2p/2PErGlCzga+fTRWBs2zTBZAt1wSYkhGrV0jcgWJOY6hZU1g+y6p2bCXaNYqZW0nfxNcT+IKYTEQcRJV1meMmQ60MB7Jmc664nKToXMlBIghey+go7900pJ4z+knYCGGDu/bpWvk4igqLWZ8cVhkGdpOcCobVGbJtvfP0+JqZa9PcPUK5U6NMBlXf/BuWxMjw5gdgAGhN0Ln4n+q2fwq+kxIyJH/ojSuPTrl4vZX4iADaGcp/F893/kjy08gpuIWlhSGYylVgnAB40Jwz17+7E27IO6S8D0D9iqGoLs1DZMMTan9qMnqxjn4PKxZdQqg4i59xM3AgwHYOxPuVKLelzggGybqbuoOBSi0IB2b5M/3vAgVmMFgs9JuU0LRadmX9LIX6xyVBwbrJy6GKTZ3D7wiBk/6FRPvg/PsbmTetZNjLEiqEyasM5bHj1e9m47SEGW/vhR36F8g2/gRSuGI0/gX3iLlLeZ7SxSZcMaM/ilYW47jqh/UJ/ky7HMYRx9+icpxwoxMLEM+MseVkb2XQGPPkC/vI+hq8agOFBamcPMH2ww86HW2wfHeHcXeNcvmoEvelqosfvxrZmsbGgdA+5rc2Gzt1oYPJSq0TjTZrsKVBO0kWm5193/vgtxWdJpHECApCysJuVxT3p7q71/4pHSj6LxQJKKeqtFsNWWNcOeOaZPTzz+B7SU9dfcA4XLPGpiDAyu4nzN5Y4d9W9LNt8MahhBAi/92f4RyawGqxJbpB20wLKmfpqP3TqyXt/kqRPccHuKOp+RAX4WuhYi9IwcyCk+fhu+m/YCOtqiFI0Jxvs2D7Dk58/yO79IfVWhGnE7No/zTnn/yf6R1YSnn8TPPAFGlMN9h5uzqEoCY4wGblcJ4rzqiwkc0ULMZd0kXfxLOzhh65Wa/8JWMT0cCl8p5TKu9A9V8T1Lgev+WQP98A2M4GTsw1ueNkVvGvkIOv8gGU1jViDt3IVl597FsHT32ffwSPseGoXj3zpbs5rf4Hg3k/wmft2snxgmqEnPoo3Pe3GX2MgLZvLfKdrCug0BNEgadyf7jTQ6ZCNcKarlISxEEZJ7UAMJT/C92N2PDzGd758iPu/Nsb2Jxt06gEVFVPzoa+mmDk8Q8sKF1x1LvhV5NmHeL49wn//1hgmaOFphVLKDbJJivaT0dMCdtJKWDdSYaoZ5Z0t7D+pBeRSwXEsbJ2AAMxzpZ6ApDf6SL+LkyDTm6fp3SCMaegKb9tYYVVwiAuWWS5b5rPhwsuJn3kEog7L+xTLy4rbbhjiujMC/vIrR/jtzz7Ec9/7Z960tYTXmiEWQZp03aPYD2sgjlU2bSu1UILT/jgiyxSKuN8GCEI3aqhjKPVXePSQ5b4vjTEx60q0RoZgSRWGK26ZgWVVWDMEHNzNwAVXMLJhOTI1zvfbG/mb+5+iJAattRsCzsbUk0/X/w4rzbbdXCJJO1ak4xyLvCiW5f8KLXVyr4vL2SuFTXNXwcjNXBFt2wL1K57w2LZn2TGw0Q2Pti1LrnsZGwebXBJMc+Nyyw1LY16x1nJuLWLXCzH/dLjCugGP7c+3uPdgGcoaqVhsv2AiNwCEJRm8yQFVqcJcrbGJ+S8omOAsgO+RVS2XfAjWDfPxvVBbXaFSUvT3KXytUJ6gfMEva2o1YcWwEBHy119/Ac59E/rN/417JqsQB8lqKvn90mggDQZ6WeySQUmyqEjYuf8szKb09zyBg4rjaPEjSgvdJeWu7ZXO9EaFsCc9TECJQnsezZkpvjLTx8DwCth4ESNrRzn7/MP0X7COUmzpr0AgwucfDfnUNmGiGaOV4EcBn/lWE9u/HB1bN04/JMSREHYgDNwnClx4pzzjagAL1iFKBhdTs58W3tbbUG8Kg0PCkiWwdlOFeycVn//+GN+yPmvWVljSL/T1CQM1YaBP0V+DvhqUqj73T/jc97nPsf+gobnq5fzjN+51el5c5KGXmYkUSIKjUrJqEarzvlC7B9L3bipev1dWkmNOaY2g7MqFPqTakwLB/CNdx2SQwFriOGbX+CxvvvVlrFq9h1o0StmEVM8coTVpqbaabJ/V7JkMOdqyHIndGz0qnjA5HXLlpatYqSaxEa6GzxNo27xoyOa0M5EQJxqvxJl4rMvqWQP1Dkw1oR26uoVan6so8jav4ve+VWd6qsXumZjaUJmtAy45VfLETQHTMFgRvjtZ4pv7DK3JJl7Nx1bLfPjDf4yvBK0EJW5GgIhgReUYQCR5tW1qTRWRgU3DHtdv6OPJI+2e9wmeoOIWvYwTiJNdIqbQMqCXd2jufPU8TrWFj2OCUPY0+/bs5QGW0T87SjDRpj0T0R8eYNV1qxivrWTn0QhfCRv7YEs/hNapa7ve5hPfnoVaP5KsIawqFj0siC7OrnU39UrOo0oSMKS2dboFh2dhpuWExffA893v/kHNI1GNnfsblHxFTQx3PtfiO80ySyoKX7tysrKOOdBRfP4FQ39JuHKdcOahB/jI//zvgKsbkARo5FME8xRaah3d9nTZHNg3E/GV52bwPY217rV7xzL+Rbc8x+MVfbGcjokhPb4lHdvOQb9kmt6LB1JgI8qNo//pFx5hduA8aLaI6x3a420GmzuoXbCUgTNW4ccWZQ0XVGNWlYTIQl9Z8ci2KQ7HQyi3sjTgqnX0ElwhZwEnad+iPbcOQRjCdBOOzsBsKw39XJZQKycAGmB4gE8/Ok0niFxcrgSfmL96LuCZdoW+MEaFEeUlKzjAED92gccHbxF+5wYYWbGUO79ydzInULJnTkM6U6RbRs7u+RWxFVqxcwGewJblZdevBaRgDkwouAQR8uXk7OmaGVR4IEilWHpkQ7pHt8iPEVFUPMXDjz7CfeFFVGs1rFisaIKO5azOM1x5aT8jWy9gAEWfjbm8P8IXQSmhNdXms48GUKu4NYcTM6nLoIe6hUAp8EtQbwiHJqERJCBPpxM3HfO156xFX59wsDbE4y/U0b5KEp6CUopGo8OnDvss/fGf44r/+UnO/d2/5+2XlfjpzS1odtjtX8CdR0qE7RaeUlm+PiWSLdALW2R80Vrmcy8sro/DZU1xEs7CjOn5LvxMDfZpwAD5O3K6bU/+y9ocE8zTp+yYIIqYKq3hLZetxo7vIJ3NqWPFg89M8NXDJVafu4mhxiylTgfRisOBUFOW/TOWWy8oU5Uged2buNSvBvFwBXrGhXcxin2HFRiLV8j5p0BQe05IymUYXlnmU4d8nnh2lrKGmlhqAqXYsHXtCOdWPO7f22JqxSbOYJqxr/wD972g2H7QYi9/Jb/9j9+g1Wjiey72T6OA3BUk6phgAMkcdaog+WCbiBtC3zsVJNc6KXYVGXDy6wTmbW5SqHdvRbtZREFcXBWk2J/cPTy7azc3vOq1nBNtJwo7gKAjeHhM88XHJ9g9Uaey6SzWVxRDM9NMGUVLCZ1WzKplZc5dFbtBIxIC2oTBnpvlay14ZUunBUGQlIWn8wYlmSDiQ7kKfRVorBjhbx9qoDoRFvCswSBctXkFV/ox04fHeXbPIb76T19laP/DVDsBOg657nU38dmZIb5+z0P4WtAp81XO5NTcoxLmi7OSxrgIKa2uLuqVs1DHMNwyz88Ce6Twv8jpAIEFczU3/hDC2HL2sjLXbewniOKuvSCJ+3BZMU8rbBTwkbt3EG95HaoduJU6jJtapX2PoFnnn7/7FF8K+qicey7XVKEaGTwMd36vTb1dQtzrN53pVy77h8oXXxSBoSWO0UXt14n593336RvyeGy6zMx4h5YVojDGlsq8eusarjR1xg9PEGhBleC8jYNcssoyVA644JrzGT3r5fzJ396Z1f2nGCBbU550zD+Pk+LYsrpfc+6ycjKZtECreazrvG0ec1+8kC38bzlNGMB1qxBcF7rga+GZ0Q7f2DFDydddK1n3gkElQlkrPv/5L/NVcz6VC68jHu9gYqEVOQSstKaqYMf25/nErkna523hmqV91OKY0fGQbQcB33MRgHZexFghmk2657kb9vVDuSRolZRyiQsFS245YUo+NPuq3PX4DIqYRjtieKiPd5w/wsWTR5ieqBMki0O02nD9xWtZs34Ta179etb9+C/wh599gEa9jqdUErqlKmizms7eQV1RQis0zLSj7sUjutpxPX+RKfP/Lmx6cV4dmwaaCbfjZNQrXyC9EALZ3L+JclagE0f85h99nOv/+gNUzCDx977M6LRbWjKFTpWSR330KHfMznDtls1cXBvn+7sP8/mHfS47w8PzY0QMUdMSTSVwKnlaK1CuWQYHhclxhwusypG/70OlJmyLyhw9OsVsLGw9dyU/ubaMv/8oe5ohUQTtwDDdgpe9/Hx+5t1vpVbW6BWr+fsvPsUnvnSvi/nT0E/yNdOLGb9UAx3AE6YCIIiTsnKHnNLxrZxtRWjfS/sFdnf73EwBTwMG6L1z+pUOZSZ7E+eTzSMo+KRshDM5VgF7Dxxh+56j/Oh7/z21tWv4/D3PsH3/LCUMynNoXCuFRBF7jowRLxlmw/AgBw/PsnWVMLJMME1DNJGIm85NPYnWI0K76WaJinUC4lUEvwLeQJnP7FAcGQ1YvWkVqqyZ8YXqeevZfMVGtly6muEz+jnjnDP42Z9/HYN9ZdTICu68bw8/94E/IwwClyZWec7fDfYkXj0lguT+H5EkR5EkhYr1OhniWxhvLcQOpOc7NyKnAwQucHOhS2rn77Ik7xSQ3Gjkp/P0c7t56NHt3Pa2d/DKH7mcKrMcapQ4enQCohjlKUQpPLGMT0zS0Ir+ch81ZblwaUQ0nab5cpBXHPHTntBoaYKRKv0qxMSuyqdcgkOlEl94MqZ/aIAXJpo8uXOM7++a5t6njnDP06M8MtZm64VLed3LzqR/9dmwZBWf+Ppz/PSvfphms0lJq8QCqAzwuUd2HclzAk5ILIWMYAEk5lmslLDHpjnHkpO5QnH6BCA1b8fsaMZk6TIWLlFUfFb3Q2F5fs9B7n/wcd7yo6/h5q2DXL8OVpx3EWONmNHD48RBhNaakqcIm00OtUIOhn28YnVMnxdgkxf8iip8JwLglQQdxOyb8Vm2waNiQpQHflnx7UaFxw8IL0w0mW52KPuakhLaHUPfQI2ffMUGbj3Lp9S3HL3+Cv7yi4/y8x/4E4KgQ0k7xiuVLAWZWbj8fQFFrSc9JhmGTC3EXM0/wbaQEORu4vRagC73Y3tdUcL0bCm5PPtF4biigKbn7z08ylfvf5hbXnkrmyoznB3v4ZYbLuLsyy6gZTRHDozTbob4vjAdW56faHH+pqWct7ThMENqgosf5crCtCdM7os40imzfo3FszHN/gqfflrx3GiblrFoJQRBTGiEV1+/jj/46bO5aWMFUUL57Bv56Bef4j2/81GiKMRPNV9Sze/R4KKG9wwJF11F6iK6hOAYrv+YTFn4nNMnAPMBzgXvK0U3lLec+d29FuDw6CRfuvcRbn7d6zmj2qa941E2DQuvuOE8rrnxCsp9NXbvHWWyESBY6tLH68/VqLgNuhCDJ++nyYjvCRLB888Z+tZUWTVgeGCmyqe/36RtLWCoNw1nnjHEr73jHN57wxKGNETWUN18DX/8tYP80u9/DBO7sQqVmP3MxCdYCOnW/rmCkBcj9ApN13KxJ9rmwwJ5O/0YIHvootnr2Zv+yrW+GyPYFDH3dHhiapYv3vMYV936as5bElDfuY1obC+rzCg33nAhP3L7dWx7YhcTk3UmGjE3XHE2K/VRdy0lhU/6v0KVBG2F1lTMob2W/o0D/MFDAXvHO4ShRXseb3/VRv7b29dy2dKQdiCIX6K68XJ+/64x/sOH/g5r3fyGDPVnZl/mvBhCoZIXRKQ4oGj+E8pIN83SSbWLJP6J7DvdGGDuPbsEsEvzJXMHKXGAfDiU7mNTkDg9W+fOux/l4htu5ZLaNJ3dz9CZnaD53JMsH1rCigvP4Z++8TRhJ2R3PMhtFy+h1BoHpXIB0IJot+KH8l25dTgjdJrw3VKNOx6f5cylFV528XJ+4x3n8BOXlPGaM7So4tUGqJ55CR/4wiE+8KefBmzGfFFuGUzJGJk+Xfr8Paa+6Aay4wvnp+cuFgQuhPoXOv7FcQGSfbIkF4WBoUStu1xAts32bprzGyytVpsv3P8U59z8Wi5Zaukc2YeJYfy7T7Fh02oeGwvYsW+ap/aNc6CyjtduUYiZdZVDnriPTr6VIB6UWpZ40yq+5S/nZZds5KbLV/Kaq4a5qNqh0YqIy4OUh4aprL2QX/3sHn7vY3cCbqFLnQpWkZkUBT93AS4nQMHsS7f2J3ST7DslwHx2tLBhPp96fEF4kcLA7J7JA817QC7dWYqWHrdRMA25CLn/O+02/3TvE6y+6iYuX1mBoI0qD6AbTTZecQ7feOQgYTvknh2jlNdcwPVnhYhpIJ52mpqsTS8KpGxplfr5xXtjvv3EKPt2HOYL9+3lhUMdLglnCaXC4KYzKS3dxC9//Bn+8ONfBVzRZp7syeP87OlT005BCHoBYMEiZMfRfY0Tcv7zHfrSC0DBFiV1T92zXwsM7vX/COlrZosZM9tj3iyWKAz56ne2c8vr30Z95z5mhkZoLR1hZMhny4YVNJoR+0brfPmpcTacuZGttSnaLTCRh+cpxFfgu0kbv/AF+IenZ+iLIkYbEecu6+PWIUvfyhLhk7vRtRX89sNtPvTxrwEua6e7AF+3yc81132rrvi/VwjSY6Wb4dJ1tbltPp97AplioPXipIKBrt6Je3FCOGelU9fjLEUq4NLDudZbDErApDGyMSgF1rgK36DZ5M+/9AA/H7fY8ZUn8MswG8ETUuXizStYdcVK/nHbBP/5088Tn6c4PB4SYli71OOcVcLGtR5//rjm755oUBPYE8ANGwZ584ilPj3Lk88LL19SZcxbx1999R4gGTdQki3u1I3aXUuxTGLMyVbzKOIDocfXzffzGNp/Ysyet522waDulnfaiptBu36olJl6KFiExEymcDAPmaSwLzHVQrIwQxpuuWt87btPU7n+Jvr6BVVzAz2rbIdtj+6hvW+M164sc0u/pe9InaXtkGA65L7Hm/zFP3f4qxfO5g+/E6CwhBbe/Yozed8Vg7RnGoSeZnR0htGhVTzQFMYOHUJIh2oltfFznjk3BlLgb0EcMtfQXfkzv7YvgsvzWYJFeo0XyQLkvlpwCxbsmAySlygWj4HM66feIg0VhOKb07JrWSwoizLpRD9h7PAYn332KK9Yv5aDu/dhPGFF2VLVin2tmM70LMt8j9hTLCkZBrCcs3IJl73zjRyMY5pfeoI1wzV+683nc82Q4cv/+DhSUsQIXmzoVFfxt/c+AiQCWMwj4PrsMpnzCERquSR1dt3TvbK/8zHxeC3zjz0k7f19jPaiW4C0J6pnc1eoUwiDus4sallqPtMvka4iy8/c/TCVK67LVvFCYNAzrKuCpxUTxtBBM2Jg1aYzuP1Xf46LX3YOM5Pj3LplKe+/dTPe1DSPfmMbqwYsS6vg25ilNY1/5kaefG6ve45e3931PO55s1Ru6tedpOZ+vwvgzUO3Im26ti2iFXHAIs55kQSg2ApM7pHKLgIUhSAjXPG4AoiSnJgOjCn27TnAt1seI2tXEQfJC6cF+n1DJZkKdqRjWL50gK0/+0b6lw1g6k2ao1OUjk7zT196gu/cvwNlAwarFmUtnXbMijPW8vlnDtOoN7OqHtIFcBIfngw2uz6ljKYw6NAF5tL/C0a/iw7z8K3I1GJbaJv07D+GILwEAuBgXpYD6PGJxd+5OcytQjGuTrfbTANVbgmA//nJr9DaejW+cvV+OnkVbUmBZy07pmLkhpcztGaEqNEkijpsf+4As62I5X2ac4cVTStEVqh3oL/kM37OVfzV1x7O7jOXofkjZOievI9CbjG6IgUpPGuP056zwOuJgr3erNwxxhBOrwAsKGlpFF94m0im0UWZyE2rJP/3uonUgAqSjOy5c7SnGTs6xu/eu52R665DQndJLTDgOZJ2DBwIYlTcRhMyOzPN3gPjeCU4q8+92ye0QsdYonbMkiuv53fueoTZ6Znuqh6y5RyTvik3e6/ophIGS9czUOB1UZASR56ec0K0XaB1g6cFhej0zAuY76bzHiKFFyXb7gN6HWIBTOWaVNglxX1uCNXTmm1PPctfH1asu/wCbNuNBA54bnqVFti5bQ+2U6dsG+zbc5Cj4w1W1RTra+n6Am6xp6HN5/H3hwJ27dzpTH+BmXMzrm6GT+4C8ighewFUxtx5AGDhsyCfTyXkO8a5L4kLKLYsJi5sSbfn4Kig7b0mk/xtO0V3IZK6A8Wdd93Ld5edy/qzVkNgqflQ01D2YeeOQ0wePYIXNdi39whBaNjcr9Fi3XyAOKa2ZCkPD2/k6/c8kJvtQqiadYWU0QX2ZSFsPiKYhX1Fj1F0bdkjnobA/gQtxWmeGnYirWASu04uuIYuU5kzPDevBVeRgkKlECwf/OQ/M3P1LZyxokZNDH0+VBTsHK3z/af3QjjD2NgUK8vCxn6XAyiJZWSgzOGzr+Qv7rofSKZzFVkjadqqoNUZ0yVnevqRuRo/F/2npDxhIs6l+wnKkLLWtk/8rifXpPBt5zCeHiwkWRiYahapJkFG8JwBZMcq7dGpz/Dr//Qww694DStrsLJiia3icGDZtecIdBrsOTjL2UOCxjBYhrYHj625jD968Dk69dmsqCPtT5GNFJieC2Y3Xsl9Fvn/veFj4c+C7F+sXPQYleM1ay3KGDu9+FOOcdMTlLxur9fjI4t0K/zoHSjJCA8Uwy4R0Erz/DPP8pHtLc688UY2Vw0TaDrAc4eaNKaaBM2ALQNQ9hTfnzJ8Tc7hnlFh1/MvdJvsgtXJ79mD8AvmnvS7SyjS5ys+EFmR6DH5dqxKoN7kzwnwwVorylpzZPGnzG0Llq+fyDXmcQMZCiiY2dw1JD42PbJgHTKmJGVZn/nsF7nLP5eBc89hXzOkz4Od+9rsODDDOt1Ae4q/2wN3N0ZYetbZPPjgQ1kPeow5JHkHEckmEuYCkD5IwR0UhLtbOQvAdrEquxCdT8JrgKOVMbFRcRw/d3KXKLRjSegiO1jUnnyD+1FQRPKMGtl3Mc6WtCIHQZIpVL/9v/+RHWdeS7lWRQTGZ9o8/lydqSZ85FnLtinh9ttv5iv3PuQWoU7G7AuyNY+f74nvUXRbA+l6lC5CHE9reml2gqZ9sS2O44MqjqOHT26ZGNeKAzwLH7D4llveXOO7iZ5rZ2oJ3HmSDBhJXpyBoJTH1MQE9z9/gJ+9+XL3bkkb8zcPjvHJHQHjLcP/+9M30ezUOXJ0rPAwBScl3czN08GFCt5ek58+SxHtF6zUcYm6GIbPJygn0OI4fkLFcfxtY+K4N7d9Qu0Efc/Cregv822ZeZX5hCInsiSLKWXMUTmjPn7Xg5SH+rl9ZYUIj1azhYksr7/uHM4a8PjoZ+/Ju1C8Rs91VCENnRxIVtmTMb7b98tiGb8QSY5F35Oku7WWMAzvVcD2MAy3ndxlFminbLKKoVXyf7pHigQumP9MYfMyKyHPDUStJp/aV+dHLtrEDUsU7VC4+KwVvGa55uPf3Uuz1SazLYWZvOmAU7E/mcCpQh+L0UmBDnPM/2LbfCN8vZfpZf4ihUFEiOOoY0x8twaMtXZFqVS+6cR7eay7nK7L5Ca5aFJT3CjkFchzOiAFhlh47uAY1124kUsmd1Nbs4JrSy0OzYZ86PFDxFGcaLtKZvPm07oyS1DQ7PmFsJf5i9CEE6HT6aKpCO12+74oij6sAKIo+kQURZ1TcQOLPvNkrGBX+NftT3OEXUTkPYBMFEpromaDL46HrN68iZtkgqU24gsz0Gm1M61XqUnvug6F7+K1u/tQ7Ncpo/tTPfZYl7GGIOh8DPJM4I5Op33HKV30uBuOs/04rdcE92ofhbAq8wYJRkjn5ivl8flvb+fJ2gqGbczO0iBf2z2d+PakyihlcLaaR2GSh+TCVzBBObNP1s+/hE1E6HSCncaYO6GQCg6C4PejKApPCQwuqgenenrBGnQJATk4SxlYsApKKbTWhI0Z7jjUZM2N1/HVTh9Rp11YuiXX/OKiTnl4KXM0PN2GnCbWv8jkt9bSbrf+AGhCtqYWAEeMiVeWy5WrXtwunHrLGA4JArddWpgfB6m65jhB2DM+g3/epXz83seII4NWDt07v1/0/ZJHG8UQcD4M8JI9/ck35/tb3w/D4JdIVlXu7fdIrdb/SKVSWX8quYGXqqV9zPtqs5cx5L/TVUrTV7NAbGIiA1psMnjEHEYX07v0MDsz+/9CGA+u31EUxbOz0zdYax9Mt/eOBk60Wo13xXFkTskVHOvU00ixbg1NLt4F2nIfrhI34FYhcdPJPc9zGq9zze+q+Stcv+v7eMxfCPz/AKXFWkuzWf+tIvOh2wWk7YUoitqlUumV6lirUZ1sO81EyEPwXDMzbSXbmR2TCYbqye6l8X7BBSjBzSZOsUCXEJxgJ3+ATURoNOp3hGH43t598wkA1ppvxXE0UiqVr37RQWHapOf7BE+eY6KLcXkmGCDF6txe/16M+7sGe3DjChkoXKDvx3u2hfad0rMf59YiNJuNBzqd9ltI3oNebPMKAIAx5q44jpeXSqWrFi0EL7Wk99xvTqho099pLqEYLeRInxTcFSMHkpAw+z6Fx+s9caELnW7r6Jh/f7vdeiMwNd8xCwoAgDHxl6MoFM/zbzpld3AiD3caQsUsTFsA0C30yfMAeXh3yl1arACcppYqbKNR/2yn034rCzAfjiMAAMaYe8IweFopdYPnef0vGjg81nEnCSrngMSeEG4u4s9zC6QW4wS6fTJ9PN0tQftRvT77m1EUvpd5zH6xHVcAAKy124Ig+KwxZrXW+kJnDX4IAiDhuDWJhaxADhCLPrc33qeYAPqX01LFbLdb32k26+8wxvzdos47iRvdVi5X318ul2/U2snPDzxn0CsItmfb/6UtZboxhiAInu10Wh+O4/gvgWDR1ziFm7/S90s/UyqVX+V53rIiRkgTMf/aknaahLHoft2bVqJ2EAT3B0Hnb40xnwMaJ9O1U22rEnxwk9bepVrrDUrpksrm0f1rO9XmspgGY4yJ4/hAHMdPRFF4fxzH9wKnVNL3fwAz/j5a2PSnwAAAAABJRU5ErkJggg==","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10001,"category"=> "SYS_BASE","remark"=> "系统名称","sort"=> 2,"status"=> 0,"configKey"=> "SQM_SYS_NAME","configValue"=> "橙石链溯源","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10002,"category"=> "SYS_BASE","remark"=> "系统版本","sort"=> 3,"status"=> 0,"configKey"=> "SQM_SYS_VERSION","configValue"=> "V1.2.0","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10003,"category"=> "SYS_BASE","remark"=> "系统版权","sort"=> 4,"status"=> 0,"configKey"=> "SQM_SYS_COPYRIGHT","configValue"=> "©Dual Engine Sci. & Tech 2023","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10004,"category"=> "SYS_BASE","remark"=> "系统版权链接地址","sort"=> 5,"status"=> 0,"configKey"=> "SQM_SYS_COPYRIGHT_URL","configValue"=> "https://www.sqm.la","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10005,"category"=> "SYS_BASE","remark"=> "登录验证码开关","sort"=> 8,"status"=> 0,"configKey"=> "SQM_SYS_DEFAULT_CAPTCHA_OPEN","configValue"=> "true","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10006,"category"=> "SYS_BASE","remark"=> "公网安备号","sort"=> 99,"status"=> 0,"configKey"=> "SQM_SYS_GA_BEIAN","configValue"=> "浙公网安备 33021202002010号","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10007,"category"=> "SYS_BASE","remark"=> "工信部备案号","sort"=> 99,"status"=> 0,"configKey"=> "SQM_SYS_MIITBEIAN","configValue"=> "浙ICP备10032030号","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10008,"category"=> "SYS_BASE","remark"=> "默认文件存储引擎","sort"=> 9,"status"=> 0,"configKey"=> "SQM_SYS_DEFAULT_FILE_ENGINE","configValue"=> "QINIU","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10009,"category"=> "SYS_BASE","remark"=> "系统描述","sort"=> 11,"status"=> 0,"configKey"=> "SQM_SYS_DEFAULT_DESCRRIPTION","configValue"=> "双擎码溯源是以区块链技术为依托，为有形商品或无形资产提供全渠道全生命周期的追溯、监管、营销、渠道管控等一站式服务的大规模商用溯源服务平台。已在农产品溯源、跨境商品溯源、冷链监管溯源、快消品溯源、工艺品溯源等多个领域有真实落地场景。","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10099,"category"=> "OTHER","remark"=> null,"sort"=> 99,"status"=> 0,"configKey"=> "SQM_SYS_DEFAULT_PASSWORD","configValue"=> "12345678","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10100,"category"=> "SYS_BASE","remark"=> null,"sort"=> 99,"status"=> 0,"configKey"=> "SQM_SYS_DEFAULT_SMS_ENGINE","configValue"=> "ALIYUN","extJson"=> null,"isDeleted"=> 0],
                     ["id"=> 10101,"category"=> "SYS_BASE","remark"=> null,"sort"=> 99,"status"=> 0,"configKey"=> "SQM_SYS_DEFAULT_EMAIL_ENGINE","configValue"=> "ALIYUN","extJson"=> null,"isDeleted"=> 0]]);
            }

        /**
         * 初始化用户数据
         * @return void
         * @throws \think\db\exception\DbException
         */
        private function insertUser()
        {
            $modal = SysUser::mk()->whereRaw('1=1')->findOrEmpty();
            $modal->isEmpty() && $modal->insert([
                'id'       => '10000',
                'category' => 'GLOBAL',
                'account' => 'superAdmin',
                'name' => '超级管理员',
                'password' => '7fef6171469e80d32c0559f88b377245'
            ]);
        }

    /**
     * 初始化系统菜单
     * @return void
     */
    private function insertMenu()
    {
        if (SysMenu::mk()->whereRaw('1=1')->findOrEmpty()->isEmpty()) {
            // 解析并初始化菜单数据
            $json = '[
    {
        "group_name": "业务",
        "name": "7073669158520492032",
        "title": "商户管控",
        "code": "",
        "icon": "apartment-outlined",
        "category": "MENU",
        "module": 10000,
        "menu_type": "CATALOG",
        "path": "/e4y8y7ib2p",
        "component": "",
        "subs": [
            {
                "name": "sysMerchant",
                "title": "商户管理",
                "code": "sys/merchant/page",
                "icon": "cluster-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/merchant",
                "component": "sys/merchant/index",
                "subs": [
                    {
                        "title": "新增商户",
                        "code": "sys/merchant/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "编辑商户",
                        "code": "sys/merchant/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取商户详情",
                        "code": "sys/merchant/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改商户状态",
                        "code": "sys/merchant/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除商户",
                        "code": "sys/merchant/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取商户列表",
                        "code": "sys/merchant/list",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "授权商户子系统",
                        "code": "sys/merchant/authorize",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "商户审核",
                        "code": "sys/merchant/toExamine",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "信息录入规范设置",
                        "code": "sys/merchant/infoType",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysUser",
                "title": "用户管理",
                "code": "sys/user/page",
                "icon": "user-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/user",
                "component": "sys/user/index",
                "subs": [
                    {
                        "title": "新增用户",
                        "code": "sys/user/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取用户详情",
                        "code": "sys/user/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "添加用户",
                        "code": "sys/user/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "更新用户",
                        "code": "sys/user/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改用户状态",
                        "code": "sys/user/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除用户",
                        "code": "sys/user/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取该用户的所有角色ID",
                        "code": "sys/user/ownRole",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取角色列表",
                        "code": "sys/user/roleSelector",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "用户分配角色",
                        "code": "sys/user/grantRole",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "用户资源授权",
                        "code": "sys/user/grantResource",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "用户授权权限",
                        "code": "sys/user/grantPermission",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "用户已授权的资源",
                        "code": "sys/user/ownResource",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "用户已授权的权限",
                        "code": "sys/user/ownPermission",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "重置密码",
                        "code": "sys/user/resetPassword",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取该用户的所有商户ID",
                        "code": "sys/user/ownMerchant",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取商户列表",
                        "code": "sys/user/merchantSelector",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "用户分配商户",
                        "code": "sys/user/grantMerchant",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "SysTenant",
                "title": "租户管理",
                "code": "sys/tenant/page",
                "icon": "cluster-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/tenant",
                "component": "sys/tenant/index",
                "subs": [
                    {
                        "title": "租户列表",
                        "code": "sys/tenant/list",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "新增租户",
                        "code": "sys/tenant/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "编辑租户",
                        "code": "sys/tenant/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "租户详情",
                        "code": "sys/tenant/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改租户状态",
                        "code": "sys/tenant/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "租户延期",
                        "code": "sys/tenant/extension",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "移到回收站",
                        "code": "sys/tenant/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "租户审核",
                        "code": "sys/tenant/toExamine",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "租户分配商户",
                        "code": "sys/tenant/grantMerchant",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "租户分配用户",
                        "code": "sys/tenant/grantUser",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取商户列表",
                        "code": "sys/tenant/merchantSelector",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取该租户的所有商户ID",
                        "code": "sys/tenant/ownMerchant",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取该租户的所有用户ID",
                        "code": "sys/tenant/ownUser",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7073669174270103552",
        "title": "权限管控",
        "code": "",
        "icon": "user-switch-outlined",
        "category": "MENU",
        "module": 10000,
        "menu_type": "CATALOG",
        "path": "/5k9uuuzafi",
        "component": "",
        "subs": [
            {
                "name": "sysRole",
                "title": "角色管理",
                "code": "sys/role/page",
                "icon": "deployment-unit-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/role",
                "component": "sys/role/index",
                "subs": [
                    {
                        "title": "获取角色详情",
                        "code": "sys/role/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "添加角色",
                        "code": "sys/role/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "编辑角色",
                        "code": "sys/role/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改角色状态",
                        "code": "sys/role/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除角色",
                        "code": "sys/role/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "查询角色已授权用户",
                        "code": "sys/role/ownUser",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "资源选择器",
                        "code": "sys/role/resourceTreeSelector",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "权限选择器",
                        "code": "sys/role/permissionTreeSelector",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "查询角色已授权的资源",
                        "code": "sys/role/ownResource",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "查询角色已授权的权限",
                        "code": "sys/role/ownPermission",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "组织树选择器",
                        "code": "sys/role/orgTreeSelector",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "主管选择器",
                        "code": "sys/role/userSelector",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "角色资源授权",
                        "code": "sys/role/grantResource",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "角色授权用户",
                        "code": "sys/role/grantUser",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "角色授权权限",
                        "code": "sys/role/grantPermission",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysModule",
                "title": "模块管理",
                "code": "sys/module/page",
                "icon": "appstore-add-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/module",
                "component": "sys/resource/module/index",
                "subs": [
                    {
                        "title": "获取模块详情",
                        "code": "sys/module/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "添加模块",
                        "code": "sys/module/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "更新模块",
                        "code": "sys/module/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改模块状态",
                        "code": "sys/module/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除模块",
                        "code": "sys/module/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysMenu",
                "title": "菜单管理",
                "code": "sys/menu/page",
                "icon": "pic-left-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/menu",
                "component": "sys/resource/menu/index",
                "subs": [
                    {
                        "title": "获取模块选择器",
                        "code": "sys/menu/moduleSelector",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "菜单所属模块更改",
                        "code": "sys/menu/changeModule",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取菜单选择树",
                        "code": "sys/menu/menuTreeSelector",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取菜单树",
                        "code": "sys/menu/tree",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取菜单详情",
                        "code": "sys/menu/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "添加菜单",
                        "code": "sys/menu/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "编辑菜单",
                        "code": "sys/menu/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改菜单状态",
                        "code": "sys/menu/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除菜单",
                        "code": "sys/menu/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "重写菜单数据",
                        "code": "sys/menu/reset",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "按钮列表",
                        "code": "sys/button/page",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取按钮详情",
                        "code": "sys/button/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "添加按钮",
                        "code": "sys/button/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "更改按钮",
                        "code": "sys/button/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改按钮状态",
                        "code": "sys/button/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除按钮",
                        "code": "sys/button/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysSpa",
                "title": "单页管理",
                "code": "sys/spa/page",
                "icon": "pic-center-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/spa",
                "component": "sys/resource/spa/index",
                "subs": [
                    {
                        "title": "获取单页详情",
                        "code": "sys/spa/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "新增单页",
                        "code": "sys/spa/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "更改单页",
                        "code": "sys/spa/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改单页状态",
                        "code": "sys/spa/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除单页",
                        "code": "sys/spa/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7073669190158127104",
        "title": "基础工具",
        "code": "",
        "icon": "tool-outlined",
        "category": "MENU",
        "module": 10000,
        "menu_type": "CATALOG",
        "path": "/ozmlc6eyw5",
        "component": "",
        "subs": [
            {
                "name": "devFile",
                "title": "文件管理",
                "code": "dev/file/page",
                "icon": "copy-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/dev/file",
                "component": "dev/file/index",
                "subs": [
                    {
                        "title": "文件列表",
                        "code": "dev/file/list",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取文件详情",
                        "code": "dev/file/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除文件",
                        "code": "dev/file/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "devEmail",
                "title": "邮件推送",
                "code": "sys/email/page",
                "icon": "send-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/dev/email",
                "component": "dev/email/index",
                "subs": [
                    {
                        "title": "获取邮件发送详情",
                        "code": "sys/email/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "本地发送文本类邮件",
                        "code": "sys/email/sendLocalTxt",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "本地发送HTML类邮件",
                        "code": "sys/email/sendLocalHtml",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "阿里云发送文本类邮件",
                        "code": "sys/email/sendAliyunTxt",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "阿里云发送HTML类邮件",
                        "code": "sys/email/sendAliyunHtml",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "阿里云发送模板类邮件",
                        "code": "sys/email/sendAliyunTmp",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "腾讯云发送文本类邮件",
                        "code": "sys/email/sendTencentTxt",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "腾讯云发送HTML类邮件",
                        "code": "sys/email/sendTencentHtml",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "腾讯云发送模板类邮件",
                        "code": "sys/email/sendTencentTmp",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除邮件",
                        "code": "sys/email/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "devSms",
                "title": "短信发送",
                "code": "sys/sms/page",
                "icon": "mail-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/dev/sms",
                "component": "dev/sms/index",
                "subs": [
                    {
                        "title": "发送阿里云短信",
                        "code": "sys/sms/sendAliyun",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "发送腾讯云短信",
                        "code": "sys/sms/sendTencent",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取短信详情",
                        "code": "sys/sms/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除短信",
                        "code": "sys/sms/detele",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "devMessage",
                "title": "站内信息",
                "code": "sys/message/page",
                "icon": "message-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/dev/message",
                "component": "dev/message/index",
                "subs": [
                    {
                        "title": "发送站内信",
                        "code": "sys/message/send",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取站内信详情",
                        "code": "sys/message/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除站内信",
                        "code": "sys/message/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysIndustry",
                "title": "行业分类",
                "code": "sys/industry/page",
                "icon": "usergroup-delete-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/industry",
                "component": "sys/industry/index",
                "subs": [
                    {
                        "title": "获取行业分类详情",
                        "code": "sys/industry/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "新增行业分类",
                        "code": "sys/industry/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "编辑行业分类",
                        "code": "sys/industry/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除行业分类",
                        "code": "sys/industry/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改行业分类状态",
                        "code": "sys/industry/state",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysPubliccate",
                "title": "商品分类",
                "code": "sys/publicCategory/page",
                "icon": "database-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/publiccate",
                "component": "sys/publiccate/index",
                "subs": [
                    {
                        "title": "获取商品公共分类详情",
                        "code": "sys/PublicCategory/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "添加商品公共分类",
                        "code": "sys/PublicCategory/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "更改商品公共分类",
                        "code": "sys/PublicCategory/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除商品公共分类",
                        "code": "sys/PublicCategory/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysGpc",
                "title": "GPC分类",
                "code": "sys/gpc/page",
                "icon": "database-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/gpc",
                "component": "sys/gpccate/index",
                "subs": [
                    {
                        "title": "获取GPC分类详情",
                        "code": "sys/gpc/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "添加GPC分类",
                        "code": "sys/gpc/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "更改GPC分类",
                        "code": "sys/gpc/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改GPC状态",
                        "code": "sys/gpc/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除GPC分类",
                        "code": "sys/gpc/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysArea",
                "title": "地区分类",
                "code": "sys/area/page",
                "icon": "file-word-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/area",
                "component": "sys/area/index",
                "subs": [
                    {
                        "title": "获取地区详情",
                        "code": "sys/area/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "新增地区",
                        "code": "sys/area/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "更改地区",
                        "code": "sys/area/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修改地区状态",
                        "code": "sys/area/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除地区状态",
                        "code": "sys/area/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "devStandardCode",
                "title": "标准代码",
                "code": "sys/StandardCode/page",
                "icon": "copyright-circle-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/standardcode",
                "component": "sys/standardcode/index",
                "subs": [
                    {
                        "title": "获取标准代码列表",
                        "code": "sys/StandardCode/list",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "标准代码新增",
                        "code": "sys/StandardCode/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "标准代码编辑",
                        "code": "sys/StandardCode/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "标准代码删除",
                        "code": "sys/StandardCode/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取标准代码详情",
                        "code": "sys/StandardCode/detail",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysWorldCode",
                "title": "国家代码",
                "code": "sys/WorldCode/page",
                "icon": "global-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/worldCode",
                "component": "sys/worldcode/index",
                "subs": [
                    {
                        "title": "新增代码",
                        "code": "sys/WorldCode/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "编辑代码",
                        "code": "sys/WorldCode/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取详情",
                        "code": "sys/WorldCode/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "代码状态更改",
                        "code": "sys/WorldCode/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "国家代码列表",
                        "code": "sys/WorldCode/list",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "批量上传",
                        "code": "sys/WorldCode/importData",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "国家代码删除",
                        "code": "sys/WorldCode/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7157665573772464128",
        "title": "微信管理",
        "code": null,
        "icon": "message-outlined",
        "category": "MENU",
        "module": 10000,
        "menu_type": "CATALOG",
        "path": "/7157665573772464128",
        "component": null,
        "subs": [
            {
                "name": "sysWechatFans",
                "title": "粉丝管理",
                "code": "sys/wechat.fans/index",
                "icon": "user-switch-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/wechat/fans",
                "component": "sys/wechat/fans/index"
            },
            {
                "name": "sysWechatNews",
                "title": "图文管理",
                "code": "sys/wechat.news/index",
                "icon": "snippets-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/wechat/news",
                "component": "sys/wechat/news/index"
            },
            {
                "name": "sysWechatMenu",
                "title": "菜单配置",
                "code": "sys/wechat.menu/index",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/wechat/menu",
                "component": "sys/wechat/menu/index"
            },
            {
                "name": "sysWechatKeys",
                "title": "回复规则",
                "code": "sys/wechat.keys/index",
                "icon": "play-circle-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/wechat/keys",
                "component": "sys/wechat/keys/index"
            },
            {
                "name": "sysWechatAuto",
                "title": "自动回复",
                "code": "sys/wechat.auto/index",
                "icon": "check-circle-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/wechat/auto",
                "component": "sys/wechat/auto/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7073669208290103296",
        "title": "系统运维",
        "code": "",
        "icon": "hdd-outlined",
        "category": "MENU",
        "module": 10000,
        "menu_type": "CATALOG",
        "path": "/a0l7fxfq3m",
        "component": "",
        "subs": [
            {
                "name": "devdict",
                "title": "数据字典",
                "code": "sys/dict/page",
                "icon": "file-search-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/dict",
                "component": "dev/dict/index",
                "subs": [
                    {
                        "title": "字典分页列表",
                        "code": "sys/dict/page",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "字典列表",
                        "code": "sys/dict/list",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "字典新增",
                        "code": "sys/dict/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "字典编辑",
                        "code": "sys/dict/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "字典删除",
                        "code": "sys/dict/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取字典详情",
                        "code": "sys/dict/tree",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "devConfig",
                "title": "系统配置",
                "code": "sys/config/page",
                "icon": "setting-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/config",
                "component": "dev/config/index",
                "subs": [
                    {
                        "title": "获取其他配置分页列表",
                        "code": "sys/config/page",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取配置列表",
                        "code": "sys/config/list",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取配置详情",
                        "code": "sys/config/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "新增配置",
                        "code": "sys/config/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "编辑配置",
                        "code": "sys/config/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除配置",
                        "code": "sys/config/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "批量更新配置",
                        "code": "sys/config/editBatch",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysQueue",
                "title": "任务管理",
                "code": "sys/queue/page",
                "icon": "field-time-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/queue",
                "component": "sys/queue/index",
                "subs": [
                    {
                        "title": "系统任务管理分页列表",
                        "code": "sys/queue/page",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "重启系统任务",
                        "code": "sys/queue/redo",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "清理运行数据",
                        "code": "sys/queue/clear",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除系统任务",
                        "code": "sys/queue/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "停止监听服务",
                        "code": "sys/queue/stop",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "启动监听服务",
                        "code": "sys/queue/start",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "检查监听服务",
                        "code": "sys/queue/status",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "查询任务进度",
                        "code": "sys/queue/progress",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysOplog",
                "title": "系统日志",
                "code": "sys/oplog/page",
                "icon": "robot-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/oplog",
                "component": "sys/oplog/index",
                "subs": [
                    {
                        "title": "系统日志分页列表",
                        "code": "sys/oplog/page",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "清理系统日志",
                        "code": "sys/oplog/clear",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除系统日志",
                        "code": "sys/oplog/delete",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysDatabase",
                "title": "数据库管理",
                "code": "sys/database/page",
                "icon": "team-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/database",
                "component": "sys/database/index",
                "subs": [
                    {
                        "title": "数据库分页列表",
                        "code": "sys/database/page",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "数据表详情",
                        "code": "sys/database/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "数据表优化",
                        "code": "sys/database/optimize",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "修复所有数据表",
                        "code": "sys/database/repair",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "清理表碎片",
                        "code": "sys/database/fragment",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "数据库打包",
                        "code": "sys/database/packages",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7086336017770221568",
        "title": "移动端管理",
        "code": "",
        "icon": "mobile-outlined",
        "category": "MENU",
        "module": 10000,
        "menu_type": "CATALOG",
        "path": "/7086336017770221568",
        "component": "",
        "subs": [
            {
                "name": "mobileMenuIndex",
                "title": "菜单管理",
                "code": "sys/MobileMenu/page",
                "icon": "appstore-add-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/mobile/menu/index",
                "component": "mobile/resource/menu/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066387589372055552",
        "title": "商户管理",
        "code": "",
        "icon": "credit-card-outlined",
        "category": "",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7066387589372055552",
        "component": "",
        "subs": [
            {
                "name": "bizUniversalMerchant",
                "title": "商户管理",
                "code": "biz/merchant/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/merchant",
                "component": "biz/merchants/merchant/index",
                "subs": [
                    {
                        "title": "新增商户",
                        "code": "biz/merchant/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑商户",
                        "code": "biz/merchant/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取一个商户详情",
                        "code": "biz/merchant/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除商户",
                        "code": "biz/merchant/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取商户列表",
                        "code": "biz/merchant/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改商户状态",
                        "code": "biz/merchant/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "授权商户子系统",
                        "code": "biz/merchant/authorize",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "商户审核",
                        "code": "biz/merchant/toExamine",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "信息录入规范设置",
                        "code": "biz/merchant/infoType",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "签署承诺书",
                        "code": "biz/merchant/commitment",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalSupplier",
                "title": "供应商管理",
                "code": "biz/supplier/page",
                "icon": "user-switch-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/supplier",
                "component": "biz/merchants/supplier/index",
                "subs": [
                    {
                        "title": "获取供应商详情",
                        "code": "biz/supplier/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增供应商",
                        "code": "biz/supplier/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑供应商",
                        "code": "biz/supplier/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除供应商",
                        "code": "biz/supplier/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取供应商列表",
                        "code": "biz/supplier/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改供应商状态",
                        "code": "biz/supplier/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalDistributor",
                "title": "经销商管理",
                "code": "biz/distributor/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/distributor",
                "component": "biz/merchants/distributor/index",
                "subs": [
                    {
                        "title": "获取经销商详情",
                        "code": "biz/distributor/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增经销商",
                        "code": "biz/distributor/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑经销商",
                        "code": "biz/distributor/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除经销商",
                        "code": "biz/distributor/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "批量导入经销商",
                        "code": "biz/distributor/importData",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取经销商列表",
                        "code": "biz/distributor/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改经销商状态",
                        "code": "biz/distributor/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalEmployees",
                "title": "员工管理",
                "code": "biz/employees/page",
                "icon": "usergroup-add-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/employees",
                "component": "biz/merchants/employees/index",
                "subs": [
                    {
                        "title": "获取员工列表",
                        "code": "biz/employees/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增员工",
                        "code": "biz/employees/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑员工",
                        "code": "biz/employees/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除员工",
                        "code": "biz/employees/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取员工详情",
                        "code": "biz/employees/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改员工关态",
                        "code": "biz/employees/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066706762136555520",
        "title": "商品管理",
        "code": "",
        "icon": "picture-outlined",
        "category": "",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7066706762136555520",
        "component": "",
        "subs": [
            {
                "name": "goodsCate",
                "title": "商品分类",
                "code": "biz/cate/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/cate",
                "component": "biz/goods/cate/index",
                "subs": [
                    {
                        "title": "获取菜单树",
                        "code": "biz/cate/tree",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "菜单列表",
                        "code": "biz/cate/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增菜单",
                        "code": "biz/cate/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑菜单",
                        "code": "biz/cate/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取菜单详情",
                        "code": "biz/cate/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除菜单",
                        "code": "biz/cate/delete",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalGoods",
                "title": "商品管理",
                "code": "biz/goods/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/goods",
                "component": "biz/goods/commodity/index",
                "subs": [
                    {
                        "title": "获取商品详情",
                        "code": "biz/goods/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增商品",
                        "code": "biz/goods/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑商品",
                        "code": "biz/goods/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除商品",
                        "code": "biz/goods/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取商品列表",
                        "code": "biz/goods/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改商品状态",
                        "code": "biz/goods/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizGoodsInfo",
                "title": "商品信息配置",
                "code": "biz/batch.goodsInfo/page",
                "icon": "diff-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/batch/goodsInfoConfig",
                "component": "sub/public/trace/goodsInfoConfig"
            },
            {
                "name": "bizUniversalGoodsBrand",
                "title": "品牌管理",
                "code": "biz/brand/page",
                "icon": "trademark-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/brand",
                "component": "biz/goods/brand/index",
                "subs": [
                    {
                        "title": "获取商品品牌详情",
                        "code": "biz/brand/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "商品品牌新增",
                        "code": "biz/brand/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "商品品牌编辑",
                        "code": "biz/brand/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "商品品牌删除",
                        "code": "biz/brand/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改品牌状态",
                        "code": "biz/brand/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取品牌列表",
                        "code": "biz/brand/list",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalRawMaterial",
                "title": "原料管理",
                "code": "biz/RawMaterial/page",
                "icon": "block-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/material",
                "component": "biz/goods/material/index",
                "subs": [
                    {
                        "title": "获取原料列表",
                        "code": "biz/RawMaterial/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取原料详情",
                        "code": "biz/RawMaterial/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增原料",
                        "code": "biz/RawMaterial/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑原料",
                        "code": "biz/RawMaterial/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除原料",
                        "code": "biz/RawMaterial/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/RawMaterial/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalGoodsCerification",
                "title": "商品认证",
                "code": "biz/cerification/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/cerification",
                "component": "biz/goods/cerification/index",
                "subs": [
                    {
                        "title": "商品认证列表",
                        "code": "biz/cerification/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取商品认证详情",
                        "code": "biz/cerification/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增商品认证",
                        "code": "biz/cerification/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑商品认证",
                        "code": "biz/cerification/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除商品认证",
                        "code": "biz/cerification/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/cerification/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalBom",
                "title": "物料清单",
                "code": "biz/bom/page",
                "icon": "ordered-list-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/bom",
                "component": "biz/goods/bom/index",
                "subs": [
                    {
                        "title": "获取物料清单列表",
                        "code": "biz/bom/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取物料清单详情",
                        "code": "biz/bom/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增物料清单",
                        "code": "biz/bom/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑物料清单",
                        "code": "biz/bom/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除物料清单",
                        "code": "biz/bom/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/bom/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalShop",
                "title": "店铺管理",
                "code": "biz/shop/page",
                "icon": "shop-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/shop",
                "component": "biz/goods/shop/index",
                "subs": [
                    {
                        "title": "获取店铺列表",
                        "code": "biz/shop/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增店铺",
                        "code": "biz/shop/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑店铺",
                        "code": "biz/shop/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除店铺",
                        "code": "biz/shop/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取店铺详情",
                        "code": "biz/shop/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/shop/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizHyperInfo",
                "title": "环节信息配置",
                "code": "biz/batch.hyper/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/batch/hyperInfo",
                "component": "sub/public/trace/hyperInfo",
                "subs": [
                    {
                        "title": "新增环节",
                        "code": "/biz/hyper/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑环节",
                        "code": "/biz/hyper/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除环节",
                        "code": "/biz/hyper/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "/biz/hyper/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "环节列表",
                        "code": "/biz/hyper/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "环节详情",
                        "code": "/biz/hyper/detail",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7068831409917399040",
        "title": "原料采购",
        "code": "",
        "icon": "login-outlined",
        "category": "",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7068831409917399040",
        "component": "",
        "subs": [
            {
                "name": "bizUniversalProductMateral",
                "title": "原料采购",
                "code": "biz/product.material/page",
                "icon": "plus-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/product/material",
                "component": "sub/universal/product/material/index",
                "subs": [
                    {
                        "title": "获取原料采购列表",
                        "code": "biz/product.material/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取原料采购详情",
                        "code": "biz/product.material/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增原料采购",
                        "code": "biz/product.material/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑原料采购",
                        "code": "biz/product.material/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除原料采购",
                        "code": "biz/product.material/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "上链",
                        "code": "biz/product.material/upChain",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改原料采购状态",
                        "code": "biz/product.material/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/product.material/freeze",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalProductMaterialBatch",
                "title": "原料批次",
                "code": "biz/product.MaterialItem/page",
                "icon": "pic-right-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/product/material/batch",
                "component": "sub/universal/product/material/batch",
                "subs": [
                    {
                        "title": "获取原料批次列表",
                        "code": "biz/product.MaterialItem/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取原料批次详情（预览）",
                        "code": "biz/product.MaterialItem/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改原料批次状态（冻结）",
                        "code": "biz/product.MaterialItem/freeze",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7070765572912320512",
        "title": "生产管理",
        "code": "",
        "icon": "scissor-outlined",
        "category": "",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7070765572912320512",
        "component": "",
        "subs": [
            {
                "name": "bizUniversalProductProduceIn",
                "title": "生产入库",
                "code": "biz/trace.ProduceIn/page",
                "icon": "build-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/product/produce/in",
                "component": "sub/universal/product/produceIn/index",
                "subs": [
                    {
                        "title": "获取生产入库信息列表",
                        "code": "biz/trace.ProduceIn/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取生产入库详情",
                        "code": "biz/trace.ProduceIn/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增生产入库",
                        "code": "biz/trace.ProduceIn/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑生产入库",
                        "code": "biz/trace.ProduceIn/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除生产入库",
                        "code": "biz/trace.ProduceIn/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改生产入库状态（冻结，解冻）",
                        "code": "biz/trace.ProduceIn/freeze",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.ProduceIn/upChain",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalProductProduceInBatch",
                "title": "生产批次",
                "code": "biz/trace.ProduceInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/product/produce/in/batch",
                "component": "sub/universal/product/produceIn/batch",
                "subs": [
                    {
                        "title": "获取生产批次列表",
                        "code": "biz/trace.ProduceInItem/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改生产入库批次状态（冻结，解冻）",
                        "code": "biz/trace.ProduceInItem/freeze",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取生产入库批次详情",
                        "code": "biz/trace.ProduceInItem/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "开启店铺",
                        "code": "biz/trace.ProduceInItem/openShop",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "关闭店铺",
                        "code": "biz/trace.ProduceInItem/closeShop",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "冻结",
                        "code": "biz/trace.ProduceInItem/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "添加商品入库批次",
                        "code": "biz/trace.ProduceInItem/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更新商品入库批次",
                        "code": "biz/trace.ProduceInItem/edit",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7070752739495514112",
        "title": "入库管理",
        "code": "",
        "icon": "plus-square-outlined",
        "category": "",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/biz/trace/stock/in",
        "component": "",
        "subs": [
            {
                "name": "bizUniversalProductStockIn",
                "title": "入库管理",
                "code": "biz/trace.StockIn/page",
                "icon": "arrow-right-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/product/stock/in",
                "component": "sub/universal/product/stockIn/index",
                "subs": [
                    {
                        "title": "获取商品入库信息列表",
                        "code": "biz/trace.StockIn/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取商品入库详情",
                        "code": "biz/trace.StockIn/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "添加商品入库",
                        "code": "biz/trace.StockIn/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑商品入库",
                        "code": "biz/trace.StockIn/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除入库信息",
                        "code": "biz/trace.StockIn/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockIn/upChain",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改商品入库状态（冻结，解冻）",
                        "code": "biz/trace.StockIn/freeze",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizUniversalProductStockInBatch",
                "title": "入库批次",
                "code": "biz/trace.StockInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/product/stock/in/batch",
                "component": "sub/universal/product/stockIn/batch",
                "subs": [
                    {
                        "title": "获取入库批次列表",
                        "code": "biz/trace.StockInItem/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改商品入库批次状态（冻结，解冻）",
                        "code": "biz/trace.StockInItem/freeze",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取入库批次详情",
                        "code": "biz/trace.StockInItem/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "开启店铺",
                        "code": "biz/trace.StockInItem/openShop",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "关闭店铺",
                        "code": "biz/trace.StockInItem/closeShop",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/trace.StockInItem/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "添加商品入库批次",
                        "code": "biz/trace.StockInItem/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更新商品入库批次",
                        "code": "biz/trace.StockInItem/edit",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7082604060104200192",
        "title": "批次管理",
        "code": "",
        "icon": "pic-left-outlined",
        "category": "MENU",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7082604060104200192",
        "component": "",
        "subs": [
            {
                "name": "bizBatch",
                "title": "批次管理",
                "code": "biz/batch/page",
                "icon": "pic-right-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/batch",
                "component": "biz/batch/index",
                "subs": [
                    {
                        "title": "新增批次",
                        "code": "biz/batch/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑批次",
                        "code": "biz/batch/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "批次详情",
                        "code": "biz/batch/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除批次",
                        "code": "biz/batch/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/batch/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "批次列表",
                        "code": "biz/batch/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "开启店铺",
                        "code": "biz/batch/openShop",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "关闭店铺",
                        "code": "biz/batch/closeShop",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "binding",
                "title": "关联批次",
                "code": "biz/bindingRange/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/binding",
                "component": "biz/batch/binding/index",
                "subs": [
                    {
                        "title": "新增起止号关联",
                        "code": "biz/bindingRange/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑起止号关联",
                        "code": "biz/bindingRange/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除起止号关联",
                        "code": "biz/bindingRange/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "起止号关联状态改变",
                        "code": "biz/bindingRange/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bindingSingle",
                "title": "关联批次（单）",
                "code": "biz/bindingSingle/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "biz/binding/single",
                "component": "biz/batch/binding/bindingSingle",
                "subs": [
                    {
                        "title": "新增逐个关联",
                        "code": "biz/bindingSingle/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑逐个关联",
                        "code": "bizbindingSingle/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除逐个关联",
                        "code": "biz/bindingSingle/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "逐个关联状态改变",
                        "code": "biz/bindingSingle/state",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7075384520802832384",
        "title": "检验检疫",
        "code": "",
        "icon": "bug-outlined",
        "category": "MENU",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7075384520802832384",
        "component": "",
        "subs": [
            {
                "name": "bizUniversalQuarantineReport",
                "title": "检验检疫",
                "code": "biz/QuarantineReport/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/QuarantineReport",
                "component": "biz/quarantineReport/index",
                "subs": [
                    {
                        "title": "获取检验检疫列表",
                        "code": "biz/QuarantineReport/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增检验检疫",
                        "code": "biz/QuarantineReport/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑检验检疫",
                        "code": "biz/QuarantineReport/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除检验检疫",
                        "code": "biz/QuarantineReport/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取检验检疫详情",
                        "code": "biz/QuarantineReport/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "激活检验检疫",
                        "code": "biz/QuarantineReport/freeze",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "上链",
                        "code": "biz/QuarantineReport/upChain",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7070754898417029120",
        "title": "出库管理",
        "code": "",
        "icon": "minus-square-outlined",
        "category": "",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7070754898417029120",
        "component": "",
        "subs": [
            {
                "name": "bizUniversalProductStockOut",
                "title": "出库管理",
                "code": "biz/trace.StockOut/page",
                "icon": "rollback-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/universal/product/stock/out",
                "component": "sub/universal/product/stockOut/index",
                "subs": [
                    {
                        "title": "获取商品出库信息列表",
                        "code": "biz/trace.StockOut/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取商品出库详情",
                        "code": "biz/trace.StockOut/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "添加商品出库",
                        "code": "biz/trace.StockOut/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑出库信息",
                        "code": "biz/trace.StockOut/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除出库信息",
                        "code": "biz/trace.StockOut/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改出库状态",
                        "code": "biz/trace.StockOut/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/trace.StockOut/freeze",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockOut/upChain",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082602944318672896",
        "title": "渠道管理",
        "code": "",
        "icon": "swap-outlined",
        "category": "MENU",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7082602944318672896",
        "component": "",
        "subs": [
            {
                "name": "bizSuspectedSmuggling",
                "title": "疑似窜货",
                "code": "biz/FleeingGoods/page",
                "icon": "strikethrough-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/distribution/channel/suspectedSmuggling",
                "component": "biz/distribution/channel/suspectedSmuggling/index",
                "subs": [
                    {
                        "title": "疑似窜货列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "窜货详情",
                        "code": "biz/FleeingGoods/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除窜货",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizStatistics",
                "title": "窜货统计",
                "code": "biz/channel.statistics/page",
                "icon": "project-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/distribution/channel/statistics",
                "component": "biz/distribution/channel/statistics/index",
                "subs": [
                    {
                        "title": "窜货统计列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除窜货统计",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082603389485322240",
        "title": "客群洞察",
        "code": "",
        "icon": "branches-outlined",
        "category": "MENU",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7082603389485322240",
        "component": "",
        "subs": [
            {
                "name": "bizDataDashboard",
                "title": "客群洞察",
                "code": "biz/data.dashboard/page",
                "icon": "deployment-unit-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/distribution/data/dashboard",
                "component": "biz/distribution/data/dashboard/index"
            },
            {
                "name": "bizDataScanQr",
                "title": "扫码统计",
                "code": "biz/data.scanQr/page",
                "icon": "scan-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/distribution/data/scanQr",
                "component": "biz/distribution/data/scanQr/index",
                "subs": [
                    {
                        "title": "扫码统计列表",
                        "code": "biz/ScanLogs/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/ScanLogs/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除扫码统计",
                        "code": "biz/ScanLogs/delete",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizDataSellTrend",
                "title": "销售趋势",
                "code": "biz/data.sellTrend/page",
                "icon": "monitor-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/distribution/data/sellTrend",
                "component": "biz/distribution/data/sellTrend/index"
            }
        ]
    },
    {
        "group_name": "码",
        "name": "7082598728053297152",
        "title": "标识管理",
        "code": "",
        "icon": "qrcode-outlined",
        "category": "MENU",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7082598728053297152",
        "component": "",
        "subs": [
            {
                "name": "bizCodeRule",
                "title": "生码管理",
                "code": "biz/codeRule/page",
                "icon": "holder-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/code/mark/rule",
                "component": "biz/code/mark/rule/index",
                "subs": [
                    {
                        "title": "码规则列表",
                        "code": "biz/CodeRule/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增码规则",
                        "code": "biz/CodeRule/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑码规则",
                        "code": "biz/CodeRule/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取详情",
                        "code": "biz/CodeRule/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除码规则",
                        "code": "biz/CodeRule/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/CodeRule/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "码规则审核",
                        "code": "biz/CodeRule/toExamine",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizCreateCode",
                "title": "生码纪录",
                "code": "biz/issue/page",
                "icon": "clock-circle-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/createCode",
                "component": "biz/code/mark/create/index",
                "subs": [
                    {
                        "title": "获取生码列表",
                        "code": "biz/issue/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "导入码包",
                        "code": "biz/ImportCode/importCodeRelation",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "扫码关联批次",
                        "code": "biz/ImportCode/scanCodeRelation",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/issue/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "生码详情",
                        "code": "biz/issue/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "生码",
                        "code": "biz/issue/crtQr",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "码下载",
                        "code": "biz/issue/down",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizOverdue",
                "title": "报损纪录",
                "code": "biz/overdue/page",
                "icon": "menu-fold-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/code/mark/overdue",
                "component": "biz/code/mark/overdue/index",
                "subs": [
                    {
                        "title": "导入报损",
                        "code": "biz/overdue/importBadCode",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "扫码报损",
                        "code": "biz/overdue/scanCode",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取报损详情",
                        "code": "biz/overdue/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除报损",
                        "code": "biz/overdue/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "报损列表",
                        "code": "biz/overdue/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "号段报损",
                        "code": "biz/overdue/scanCodeRange",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizCodeSearchIdentification",
                "title": "查询标识",
                "code": "biz/code.searchIdentification/page",
                "icon": "zoom-in-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/code/mark/searchIdentification",
                "component": "biz/code/mark/searchIdentification/index"
            },
            {
                "name": "bizCodePackage",
                "title": "包装层级",
                "code": "biz/package/page",
                "icon": "pic-left-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/code/mark/package",
                "component": "biz/code/mark/package/index",
                "subs": [
                    {
                        "title": "获取包装层级列表",
                        "code": "biz/package/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取包装层级详情",
                        "code": "biz/package/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "添加包装层级",
                        "code": "biz/package/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更新包装层级",
                        "code": "biz/package/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "修改包装层级状态",
                        "code": "biz/package/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除包装层级",
                        "code": "biz/package/delete",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "码",
        "name": "7227247203742388224",
        "title": "物料服务",
        "code": "",
        "icon": "file-pdf-outlined",
        "category": "MENU",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7227247203742388224",
        "component": "",
        "subs": [
            {
                "name": "printLabel",
                "title": "印码服务",
                "code": "biz/label.PrintOrder/page",
                "icon": "file-pdf-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/label/print/index",
                "component": "biz/label/print/index",
                "subs": [
                    {
                        "title": "印码服务列表",
                        "code": "biz/label.PrintOrder/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增印码服务",
                        "code": "biz/label.PrintOrder/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑印码服务",
                        "code": "biz/label.PrintOrder/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改印码服务状态",
                        "code": "biz/label.PrintOrder/status",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "生成PDF合同",
                        "code": "biz/label.PrintOrder/createContract",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "订单状态进度处理",
                        "code": "biz/label.PrintOrder/setProcessStatus",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "印码服务删除",
                        "code": "biz/label.PrintOrder/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取订单详情",
                        "code": "biz/label.PrintOrder/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "创建开票信息",
                        "code": "biz/label.PrintOrder/invoice",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "发货",
                        "code": "biz/label.PrintOrder/express",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "发票上传",
                        "code": "biz/label.OrderItem/invoicing",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "合同上传",
                        "code": "biz/label.OrderItem/contract",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "labelForm",
                "title": "新增印码订单",
                "code": "biz/label.PrintOrder/add",
                "icon": "",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/label/print/form",
                "component": "biz/label/print/step-form/index"
            },
            {
                "name": "processSheet",
                "title": "物料工艺",
                "code": "biz/label.process/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/label/process",
                "component": "biz/label/processSheet/index",
                "subs": [
                    {
                        "title": "工艺单列表",
                        "code": "biz/label.process/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增物料工艺",
                        "code": "biz/label.process/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑物料工艺",
                        "code": "biz/label.process/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取物料工艺详情",
                        "code": "biz/label.process/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "修改物流工艺状态",
                        "code": "biz/label.process/status",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "物料工艺报价",
                        "code": "biz/label.process/quotation",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "物料工艺查看",
                        "code": "biz/label.process/view",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "物料工艺审核",
                        "code": "biz/label.process/toExamine",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "labelOrder",
                "title": "标签订购",
                "code": "biz/label.order/page",
                "icon": "shopping-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/label/order/index",
                "component": "biz/label/order/index",
                "subs": [
                    {
                        "title": "标签订购列表",
                        "code": "biz/label.order/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增标签订购订单",
                        "code": "biz/label.order/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "修改标签订购订单",
                        "code": "biz/label.order/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "审核标签订购订单",
                        "code": "biz/label.order/toExamine",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "订单状态进度更新",
                        "code": "biz/label.order/setProcessStatus",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "订单发货",
                        "code": "biz/label.order/express",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "订单开票信息更新",
                        "code": "biz/label.order/invoice",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "订单开票（上传发票）",
                        "code": "biz/label.order/invoicing",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "订单删除",
                        "code": "biz/label.order/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取订单详情",
                        "code": "biz/label.order/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "生成PDF合同",
                        "code": "biz/label.order/createContract",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "合同上传",
                        "code": "biz/label.order/contract",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "labelOrderAdd",
                "title": "新增通用标订单",
                "code": "biz/label.order/add",
                "icon": "plus-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/label/order/form",
                "component": "biz/label/order/step-form/index"
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082601245256454144",
        "title": "配置管理",
        "code": "",
        "icon": "form-outlined",
        "category": "MENU",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7082601245256454144",
        "component": "",
        "subs": [
            {
                "name": "bizCheck",
                "title": "检测报告模板",
                "code": "biz/template.check/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/checkTemplate",
                "component": "biz/configue/config/check/index",
                "subs": [
                    {
                        "title": "新增检测报告模板",
                        "code": "biz/template.check/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑检测报告模板",
                        "code": "biz/template.check/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "检测报告模板列表",
                        "code": "biz/template.check/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "检测报告模板删除",
                        "code": "biz/template.check/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改检测报告模板状态",
                        "code": "biz/template.check/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "检测报告模板详情",
                        "code": "biz/template.check/detail",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizInfoTemplate",
                "title": "溯源模板管理",
                "code": "biz/template.info/page",
                "icon": "retweet-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/infoTemplate",
                "component": "biz/configue/config/info/index",
                "subs": [
                    {
                        "title": "新增溯源模板管理",
                        "code": "biz/template.info/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑溯源模板管理",
                        "code": "biz/template.info/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "溯源模板删除",
                        "code": "biz/template.info/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "溯源模板列表",
                        "code": "biz/template.info/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改溯源模板状态",
                        "code": "biz/template.info/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取溯源模板详情",
                        "code": "biz/template.info/detail",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizApplet",
                "title": "小程序模板管理",
                "code": "biz/appletTemplate/page",
                "icon": "fullscreen-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/config/applet",
                "component": "biz/configue/config/applet/index",
                "subs": [
                    {
                        "title": "获取小程序模板列表",
                        "code": "biz/page/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增小程序模板",
                        "code": "biz/page/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑小程序模板",
                        "code": "biz/page/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除小程序模板",
                        "code": "biz/page/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取小程序模板详情",
                        "code": "biz/page/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "设为默认小程序模板",
                        "code": "biz/page/setDefault",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizConfigTemplateInfo",
                "title": "溯源模板配置",
                "code": "biz/infoTemplate/list",
                "icon": "border-outer-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/configue/InfoConfig",
                "component": "biz/configue/config/info/info",
                "subs": [
                    {
                        "title": "新增溯源模板",
                        "code": "biz/template.info/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑溯源模板",
                        "code": "biz/template.info/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除溯源环节模板",
                        "code": "biz/template.info/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取溯源环节模板详情",
                        "code": "biz/template.info/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取溯源模板列表",
                        "code": "biz/template.info/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取环节列表",
                        "code": "biz/hyper/list",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "新增环节",
                        "code": "biz/hyper/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑环节",
                        "code": "biz/hyper/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "删除环节",
                        "code": "biz/hyper/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "获取环节详情",
                        "code": "biz/hyper/detail",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizAppletForm",
                "title": "新增编辑小程序模板",
                "code": "biz/applet/save",
                "icon": "code-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/config/applet/form",
                "component": "biz/configue/config/applet/form"
            },
            {
                "name": "bizCertificate",
                "title": "溯源链证书配置",
                "code": "biz/template.blockchain/detail",
                "icon": "book-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/configue/certificate",
                "component": "biz/configue/config/certificate/index",
                "subs": [
                    {
                        "title": "添加区块链模板",
                        "code": "biz/template.blockchain/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更新区块链模板",
                        "code": "biz/template.blockchain/edit",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082602400019648512",
        "title": "账号管理",
        "code": "",
        "icon": "team-outlined",
        "category": "MENU",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7082602400019648512",
        "component": "",
        "subs": [
            {
                "name": "bizAccountMember",
                "title": "成员管理",
                "code": "biz/account/page",
                "icon": "user-add-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/account/member",
                "component": "biz/configue/newAccount/member/index",
                "subs": [
                    {
                        "title": "新增成员",
                        "code": "biz/account/add",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "编辑成员",
                        "code": "biz/account/edit",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "成员详情",
                        "code": "biz/account/detail",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/account/state",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "成员删除",
                        "code": "biz/account/delete",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "成员列表",
                        "code": "biz/account/list",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "bizAccountAuthorization",
                "title": "权限管理",
                "code": "biz/authorization/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/account/authorization",
                "component": "biz/configue/newAccount/member/authorization",
                "subs": [
                    {
                        "title": "分配权限",
                        "code": "biz/authorization/auth",
                        "category": "BUTTON",
                        "module": 10002
                    },
                    {
                        "title": "角色列表",
                        "code": "biz/authorization/getRoleList",
                        "category": "BUTTON",
                        "module": 10002
                    }
                ]
            },
            {
                "name": "configDevelop",
                "title": "开发者管理",
                "code": "biz/develop/page",
                "icon": "console-sql-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/account/api",
                "component": "biz/configue/newAccount/develop/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066387589372055552",
        "title": "商户管理",
        "code": "",
        "icon": "shop-outlined",
        "category": "",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7066387589372055552",
        "component": "",
        "subs": [
            {
                "name": "bizImportsMerchant",
                "title": "商户管理",
                "code": "biz/merchant/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/merchant",
                "component": "biz/merchants/merchant/index",
                "subs": [
                    {
                        "title": "新增商户",
                        "code": "biz/merchant/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑商户",
                        "code": "biz/merchant/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取一个商户详情",
                        "code": "biz/merchant/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除商户",
                        "code": "biz/merchant/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取商户列表",
                        "code": "biz/merchant/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改商户状态",
                        "code": "biz/merchant/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "授权商户子系统",
                        "code": "biz/merchant/authorize",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "商户审核",
                        "code": "biz/merchant/toExamine",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "信息录入规范设置",
                        "code": "biz/merchant/infoType",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsSupplier",
                "title": "供应商管理",
                "code": "biz/supplier/page",
                "icon": "user-switch-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/supplier",
                "component": "biz/merchants/supplier/index",
                "subs": [
                    {
                        "title": "获取供应商详情",
                        "code": "biz/supplier/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增供应商",
                        "code": "biz/supplier/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑供应商",
                        "code": "biz/supplier/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除供应商",
                        "code": "biz/supplier/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取供应商列表",
                        "code": "biz/supplier/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改供应商状态",
                        "code": "biz/supplier/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsDistributor",
                "title": "经销商管理",
                "code": "biz/distributor/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/distributor",
                "component": "biz/merchants/distributor/index",
                "subs": [
                    {
                        "title": "获取经销商详情",
                        "code": "biz/distributor/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增经销商",
                        "code": "biz/distributor/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑经销商",
                        "code": "biz/distributor/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除经销商",
                        "code": "biz/distributor/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "批量导入经销商",
                        "code": "biz/distributor/importData",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取经销商列表",
                        "code": "biz/distributor/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改经销商状态",
                        "code": "biz/distributor/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsEmployees",
                "title": "员工管理",
                "code": "biz/employees/page",
                "icon": "usergroup-add-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/employees",
                "component": "biz/merchants/employees/index",
                "subs": [
                    {
                        "title": "获取员工列表",
                        "code": "biz/employees/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增员工",
                        "code": "biz/employees/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑员工",
                        "code": "biz/employees/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除员工",
                        "code": "biz/employees/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取员工详情",
                        "code": "biz/employees/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改员工关态",
                        "code": "biz/employees/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "其他",
        "name": "7112368231016304640",
        "title": "上链日志",
        "code": "",
        "icon": "api-outlined",
        "category": "MENU",
        "module": 10002,
        "menu_type": "CATALOG",
        "path": "/7112368231016304640",
        "component": "",
        "subs": [
            {
                "name": "bizUniversalBlockChain",
                "title": "上链纪录",
                "code": "biz/BlockChain/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10002,
                "menu_type": "MENU",
                "path": "/biz/universal/BlockChain",
                "component": "biz/blockChain/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066706762136555520",
        "title": "商品管理",
        "code": "",
        "icon": "picture-outlined",
        "category": "",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7066706762136555520",
        "component": "",
        "subs": [
            {
                "name": "goodsCateImports",
                "title": "商品分类",
                "code": "biz/cate/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/cate",
                "component": "biz/goods/cate/index",
                "subs": [
                    {
                        "title": "获取菜单列表",
                        "code": "biz/cate/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取菜单树",
                        "code": "biz/cate/tree",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增菜单",
                        "code": "biz/cate/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑菜单",
                        "code": "biz/cate/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取菜单详情",
                        "code": "biz/cate/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除菜单",
                        "code": "biz/cate/delete",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsGoods",
                "title": "商品管理",
                "code": "biz/goods/page",
                "icon": "gift-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/goods",
                "component": "biz/goods/commodity/index",
                "subs": [
                    {
                        "title": "获取商品详情",
                        "code": "biz/goods/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增商品",
                        "code": "biz/goods/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑商品",
                        "code": "biz/goods/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除商品",
                        "code": "biz/goods/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取商品列表",
                        "code": "biz/goods/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改商品状态",
                        "code": "biz/goods/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizGoodsInfo",
                "title": "商品信息配置",
                "code": "biz/batch.goodsInfo/page",
                "icon": "diff-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/batch/goodsInfoConfig",
                "component": "sub/public/trace/goodsInfoConfig"
            },
            {
                "name": "bizImportsGoodsBrand",
                "title": "品牌管理",
                "code": "biz/brand/page",
                "icon": "trademark-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/brand",
                "component": "biz/goods/brand/index",
                "subs": [
                    {
                        "title": "获取商品品牌详情",
                        "code": "biz/brand/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "商品品牌新增",
                        "code": "biz/brand/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "商品品牌编辑",
                        "code": "biz/brand/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "商品品牌删除",
                        "code": "biz/brand/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改品牌状态",
                        "code": "biz/brand/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取品牌列表",
                        "code": "biz/brand/list",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsRawMaterial",
                "title": "原料管理",
                "code": "biz/RawMaterial/page",
                "icon": "block-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/material",
                "component": "biz/goods/material/index",
                "subs": [
                    {
                        "title": "获取原料列表",
                        "code": "biz/RawMaterial/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取原料详情",
                        "code": "biz/RawMaterial/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增原料",
                        "code": "biz/RawMaterial/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑原料",
                        "code": "biz/RawMaterial/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除原料",
                        "code": "biz/RawMaterial/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/RawMaterial/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsGoodsCerification",
                "title": "商品认证",
                "code": "biz/cerification/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/cerification",
                "component": "biz/goods/cerification/index",
                "subs": [
                    {
                        "title": "商品认证列表",
                        "code": "biz/cerification/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取商品认证详情",
                        "code": "biz/cerification/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增商品认证",
                        "code": "biz/cerification/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑商品认证",
                        "code": "biz/cerification/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除商品认证",
                        "code": "biz/cerification/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/cerification/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsBom",
                "title": "物料清单",
                "code": "biz/bom/page",
                "icon": "ordered-list-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/bom",
                "component": "biz/goods/bom/index",
                "subs": [
                    {
                        "title": "获取物料清单列表",
                        "code": "biz/bom/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取物料清单详情",
                        "code": "biz/bom/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增物料清单",
                        "code": "biz/bom/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑物料清单",
                        "code": "biz/bom/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除物料清单",
                        "code": "biz/bom/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/bom/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsShop",
                "title": "店铺管理",
                "code": "biz/shop/page",
                "icon": "shop-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/shop",
                "component": "biz/goods/shop/index",
                "subs": [
                    {
                        "title": "获取店铺列表",
                        "code": "biz/shop/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增店铺",
                        "code": "biz/shop/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑店铺",
                        "code": "biz/shop/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除店铺",
                        "code": "biz/shop/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取店铺详情",
                        "code": "biz/shop/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/shop/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizHyperInfo",
                "title": "环节信息配置",
                "code": "biz/batch.hyper/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/batch/hyperInfo",
                "component": "sub/public/trace/hyperInfo",
                "subs": [
                    {
                        "title": "新增环节",
                        "code": "/biz/hyper/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑环节",
                        "code": "/biz/hyper/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除环节",
                        "code": "/biz/hyper/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改环节状态",
                        "code": "/biz/hyper/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "环节列表",
                        "code": "/biz/hyper/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "环节详情",
                        "code": "/biz/hyper/detail",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "706883140772173772040",
        "title": "原料采购",
        "code": "",
        "icon": "login-outlined",
        "category": "",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7068831409917399040",
        "component": "",
        "subs": [
            {
                "name": "bizImportsProductMateral",
                "title": "原料采购",
                "code": "biz/product.material/page",
                "icon": "plus-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/product/material",
                "component": "sub/imports/product/material/index",
                "subs": [
                    {
                        "title": "获取原料采购列表",
                        "code": "biz/product.material/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取原料采购详情",
                        "code": "biz/product.material/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增原料采购",
                        "code": "biz/product.material/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑原料采购",
                        "code": "biz/product.material/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除原料采购",
                        "code": "biz/product.material/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "上链",
                        "code": "biz/product.material/upChain",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改原料采购状态",
                        "code": "biz/product.material/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/product.material/freeze",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsProductMaterialBatch",
                "title": "原料批次",
                "code": "biz/product.MaterialItem/page",
                "icon": "pic-right-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/product/material/batch",
                "component": "sub/imports/product/material/batch",
                "subs": [
                    {
                        "title": "获取原料批次列表",
                        "code": "biz/product.MaterialItem/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取原料批次详情（预览）",
                        "code": "biz/product.MaterialItem/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改原料批次状态（冻结）",
                        "code": "biz/product.MaterialItem/freeze",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7083426365579464704",
        "title": "跨境采购",
        "code": "",
        "icon": "audit-outlined",
        "category": "MENU",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7083426365579464704",
        "component": "",
        "subs": [
            {
                "name": "bizImportsSupervise",
                "title": "监管信息",
                "code": "biz/imports.supervise/page",
                "icon": "audit-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/imports/supervise",
                "component": "sub/imports/crossBorder/supervise/index",
                "subs": [
                    {
                        "title": "新增监管信息",
                        "code": "biz/imports.supervise/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑监管信息",
                        "code": "biz/imports.supervise/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "监管信息详情",
                        "code": "biz/imports.supervise/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "改变状态",
                        "code": "biz/imports.supervise/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除监管信息",
                        "code": "biz/imports.supervise/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "监管信息列表",
                        "code": "biz/imports.supervise/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "上链",
                        "code": "biz/imports.supervise/upChain",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "信息转换格式",
                        "code": "biz/imports.supervise/change",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/imports.supervise/freeze",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsInfo",
                "title": "跨境信息配置",
                "code": "biz/batch.importsInfo/page",
                "icon": "diff-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/batch/importsInfoConfig",
                "component": "sub/public/trace/importsInfoConfig"
            },
            {
                "name": "bizImportSuperviseItem",
                "title": "采购批次",
                "code": "biz/imports.SuperviseItem/page",
                "icon": "global-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/imports/superviseItem",
                "component": "sub/imports/crossBorder/superviseItem/index",
                "subs": [
                    {
                        "title": "跨境采购列表",
                        "code": "biz/imports.SuperviseItem/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "跨境采购详情",
                        "code": "biz/imports.SuperviseItem/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "跨境采购状态",
                        "code": "biz/imports.SuperviseItem/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7070765572912320512",
        "title": "生产管理",
        "code": "",
        "icon": "scissor-outlined",
        "category": "",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7070765572912320512",
        "component": "",
        "subs": [
            {
                "name": "bizImportsProductProduceIn",
                "title": "生产入库",
                "code": "biz/trace.ProduceIn/page",
                "icon": "build-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/product/produce/in",
                "component": "sub/imports/product/produceIn/index",
                "subs": [
                    {
                        "title": "获取生产入库信息列表",
                        "code": "biz/trace.ProduceIn/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取生产入库详情",
                        "code": "biz/trace.ProduceIn/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增生产入库",
                        "code": "biz/trace.ProduceIn/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑生产入库",
                        "code": "biz/trace.ProduceIn/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除生产入库",
                        "code": "biz/trace.ProduceIn/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改生产入库状态（冻结，解冻）",
                        "code": "biz/trace.ProduceIn/freeze",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.ProduceIn/upChain",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsProductProduceInBatch",
                "title": "生产批次",
                "code": "biz/trace.ProduceInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/product/produce/in/batch",
                "component": "sub/imports/product/produceIn/batch",
                "subs": [
                    {
                        "title": "获取生产批次列表",
                        "code": "biz/trace.ProduceInItem/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改生产入库批次状态（冻结，解冻）",
                        "code": "biz/trace.ProduceInItem/freeze",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取生产入库批次详情",
                        "code": "biz/trace.ProduceInItem/detail",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7070752739495514112",
        "title": "入库管理",
        "code": "",
        "icon": "plus-square-outlined",
        "category": "",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/biz/trace/stock/in",
        "component": "",
        "subs": [
            {
                "name": "bizImportsProductStockIn",
                "title": "入库管理",
                "code": "biz/trace.StockIn/page",
                "icon": "arrow-right-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/product/stock/in",
                "component": "sub/imports/product/stockIn/index",
                "subs": [
                    {
                        "title": "获取商品入库信息列表",
                        "code": "biz/trace.StockIn/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取商品入库详情",
                        "code": "biz/trace.StockIn/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "添加商品入库",
                        "code": "biz/trace.StockIn/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑商品入库",
                        "code": "biz/trace.StockIn/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除入库信息",
                        "code": "biz/trace.StockIn/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockIn/upChain",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改商品入库状态（冻结，解冻）",
                        "code": "biz/trace.StockIn/freeze",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizImportsProductStockInBatch",
                "title": "入库批次",
                "code": "biz/trace.StockInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/product/stock/in/batch",
                "component": "sub/imports/product/stockIn/batch",
                "subs": [
                    {
                        "title": "获取入库批次列表",
                        "code": "biz/trace.StockInItem/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改商品入库批次状态（冻结，解冻）",
                        "code": "biz/trace.StockInItem/freeze",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取入库批次详情",
                        "code": "biz/trace.StockInItem/detail",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7075384622263046144",
        "title": "检验检疫",
        "code": "",
        "icon": "bug-outlined",
        "category": "MENU",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7075384622263046144",
        "component": "",
        "subs": [
            {
                "name": "bizImportsQuarantineReport",
                "title": "检验检疫",
                "code": "biz/QuarantineReport/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/QuarantineReport",
                "component": "biz/quarantineReport/index",
                "subs": [
                    {
                        "title": "获取检验检疫列表",
                        "code": "biz/QuarantineReport/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增检验检疫",
                        "code": "biz/QuarantineReport/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑检验检疫",
                        "code": "biz/QuarantineReport/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除检验检疫",
                        "code": "biz/QuarantineReport/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取检验检疫详情",
                        "code": "biz/QuarantineReport/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "激活检验检疫",
                        "code": "biz/QuarantineReport/freeze",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "上链",
                        "code": "biz/QuarantineReport/upChain",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7070754898417029120",
        "title": "出库管理",
        "code": "",
        "icon": "minus-square-outlined",
        "category": "",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7070754898417029120",
        "component": "",
        "subs": [
            {
                "name": "bizImportsProductStockOut",
                "title": "出库管理",
                "code": "biz/trace.StockOut/page",
                "icon": "rollback-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/imports/product/stock/out",
                "component": "sub/imports/product/stockOut/index",
                "subs": [
                    {
                        "title": "获取商品出库信息列表",
                        "code": "biz/trace.StockOut/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取商品出库详情",
                        "code": "biz/trace.StockOut/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "添加商品出库",
                        "code": "biz/trace.StockOut/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑出库信息",
                        "code": "biz/trace.StockOut/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除出库信息",
                        "code": "biz/trace.StockOut/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改出库状态",
                        "code": "biz/trace.StockOut/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockOut/upChain",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/trace.StockOut/freeze",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082608621531435008",
        "title": "渠道管理",
        "code": "",
        "icon": "swap-outlined",
        "category": "MENU",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7082608621531435008",
        "component": "",
        "subs": [
            {
                "name": "bizSuspectedSmuggling",
                "title": "疑似窜货",
                "code": "biz/FleeingGoods/page",
                "icon": "strikethrough-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/distribution/channel/suspectedSmuggling",
                "component": "biz/distribution/channel/suspectedSmuggling/index",
                "subs": [
                    {
                        "title": "疑似窜货列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "窜货详情",
                        "code": "biz/FleeingGoods/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除窜货",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizStatistics",
                "title": "窜货统计",
                "code": "biz/FleeingGoods/page",
                "icon": "project-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/distribution/channel/statistics",
                "component": "biz/distribution/channel/statistics/index",
                "subs": [
                    {
                        "title": "窜货统计列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除窜货统计",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082609065322352640",
        "title": "客群洞察",
        "code": "",
        "icon": "branches-outlined",
        "category": "MENU",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7082609065322352640",
        "component": "",
        "subs": [
            {
                "name": "bizDataDashboard",
                "title": "客群洞察",
                "code": "biz/data.dashboard/page",
                "icon": "deployment-unit-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/distribution/data/dashboard",
                "component": "biz/distribution/data/dashboard/index"
            },
            {
                "name": "bizDataScanQr",
                "title": "扫码统计",
                "code": "biz/ScanLogs/page",
                "icon": "scan-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/distribution/data/scanQr",
                "component": "biz/distribution/data/scanQr/index",
                "subs": [
                    {
                        "title": "扫码统计列表",
                        "code": "biz/ScanLogs/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/ScanLogs/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除扫码统计",
                        "code": "biz/ScanLogs/delete",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizDataSellTrend",
                "title": "销售趋势",
                "code": "biz/data.sellTrend/page",
                "icon": "monitor-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/distribution/data/sellTrend",
                "component": "biz/distribution/data/sellTrend/index"
            }
        ]
    },
    {
        "group_name": "码",
        "name": "7082606129921921024",
        "title": "标识管理",
        "code": "",
        "icon": "qrcode-outlined",
        "category": "MENU",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7082606129921921024",
        "component": "",
        "subs": [
            {
                "name": "bizCodeRule",
                "title": "生码管理",
                "code": "biz/codeRule/page",
                "icon": "holder-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/code/mark/rule",
                "component": "biz/code/mark/rule/index",
                "subs": [
                    {
                        "title": "码规则列表",
                        "code": "biz/CodeRule/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "新增码规则",
                        "code": "biz/CodeRule/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑码规则",
                        "code": "biz/CodeRule/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取详情",
                        "code": "biz/CodeRule/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除码规则",
                        "code": "biz/CodeRule/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/CodeRule/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "码规则审核",
                        "code": "biz/CodeRule/toExamine",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizCreateCode",
                "title": "生码纪录",
                "code": "biz/issue/page",
                "icon": "clock-circle-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/createCode",
                "component": "biz/code/mark/create/index",
                "subs": [
                    {
                        "title": "获取生码列表",
                        "code": "biz/issue/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "导入码包",
                        "code": "biz/issue/importCodeRelation",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "扫码关联批次",
                        "code": "biz/issue/scanCodeRelation",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/issue/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "生码详情",
                        "code": "biz/issue/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "生码",
                        "code": "biz/issue/crtQr",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "码下载",
                        "code": "biz/issue/down",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizOverdue",
                "title": "报损纪录",
                "code": "biz/overdue/page",
                "icon": "menu-fold-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/code/mark/overdue",
                "component": "biz/code/mark/overdue/index",
                "subs": [
                    {
                        "title": "导入报损",
                        "code": "biz/overdue/importBadCode",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "扫码报损",
                        "code": "biz/overdue/scanCode",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取报损详情",
                        "code": "biz/overdue/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除报损",
                        "code": "biz/overdue/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "报损列表",
                        "code": "biz/overdue/list",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizCodeSearchIdentification",
                "title": "查询标识",
                "code": "biz/code.searchIdentification/page",
                "icon": "zoom-in-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/code/mark/searchIdentification",
                "component": "biz/code/mark/searchIdentification/index"
            },
            {
                "name": "bizCodePackage",
                "title": "包装层级",
                "code": "biz/package/page",
                "icon": "pic-left-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/code/mark/package",
                "component": "biz/code/mark/package/index",
                "subs": [
                    {
                        "title": "获取包装层级列表",
                        "code": "biz/package/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "获取包装层级详情",
                        "code": "biz/package/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "添加包装层级",
                        "code": "biz/package/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更新包装层级",
                        "code": "biz/package/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "修改包装层级状态",
                        "code": "biz/package/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "删除包装层级",
                        "code": "biz/package/delete",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082607091466113024",
        "title": "配置管理",
        "code": "",
        "icon": "form-outlined",
        "category": "MENU",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7082607091466113024",
        "component": "",
        "subs": [
            {
                "name": "bizCheck",
                "title": "检测报告模板",
                "code": "biz/template.check/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/checkTemplate",
                "component": "biz/configue/config/check/index",
                "subs": [
                    {
                        "title": "新增检测报告模板",
                        "code": "biz/template.check/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑检测报告模板",
                        "code": "biz/template.check/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "检测报告模板列表",
                        "code": "biz/template.check/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "检测报告模板删除",
                        "code": "biz/template.check/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改检测报告模板状态",
                        "code": "biz/template.check/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "检测报告模板详情",
                        "code": "biz/template.check/detail",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizInfoTemplate",
                "title": "溯源模板管理",
                "code": "biz/template.info/page",
                "icon": "retweet-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/infoTemplate",
                "component": "biz/configue/config/info/index",
                "subs": [
                    {
                        "title": "新增溯源模板管理",
                        "code": "biz/template.info/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑溯源模板管理",
                        "code": "biz/template.info/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "溯源模板删除",
                        "code": "biz/template.info/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "溯源模板列表",
                        "code": "biz/template.info/list",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改溯源模板状态",
                        "code": "biz/template.info/state",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizApplet",
                "title": "小程序模板管理",
                "code": "biz/appletTemplate/page",
                "icon": "fullscreen-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/config/applet",
                "component": "biz/configue/config/applet/index"
            },
            {
                "name": "bizConfigTemplateInfo",
                "title": "溯源模板配置",
                "code": "biz/infoTemplate/list",
                "icon": "border-outer-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/configue/InfoConfig",
                "component": "biz/configue/config/info/info"
            },
            {
                "name": "bizAppletForm",
                "title": "新增编辑小程序模板",
                "code": "biz/applet/save",
                "icon": "code-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/config/applet/form",
                "component": "biz/configue/config/applet/form"
            },
            {
                "name": "bizCertificate",
                "title": "溯源链证书配置",
                "code": "biz/template.blockchain/detail",
                "icon": "book-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/configue/certificate",
                "component": "biz/configue/config/certificate/index",
                "subs": [
                    {
                        "title": "添加区块链模板",
                        "code": "biz/template.blockchain/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更新区块链模板",
                        "code": "biz/template.blockchain/edit",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082608218270076928",
        "title": "账号管理",
        "code": "",
        "icon": "usergroup-add-outlined",
        "category": "MENU",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7082608218270076928",
        "component": "",
        "subs": [
            {
                "name": "bizAccountMember",
                "title": "成员管理",
                "code": "biz/account/page",
                "icon": "user-add-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/account/member",
                "component": "biz/configue/newAccount/member/index",
                "subs": [
                    {
                        "title": "新增成员",
                        "code": "biz/account/add",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "编辑成员",
                        "code": "biz/account/edit",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "成员详情",
                        "code": "biz/account/detail",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/account/state",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "成员删除",
                        "code": "biz/account/delete",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "成员列表",
                        "code": "biz/account/list",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "bizAccountAuthorization",
                "title": "权限管理",
                "code": "biz/authorization/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/account/authorization",
                "component": "biz/configue/newAccount/member/authorization",
                "subs": [
                    {
                        "title": "分配权限",
                        "code": "biz/authorization/auth",
                        "category": "BUTTON",
                        "module": 10003
                    },
                    {
                        "title": "角色列表",
                        "code": "biz/authorization/getRoleList",
                        "category": "BUTTON",
                        "module": 10003
                    }
                ]
            },
            {
                "name": "configDevelop",
                "title": "开发者管理",
                "code": "biz/develop/page",
                "icon": "console-sql-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/account/api",
                "component": "biz/configue/newAccount/develop/index"
            }
        ]
    },
    {
        "group_name": "其他",
        "name": "7112444439489941504",
        "title": "上链日志",
        "code": "",
        "icon": "api-outlined",
        "category": "MENU",
        "module": 10003,
        "menu_type": "CATALOG",
        "path": "/7112444439489941504",
        "component": "",
        "subs": [
            {
                "name": "bizImportsBlockChain",
                "title": "上链纪录",
                "code": "biz/BlockChain/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10003,
                "menu_type": "MENU",
                "path": "/biz/imports/BlockChain",
                "component": "biz/blockChain/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066387589372055552",
        "title": "商户管理",
        "code": "",
        "icon": "shop-outlined",
        "category": "",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7066387589372055552",
        "component": "",
        "subs": [
            {
                "name": "bizPlantingMerchant",
                "title": "商户管理",
                "code": "biz/merchant/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/merchant",
                "component": "biz/merchants/merchant/index",
                "subs": [
                    {
                        "title": "新增商户",
                        "code": "biz/merchant/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑商户",
                        "code": "biz/merchant/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取一个商户详情",
                        "code": "biz/merchant/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除商户",
                        "code": "biz/merchant/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取商户列表",
                        "code": "biz/merchant/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改商户状态",
                        "code": "biz/merchant/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "授权商户子系统",
                        "code": "biz/merchant/authorize",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "商户审核",
                        "code": "biz/merchant/toExamine",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "信息录入规范设置",
                        "code": "biz/merchant/infoType",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizPlantingSupplier",
                "title": "供应商管理",
                "code": "biz/supplier/page",
                "icon": "user-switch-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/supplier",
                "component": "biz/merchants/supplier/index",
                "subs": [
                    {
                        "title": "获取供应商详情",
                        "code": "biz/supplier/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增供应商",
                        "code": "biz/supplier/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑供应商",
                        "code": "biz/supplier/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除供应商",
                        "code": "biz/supplier/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取供应商列表",
                        "code": "biz/supplier/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改供应商状态",
                        "code": "biz/supplier/state",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizPlantingDistributor",
                "title": "经销商管理",
                "code": "biz/distributor/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/distributor",
                "component": "biz/merchants/distributor/index",
                "subs": [
                    {
                        "title": "获取经销商详情",
                        "code": "biz/distributor/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增经销商",
                        "code": "biz/distributor/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑经销商",
                        "code": "biz/distributor/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除经销商",
                        "code": "biz/distributor/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "批量导入经销商",
                        "code": "biz/distributor/importData",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取经销商列表",
                        "code": "biz/distributor/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改经销商状态",
                        "code": "biz/distributor/state",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizPlantingEmployees",
                "title": "员工管理",
                "code": "biz/employees/page",
                "icon": "usergroup-add-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/employees",
                "component": "biz/merchants/employees/index",
                "subs": [
                    {
                        "title": "获取员工列表",
                        "code": "biz/employees/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增员工",
                        "code": "biz/employees/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑员工",
                        "code": "biz/employees/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除员工",
                        "code": "biz/employees/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取员工详情",
                        "code": "biz/employees/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改员工关态",
                        "code": "biz/employees/state",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066706762136555520",
        "title": "商品管理",
        "code": "",
        "icon": "picture-outlined",
        "category": "",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7066706762136555520",
        "component": "",
        "subs": [
            {
                "name": "goodsCatePlanting",
                "title": "商品分类",
                "code": "biz/cate/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/cate",
                "component": "biz/goods/cate/index",
                "subs": [
                    {
                        "title": "获取菜单列表",
                        "code": "biz/cate/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取菜单树",
                        "code": "biz/cate/tree",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增菜单",
                        "code": "biz/cate/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑菜单",
                        "code": "biz/cate/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取菜单详情",
                        "code": "biz/cate/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除菜单",
                        "code": "biz/cate/delete",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizPlantingGoods",
                "title": "商品管理",
                "code": "biz/goods/page",
                "icon": "gift-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/goods",
                "component": "biz/goods/commodity/index",
                "subs": [
                    {
                        "title": "获取商品详情",
                        "code": "biz/goods/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增商品",
                        "code": "biz/goods/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑商品",
                        "code": "biz/goods/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除商品",
                        "code": "biz/goods/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取商品列表",
                        "code": "biz/goods/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改商品状态",
                        "code": "biz/goods/state",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizGoodsInfo",
                "title": "商品信息配置",
                "code": "biz/batch.goodsInfo/page",
                "icon": "diff-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/batch/goodsInfoConfig",
                "component": "sub/public/trace/goodsInfoConfig"
            },
            {
                "name": "bizPlantingGoodsBrand",
                "title": "品牌管理",
                "code": "biz/brand/page",
                "icon": "trademark-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/brand",
                "component": "biz/goods/brand/index",
                "subs": [
                    {
                        "title": "获取商品品牌详情",
                        "code": "biz/brand/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "商品品牌新增",
                        "code": "biz/brand/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "商品品牌编辑",
                        "code": "biz/brand/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "商品品牌删除",
                        "code": "biz/brand/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改品牌状态",
                        "code": "biz/brand/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取品牌列表",
                        "code": "biz/brand/list",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizPlantingRawMaterial",
                "title": "投入品管理",
                "code": "biz/RawMaterial/page",
                "icon": "block-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/inputs",
                "component": "biz/goods/inputs/index",
                "subs": [
                    {
                        "title": "获取投入品列表",
                        "code": "biz/RawMaterial/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取投入品详情",
                        "code": "biz/RawMaterial/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增投入品",
                        "code": "biz/RawMaterial/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑投入品",
                        "code": "biz/RawMaterial/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除投入品",
                        "code": "biz/RawMaterial/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/RawMaterial/state",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizPlantingGoodsCerification",
                "title": "商品认证",
                "code": "biz/cerification/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/cerification",
                "component": "biz/goods/cerification/index",
                "subs": [
                    {
                        "title": "商品认证列表",
                        "code": "biz/cerification/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取商品认证详情",
                        "code": "biz/cerification/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增商品认证",
                        "code": "biz/cerification/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑商品认证",
                        "code": "biz/cerification/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除商品认证",
                        "code": "biz/cerification/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/cerification/state",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizPlantingShop",
                "title": "店铺管理",
                "code": "biz/shop/page",
                "icon": "shop-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/shop",
                "component": "biz/goods/shop/index",
                "subs": [
                    {
                        "title": "获取店铺列表",
                        "code": "biz/shop/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增店铺",
                        "code": "biz/shop/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑店铺",
                        "code": "biz/shop/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除店铺",
                        "code": "biz/shop/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取店铺详情",
                        "code": "biz/shop/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/shop/state",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizHyperInfo",
                "title": "环节信息配置",
                "code": "biz/batch.hyper/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/batch/hyperInfo",
                "component": "sub/public/trace/hyperInfo",
                "subs": [
                    {
                        "title": "新增环节",
                        "code": "/biz/hyper/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑环节",
                        "code": "/biz/hyper/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除环节",
                        "code": "/biz/hyper/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改环节状态",
                        "code": "/biz/hyper/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "环节列表",
                        "code": "/biz/hyper/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "环节详情",
                        "code": "/biz/hyper/detail",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7068831409917399040",
        "title": "原料采购",
        "code": "",
        "icon": "login-outlined",
        "category": "",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7068831409917399040",
        "component": "",
        "subs": [
            {
                "name": "bizPlantingProductMateral",
                "title": "投入品采购",
                "code": "biz/product.material/page",
                "icon": "plus-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/product/material",
                "component": "sub/planting/product/material/index",
                "subs": [
                    {
                        "title": "获取投入品采购列表",
                        "code": "biz/product.material/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取投入品采购详情",
                        "code": "biz/product.material/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增投入品采购",
                        "code": "biz/product.material/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑投入品采购",
                        "code": "biz/product.material/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除投入品采购",
                        "code": "biz/product.material/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "上链",
                        "code": "biz/product.material/upChain",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改投入品采购状态",
                        "code": "biz/product.material/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/product.material/freeze",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizPlantingProductMaterialBatch",
                "title": "投入品批次",
                "code": "biz/product.MaterialItem/page",
                "icon": "pic-right-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/product/material/batch",
                "component": "sub/planting/product/material/batch",
                "subs": [
                    {
                        "title": "获取投入品批次列表",
                        "code": "biz/product.MaterialItem/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取投入品批次详情（预览）",
                        "code": "biz/product.MaterialItem/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改投入品批次状态（冻结）",
                        "code": "biz/product.MaterialItem/freeze",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7072936412537950208",
        "title": "农事管理",
        "code": "",
        "icon": "file-zip-outlined",
        "category": "MENU",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7072936412537950208",
        "component": "",
        "subs": [
            {
                "name": "bizParcelOfLand",
                "title": "地块管理",
                "code": "biz/planting.plot/page",
                "icon": "radius-bottomright-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/agriculturalManagement/parcelOfLand",
                "component": "sub/planting/agriculturalManagement/plot/index",
                "subs": [
                    {
                        "title": "新增地块",
                        "code": "biz/planting.plot/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑地块",
                        "code": "biz/planting.plot/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "地块详情",
                        "code": "biz/planting.plot/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/planting.plot/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除地块",
                        "code": "biz/planting.plot/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "地块列表",
                        "code": "biz/planting.plot/list",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizPlantPlan",
                "title": "种植计划",
                "code": "biz/planting.plan/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/agriculturalManagement/plantPlan",
                "component": "sub/planting/agriculturalManagement/plan/index",
                "subs": [
                    {
                        "title": "新增计划",
                        "code": "biz/planting.plan/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑计划",
                        "code": "biz/planting.plan/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "计划详情",
                        "code": "biz/planting.plan/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改计划状态",
                        "code": "biz/planting.plan/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除计划",
                        "code": "biz/planting.plan/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "计划列表",
                        "code": "biz/planting.plan/list",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizFarmWork",
                "title": "种植作业",
                "code": "biz/planting.work/page",
                "icon": "account-book-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/agriculturalManagement/farmWork",
                "component": "sub/planting/agriculturalManagement/work/index",
                "subs": [
                    {
                        "title": "新增作业",
                        "code": "biz/planting.work/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑作业",
                        "code": "biz/planting.work/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改作业",
                        "code": "biz/planting.work/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "作业详情",
                        "code": "biz/planting.work/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除作业",
                        "code": "biz/planting.work/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "作业列表",
                        "code": "biz/planting.work/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "上链",
                        "code": "biz/planting.work/upChain",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/planting.work/freeze",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizFarmDevice",
                "title": "设备管理",
                "code": "biz/device/page",
                "icon": "alert-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "biz/device",
                "component": "biz/device/index",
                "subs": [
                    {
                        "title": "设备列表",
                        "code": "biz/device/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增设备",
                        "code": "biz/device/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "设备改名",
                        "code": "biz/device/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "设备删除",
                        "code": "biz/device/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "设备播放",
                        "code": "biz/device/play",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "设备抓拍",
                        "code": "biz/device/capture",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizFarmDevicePlay",
                "title": "设备播放",
                "code": "biz/device/play",
                "icon": "play-circle-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/device/play",
                "component": "biz/device/play"
            },
            {
                "name": "bizPlotFarmingInfo",
                "title": "农事活动配置",
                "code": "biz/batch.farming/page",
                "icon": "contacts-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/batch/farmingInfo",
                "component": "sub/public/trace/farmingInfo"
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7070765572912320512",
        "title": "生产管理",
        "code": "",
        "icon": "scissor-outlined",
        "category": "",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7070765572912320512",
        "component": "",
        "subs": [
            {
                "name": "bizProductPlantingProduceIn",
                "title": "采收入库",
                "code": "biz/trace.ProduceIn/page",
                "icon": "build-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/product/produce/in",
                "component": "sub/planting/product/produceIn/index",
                "subs": [
                    {
                        "title": "获取采收入库信息列表",
                        "code": "biz/trace.ProduceIn/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取采收入库详情",
                        "code": "biz/trace.ProduceIn/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增采收入库",
                        "code": "biz/trace.ProduceIn/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑采收入库",
                        "code": "biz/trace.ProduceIn/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除采收入库",
                        "code": "biz/trace.ProduceIn/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改采收入库状态（冻结，解冻）",
                        "code": "biz/trace.ProduceIn/freeze",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.ProduceIn/upChain",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizProductPlantingProduceInBatch",
                "title": "采收批次",
                "code": "biz/trace.ProduceInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/product/produce/in/batch",
                "component": "sub/planting/product/produceIn/batch",
                "subs": [
                    {
                        "title": "获取采收批次列表",
                        "code": "biz/trace.ProduceInItem/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改采收入库批次状态（冻结，解冻）",
                        "code": "biz/trace.ProduceInItem/freeze",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取采收入库批次详情",
                        "code": "biz/trace.ProduceInItem/detail",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7075384736775933952",
        "title": "检验检疫",
        "code": "",
        "icon": "bug-outlined",
        "category": "MENU",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7075384736775933952",
        "component": "",
        "subs": [
            {
                "name": "bizPlantingQuarantineReport",
                "title": "检验检疫",
                "code": "biz/QuarantineReport/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/QuarantineReport",
                "component": "biz/quarantineReport/index",
                "subs": [
                    {
                        "title": "获取检验检疫列表",
                        "code": "biz/QuarantineReport/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增检验检疫",
                        "code": "biz/QuarantineReport/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑检验检疫",
                        "code": "biz/QuarantineReport/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除检验检疫",
                        "code": "biz/QuarantineReport/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取检验检疫详情",
                        "code": "biz/QuarantineReport/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "激活检验检疫",
                        "code": "biz/QuarantineReport/freeze",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "上链",
                        "code": "biz/QuarantineReport/upChain",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7070754898417029120",
        "title": "出库管理",
        "code": "",
        "icon": "minus-square-outlined",
        "category": "",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7070754898417029120",
        "component": "",
        "subs": [
            {
                "name": "bizPlantingProductStockOut",
                "title": "出库管理",
                "code": "biz/trace.StockOut/page",
                "icon": "rollback-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/planting/product/stock/out",
                "component": "sub/planting/product/stockOut/index",
                "subs": [
                    {
                        "title": "获取商品出库信息列表",
                        "code": "biz/trace.StockOut/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取商品出库详情",
                        "code": "biz/trace.StockOut/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "添加商品出库",
                        "code": "biz/trace.StockOut/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑出库信息",
                        "code": "biz/trace.StockOut/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除出库信息",
                        "code": "biz/trace.StockOut/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改出库状态",
                        "code": "biz/trace.StockOut/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockOut/upChain",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/trace.StockOut/freeze",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082612283444039680",
        "title": "渠道管理",
        "code": "",
        "icon": "swap-outlined",
        "category": "MENU",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7082612283444039680",
        "component": "",
        "subs": [
            {
                "name": "bizSuspectedSmuggling",
                "title": "疑似窜货",
                "code": "biz/FleeingGoods/page",
                "icon": "strikethrough-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/distribution/channel/suspectedSmuggling",
                "component": "biz/distribution/channel/suspectedSmuggling/index",
                "subs": [
                    {
                        "title": "疑似窜货列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "窜货详情",
                        "code": "biz/FleeingGoods/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除窜货",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizStatistics",
                "title": "窜货统计",
                "code": "biz/FleeingGoods/page",
                "icon": "project-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/distribution/channel/statistics",
                "component": "biz/distribution/channel/statistics/index",
                "subs": [
                    {
                        "title": "窜货统计列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除窜货统计",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082612686176915456",
        "title": "客群洞察",
        "code": "",
        "icon": "branches-outlined",
        "category": "MENU",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7082612686176915456",
        "component": "",
        "subs": [
            {
                "name": "bizDataDashboard",
                "title": "客群洞察",
                "code": "biz/data.dashboard/page",
                "icon": "deployment-unit-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/distribution/data/dashboard",
                "component": "biz/distribution/data/dashboard/index"
            },
            {
                "name": "bizDataScanQr",
                "title": "扫码统计",
                "code": "biz/ScanLogs/page",
                "icon": "scan-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/distribution/data/scanQr",
                "component": "biz/distribution/data/scanQr/index",
                "subs": [
                    {
                        "title": "扫码统计列表",
                        "code": "biz/ScanLogs/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/ScanLogs/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除扫码统计",
                        "code": "biz/ScanLogs/delete",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizDataSellTrend",
                "title": "销售趋势",
                "code": "biz/data.sellTrend/page",
                "icon": "monitor-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/distribution/data/sellTrend",
                "component": "biz/distribution/data/sellTrend/index"
            }
        ]
    },
    {
        "group_name": "码",
        "name": "7082610088808026112",
        "title": "标识管理",
        "code": "",
        "icon": "qrcode-outlined",
        "category": "MENU",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7082610088808026112",
        "component": "",
        "subs": [
            {
                "name": "bizCodeRule",
                "title": "生码管理",
                "code": "biz/codeRule/page",
                "icon": "holder-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/code/mark/rule",
                "component": "biz/code/mark/rule/index",
                "subs": [
                    {
                        "title": "码规则列表",
                        "code": "biz/CodeRule/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "新增码规则",
                        "code": "biz/CodeRule/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑码规则",
                        "code": "biz/CodeRule/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取详情",
                        "code": "biz/CodeRule/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除码规则",
                        "code": "biz/CodeRule/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/CodeRule/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "码规则审核",
                        "code": "biz/CodeRule/toExamine",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizCreateCode",
                "title": "生码纪录",
                "code": "biz/issue/page",
                "icon": "clock-circle-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/createCode",
                "component": "biz/code/mark/create/index",
                "subs": [
                    {
                        "title": "获取生码列表",
                        "code": "biz/issue/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "导入码包",
                        "code": "biz/issue/importCodeRelation",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "扫码关联批次",
                        "code": "biz/issue/scanCodeRelation",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/issue/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "生码详情",
                        "code": "biz/issue/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "生码",
                        "code": "biz/issue/crtQr",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "码下载",
                        "code": "biz/issue/down",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizOverdue",
                "title": "报损纪录",
                "code": "biz/overdue/page",
                "icon": "menu-fold-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/code/mark/overdue",
                "component": "biz/code/mark/overdue/index",
                "subs": [
                    {
                        "title": "导入报损",
                        "code": "biz/overdue/importBadCode",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "扫码报损",
                        "code": "biz/overdue/scanCode",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取报损详情",
                        "code": "biz/overdue/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除报损",
                        "code": "biz/overdue/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "报损列表",
                        "code": "biz/overdue/list",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizCodeSearchIdentification",
                "title": "查询标识",
                "code": "biz/code.searchIdentification/page",
                "icon": "zoom-in-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/code/mark/searchIdentification",
                "component": "biz/code/mark/searchIdentification/index"
            },
            {
                "name": "bizCodePackage",
                "title": "包装层级",
                "code": "biz/package/page",
                "icon": "pic-left-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/code/mark/package",
                "component": "biz/code/mark/package/index",
                "subs": [
                    {
                        "title": "获取包装层级列表",
                        "code": "biz/package/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "获取包装层级详情",
                        "code": "biz/package/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "添加包装层级",
                        "code": "biz/package/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更新包装层级",
                        "code": "biz/package/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "修改包装层级状态",
                        "code": "biz/package/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "删除包装层级",
                        "code": "biz/package/delete",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082610996954533888",
        "title": "配置管理",
        "code": "",
        "icon": "form-outlined",
        "category": "MENU",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7082610996954533888",
        "component": "",
        "subs": [
            {
                "name": "bizCheck",
                "title": "检测报告模板",
                "code": "biz/template.check/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/checkTemplate",
                "component": "biz/configue/config/check/index",
                "subs": [
                    {
                        "title": "新增检测报告模板",
                        "code": "biz/template.check/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑检测报告模板",
                        "code": "biz/template.check/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "检测报告模板列表",
                        "code": "biz/template.check/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "检测报告模板删除",
                        "code": "biz/template.check/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改检测报告模板状态",
                        "code": "biz/template.check/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "检测报告模板详情",
                        "code": "biz/template.check/detail",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizInfoTemplate",
                "title": "溯源模板管理",
                "code": "biz/template.info/page",
                "icon": "retweet-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/infoTemplate",
                "component": "biz/configue/config/info/index",
                "subs": [
                    {
                        "title": "新增溯源模板管理",
                        "code": "biz/template.info/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑溯源模板管理",
                        "code": "biz/template.info/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "溯源模板删除",
                        "code": "biz/template.info/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "溯源模板列表",
                        "code": "biz/template.info/list",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改溯源模板状态",
                        "code": "biz/template.info/state",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizApplet",
                "title": "小程序模板管理",
                "code": "biz/appletTemplate/page",
                "icon": "fullscreen-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/config/applet",
                "component": "biz/configue/config/applet/index"
            },
            {
                "name": "bizConfigTemplateInfo",
                "title": "溯源模板配置",
                "code": "biz/infoTemplate/list",
                "icon": "border-outer-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/configue/InfoConfig",
                "component": "biz/configue/config/info/info"
            },
            {
                "name": "bizAppletForm",
                "title": "新增编辑小程序模板",
                "code": "biz/applet/save",
                "icon": "code-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/config/applet/form",
                "component": "biz/configue/config/applet/form"
            },
            {
                "name": "bizCertificate",
                "title": "溯源链证书配置",
                "code": "biz/template.blockchain/detail",
                "icon": "book-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/configue/certificate",
                "component": "biz/configue/config/certificate/index",
                "subs": [
                    {
                        "title": "添加区块链模板",
                        "code": "biz/template.blockchain/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更新区块链模板",
                        "code": "biz/template.blockchain/edit",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082611844178776064",
        "title": "账号管理",
        "code": "",
        "icon": "usergroup-add-outlined",
        "category": "MENU",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7082611844178776064",
        "component": "",
        "subs": [
            {
                "name": "bizAccountMember",
                "title": "成员管理",
                "code": "biz/account/page",
                "icon": "user-add-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/account/member",
                "component": "biz/configue/newAccount/member/index",
                "subs": [
                    {
                        "title": "新增成员",
                        "code": "biz/account/add",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "编辑成员",
                        "code": "biz/account/edit",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "成员详情",
                        "code": "biz/account/detail",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/account/state",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "成员删除",
                        "code": "biz/account/delete",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "成员列表",
                        "code": "biz/account/list",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "bizAccountAuthorization",
                "title": "权限管理",
                "code": "biz/authorization/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/account/authorization",
                "component": "biz/configue/newAccount/member/authorization",
                "subs": [
                    {
                        "title": "分配权限",
                        "code": "biz/authorization/auth",
                        "category": "BUTTON",
                        "module": 10004
                    },
                    {
                        "title": "角色列表",
                        "code": "biz/authorization/getRoleList",
                        "category": "BUTTON",
                        "module": 10004
                    }
                ]
            },
            {
                "name": "configDevelop",
                "title": "开发者管理",
                "code": "biz/develop/page",
                "icon": "console-sql-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/account/api",
                "component": "biz/configue/newAccount/develop/index"
            }
        ]
    },
    {
        "group_name": "其他",
        "name": "7112445175716122624",
        "title": "上链日志",
        "code": "",
        "icon": "api-outlined",
        "category": "MENU",
        "module": 10004,
        "menu_type": "CATALOG",
        "path": "/7112445175716122624",
        "component": "",
        "subs": [
            {
                "name": "bizPlantingBlockChain",
                "title": "上链纪录",
                "code": "biz/BlockChain/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10004,
                "menu_type": "MENU",
                "path": "/biz/planting/BlockChain",
                "component": "biz/blockChain/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066387589372055552",
        "title": "商户管理",
        "code": "",
        "icon": "shop-outlined",
        "category": "",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7066387589372055552",
        "component": "",
        "subs": [
            {
                "name": "bizFisheryMerchant",
                "title": "商户管理",
                "code": "biz/merchant/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/merchant",
                "component": "biz/merchants/merchant/index",
                "subs": [
                    {
                        "title": "新增商户",
                        "code": "biz/merchant/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑商户",
                        "code": "biz/merchant/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取一个商户详情",
                        "code": "biz/merchant/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除商户",
                        "code": "biz/merchant/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取商户列表",
                        "code": "biz/merchant/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改商户状态",
                        "code": "biz/merchant/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "授权商户子系统",
                        "code": "biz/merchant/authorize",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "商户审核",
                        "code": "biz/merchant/toExamine",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "信息录入规范设置",
                        "code": "biz/merchant/infoType",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisherySupplier",
                "title": "供应商管理",
                "code": "biz/supplier/page",
                "icon": "user-switch-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/supplier",
                "component": "biz/merchants/supplier/index",
                "subs": [
                    {
                        "title": "获取供应商详情",
                        "code": "biz/supplier/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增供应商",
                        "code": "biz/supplier/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑供应商",
                        "code": "biz/supplier/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除供应商",
                        "code": "biz/supplier/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取供应商列表",
                        "code": "biz/supplier/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改供应商状态",
                        "code": "biz/supplier/state",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryDistributor",
                "title": "经销商管理",
                "code": "biz/distributor/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/distributor",
                "component": "biz/merchants/distributor/index",
                "subs": [
                    {
                        "title": "获取经销商详情",
                        "code": "biz/distributor/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增经销商",
                        "code": "biz/distributor/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑经销商",
                        "code": "biz/distributor/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除经销商",
                        "code": "biz/distributor/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "批量导入经销商",
                        "code": "biz/distributor/importData",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取经销商列表",
                        "code": "biz/distributor/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改经销商状态",
                        "code": "biz/distributor/state",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryEmployees",
                "title": "员工管理",
                "code": "biz/employees/page",
                "icon": "usergroup-add-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/employees",
                "component": "biz/merchants/employees/index",
                "subs": [
                    {
                        "title": "获取员工列表",
                        "code": "biz/employees/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增员工",
                        "code": "biz/employees/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑员工",
                        "code": "biz/employees/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除员工",
                        "code": "biz/employees/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取员工详情",
                        "code": "biz/employees/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改员工关态",
                        "code": "biz/employees/state",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066706762136555520",
        "title": "商品管理",
        "code": "",
        "icon": "picture-outlined",
        "category": "",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7066706762136555520",
        "component": "",
        "subs": [
            {
                "name": "goodsCateFishery",
                "title": "商品分类",
                "code": "biz/cate/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/cate",
                "component": "biz/goods/cate/index",
                "subs": [
                    {
                        "title": "获取菜单列表",
                        "code": "biz/cate/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取菜单树",
                        "code": "biz/cate/tree",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增菜单",
                        "code": "biz/cate/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑菜单",
                        "code": "biz/cate/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取菜单详情",
                        "code": "biz/cate/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除菜单",
                        "code": "biz/cate/delete",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryGoods",
                "title": "商品管理",
                "code": "biz/goods/page",
                "icon": "gift-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/goods",
                "component": "biz/goods/commodity/index",
                "subs": [
                    {
                        "title": "获取商品详情",
                        "code": "biz/goods/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增商品",
                        "code": "biz/goods/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑商品",
                        "code": "biz/goods/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除商品",
                        "code": "biz/goods/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取商品列表",
                        "code": "biz/goods/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改商品状态",
                        "code": "biz/goods/state",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizGoodsInfo",
                "title": "商品信息配置",
                "code": "biz/batch.goodsInfo/page",
                "icon": "diff-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/batch/goodsInfoConfig",
                "component": "sub/public/trace/goodsInfoConfig"
            },
            {
                "name": "bizFisheryGoodsBrand",
                "title": "品牌管理",
                "code": "biz/brand/page",
                "icon": "trademark-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/brand",
                "component": "biz/goods/brand/index",
                "subs": [
                    {
                        "title": "获取商品品牌详情",
                        "code": "biz/brand/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "商品品牌新增",
                        "code": "biz/brand/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "商品品牌编辑",
                        "code": "biz/brand/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "商品品牌删除",
                        "code": "biz/brand/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改品牌状态",
                        "code": "biz/brand/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取品牌列表",
                        "code": "biz/brand/list",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryRawMaterial",
                "title": "投入品管理",
                "code": "biz/RawMaterial/page",
                "icon": "block-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/inputs",
                "component": "biz/goods/inputs/index",
                "subs": [
                    {
                        "title": "获取投入品列表",
                        "code": "biz/RawMaterial/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取投入品详情",
                        "code": "biz/RawMaterial/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增投入品",
                        "code": "biz/RawMaterial/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑投入品",
                        "code": "biz/RawMaterial/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除投入品",
                        "code": "biz/RawMaterial/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/RawMaterial/state",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryGoodsCerification",
                "title": "商品认证",
                "code": "biz/cerification/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/cerification",
                "component": "biz/goods/cerification/index",
                "subs": [
                    {
                        "title": "商品认证列表",
                        "code": "biz/cerification/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取商品认证详情",
                        "code": "biz/cerification/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增商品认证",
                        "code": "biz/cerification/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑商品认证",
                        "code": "biz/cerification/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除商品认证",
                        "code": "biz/cerification/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/cerification/state",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryShop",
                "title": "店铺管理",
                "code": "biz/shop/page",
                "icon": "shop-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/shop",
                "component": "biz/goods/shop/index",
                "subs": [
                    {
                        "title": "获取店铺列表",
                        "code": "biz/shop/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增店铺",
                        "code": "biz/shop/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑店铺",
                        "code": "biz/shop/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除店铺",
                        "code": "biz/shop/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取店铺详情",
                        "code": "biz/shop/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/shop/state",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizHyperInfo",
                "title": "环节信息配置",
                "code": "biz/batch.hyper/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/batch/hyperInfo",
                "component": "sub/public/trace/hyperInfo",
                "subs": [
                    {
                        "title": "新增环节",
                        "code": "/biz/hyper/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑环节",
                        "code": "/biz/hyper/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除环节",
                        "code": "/biz/hyper/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改状态",
                        "code": "/biz/hyper/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "环节列表",
                        "code": "/biz/hyper/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "环节详情",
                        "code": "/biz/hyper/detail",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7068831409917399040",
        "title": "原料采购",
        "code": "",
        "icon": "login-outlined",
        "category": "",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7068831409917399040",
        "component": "",
        "subs": [
            {
                "name": "bizFisheryProductMateral",
                "title": "投入品采购",
                "code": "biz/product.material/page",
                "icon": "plus-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/product/material",
                "component": "sub/fishery/product/material/index",
                "subs": [
                    {
                        "title": "获取投入品采购列表",
                        "code": "biz/product.material/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取投入品采购详情",
                        "code": "biz/product.material/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增投入品采购",
                        "code": "biz/product.material/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑投入品采购",
                        "code": "biz/product.material/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除投入品采购",
                        "code": "biz/product.material/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "上链",
                        "code": "biz/product.material/upChain",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改投入品采购状态",
                        "code": "biz/product.material/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/product.material/freeze",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryProductMaterialBatch",
                "title": "投入品批次",
                "code": "biz/product.MaterialItem/page",
                "icon": "pic-right-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/product/material/batch",
                "component": "sub/fishery/product/material/batch",
                "subs": [
                    {
                        "title": "获取投入品批次列表",
                        "code": "biz/product.MaterialItem/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取投入品批次详情（预览）",
                        "code": "biz/product.MaterialItem/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改投入品批次状态（冻结）",
                        "code": "biz/product.MaterialItem/freeze",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7072936412537950208",
        "title": "农事管理",
        "code": "",
        "icon": "file-zip-outlined",
        "category": "MENU",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7072936412537950208",
        "component": "",
        "subs": [
            {
                "name": "bizFisheryPlot",
                "title": "鱼塘管理",
                "code": "biz/planting.plot/page",
                "icon": "radius-bottomright-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/agriculturalManagement/fishPond",
                "component": "sub/fishery/agriculturalManagement/plot/index",
                "subs": [
                    {
                        "title": "新增鱼塘",
                        "code": "biz/planting.plot/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑鱼塘",
                        "code": "biz/planting.plot/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "鱼塘详情",
                        "code": "biz/planting.plot/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改鱼塘状态",
                        "code": "biz/planting.plot/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除鱼塘",
                        "code": "biz/planting.plot/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "鱼塘列表",
                        "code": "biz/planting.plot/list",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryPlan",
                "title": "养殖计划",
                "code": "biz/planting.plan/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/agriculturalManagement/breedingPlan",
                "component": "sub/fishery/agriculturalManagement/plan/index",
                "subs": [
                    {
                        "title": "新增计划",
                        "code": "biz/planting.plan/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑计划",
                        "code": "biz/planting.plan/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改计划状态",
                        "code": "biz/planting.plan/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除计划",
                        "code": "biz/planting.plan/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "计划列表",
                        "code": "biz/planting.plan/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "计划详情",
                        "code": "biz/planting.plan/detail",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryWork",
                "title": "养殖作业",
                "code": "biz/planting.work/page",
                "icon": "account-book-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/agriculturalManagement/breedingWork",
                "component": "sub/fishery/agriculturalManagement/work/index",
                "subs": [
                    {
                        "title": "新增作业",
                        "code": "biz/planting.work/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑作业",
                        "code": "biz/planting.work/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改作业状态",
                        "code": "biz/planting.work/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "作业详情",
                        "code": "biz/planting.work/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除作业",
                        "code": "biz/planting.work/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "作业列表",
                        "code": "biz/planting.work/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "上链",
                        "code": "biz/planting.work/upChain",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/planting.work/freeze",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizFisheryDevice",
                "title": "设备管理",
                "code": "biz/device/page",
                "icon": "alert-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "biz/device",
                "component": "biz/device/index",
                "subs": [
                    {
                        "title": "设备列表",
                        "code": "biz/device/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增设备",
                        "code": "biz/device/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "设备更名",
                        "code": "biz/device/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "设备删除",
                        "code": "biz/device/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "设备播放",
                        "code": "biz/device/play",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "设备抓拍",
                        "code": "biz/device/capture",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizDevicePlay",
                "title": "设备播放",
                "code": "biz/device/play",
                "icon": "play-circle-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/device/play",
                "component": "biz/device/play"
            },
            {
                "name": "bizFisheryFarmingInfo",
                "title": "农事活动配置",
                "code": "biz/batch.farming/page",
                "icon": "contacts-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/batch/farmingInfo",
                "component": "sub/public/trace/farmingInfo"
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7070765572912320512",
        "title": "生产管理",
        "code": "",
        "icon": "scissor-outlined",
        "category": "",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7070765572912320512",
        "component": "",
        "subs": [
            {
                "name": "bizProductFisheryProduceIn",
                "title": "捕捞入库",
                "code": "biz/trace.ProduceIn/page",
                "icon": "build-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/product/produce/in",
                "component": "sub/fishery/product/produceIn/index",
                "subs": [
                    {
                        "title": "获取捕捞入库信息列表",
                        "code": "biz/trace.ProduceIn/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取捕捞入库详情",
                        "code": "biz/trace.ProduceIn/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增捕捞入库",
                        "code": "biz/trace.ProduceIn/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑捕捞入库",
                        "code": "biz/trace.ProduceIn/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除捕捞入库",
                        "code": "biz/trace.ProduceIn/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改捕捞入库状态（冻结，解冻）",
                        "code": "biz/trace.ProduceIn/freeze",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.ProduceIn/upChain",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizProductFisheryProduceInBatch",
                "title": "捕捞批次",
                "code": "biz/trace.ProduceInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/product/produce/in/batch",
                "component": "sub/fishery/product/produceIn/batch",
                "subs": [
                    {
                        "title": "获取捕捞批次列表",
                        "code": "biz/trace.ProduceInItem/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改捕捞入库批次状态（冻结，解冻）",
                        "code": "biz/trace.ProduceInItem/freeze",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取捕捞入库批次详情",
                        "code": "biz/trace.ProduceInItem/detail",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7075384839754485760",
        "title": "检验检疫",
        "code": "",
        "icon": "bug-outlined",
        "category": "MENU",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7075384839754485760",
        "component": "",
        "subs": [
            {
                "name": "bizFisheryQuarantineReport",
                "title": "检验检疫",
                "code": "biz/QuarantineReport/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/QuarantineReport",
                "component": "biz/quarantineReport/index",
                "subs": [
                    {
                        "title": "获取检验检疫列表",
                        "code": "biz/QuarantineReport/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增检验检疫",
                        "code": "biz/QuarantineReport/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑检验检疫",
                        "code": "biz/QuarantineReport/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除检验检疫",
                        "code": "biz/QuarantineReport/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取检验检疫详情",
                        "code": "biz/QuarantineReport/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "激活检验检疫",
                        "code": "biz/QuarantineReport/freeze",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "上链",
                        "code": "biz/QuarantineReport/upChain",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7070754898417029120",
        "title": "出库管理",
        "code": "",
        "icon": "minus-square-outlined",
        "category": "",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7070754898417029120",
        "component": "",
        "subs": [
            {
                "name": "bizFisheryProductStockOut",
                "title": "出库管理",
                "code": "biz/trace.StockOut/page",
                "icon": "rollback-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/fishery/product/stock/out",
                "component": "sub/fishery/product/stockOut/index",
                "subs": [
                    {
                        "title": "获取商品出库信息列表",
                        "code": "biz/trace.StockOut/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取商品出库详情",
                        "code": "biz/trace.StockOut/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "添加商品出库",
                        "code": "biz/trace.StockOut/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑出库信息",
                        "code": "biz/trace.StockOut/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除出库信息",
                        "code": "biz/trace.StockOut/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改出库状态",
                        "code": "biz/trace.StockOut/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockOut/upChain",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/trace.StockOut/freeze",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082616300035379200",
        "title": "渠道管理",
        "code": "",
        "icon": "swap-outlined",
        "category": "MENU",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7082616300035379200",
        "component": "",
        "subs": [
            {
                "name": "bizSuspectedSmuggling",
                "title": "疑似窜货",
                "code": "/biz/channel/page",
                "icon": "strikethrough-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/distribution/channel/suspectedSmuggling",
                "component": "biz/distribution/channel/suspectedSmuggling/index"
            },
            {
                "name": "bizStatistics",
                "title": "窜货统计",
                "code": "biz/channel.statistics/page",
                "icon": "project-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/distribution/channel/statistics",
                "component": "biz/distribution/channel/statistics/index"
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082616682627207168",
        "title": "客群洞察",
        "code": "",
        "icon": "branches-outlined",
        "category": "MENU",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7082616682627207168",
        "component": "",
        "subs": [
            {
                "name": "bizDataDashboard",
                "title": "客群洞察",
                "code": "biz/data.dashboard/page",
                "icon": "deployment-unit-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/distribution/data/dashboard",
                "component": "biz/distribution/data/dashboard/index"
            },
            {
                "name": "bizDataScanQr",
                "title": "扫码统计",
                "code": "biz/data.scanQr/page",
                "icon": "scan-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/distribution/data/scanQr",
                "component": "biz/distribution/data/scanQr/index"
            },
            {
                "name": "bizDataSellTrend",
                "title": "销售趋势",
                "code": "biz/data.sellTrend/page",
                "icon": "monitor-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/distribution/data/sellTrend",
                "component": "biz/distribution/data/sellTrend/index"
            }
        ]
    },
    {
        "group_name": "码",
        "name": "7082614161661759488",
        "title": "标识管理",
        "code": null,
        "icon": "qrcode-outlined",
        "category": "MENU",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7082614161661759488",
        "component": null,
        "subs": [
            {
                "name": "bizCodeRule",
                "title": "生码管理",
                "code": "biz/codeRule/page",
                "icon": "holder-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/code/mark/rule",
                "component": "biz/code/mark/rule/index",
                "subs": [
                    {
                        "title": "码规则列表",
                        "code": "biz/CodeRule/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "新增码规则",
                        "code": "biz/CodeRule/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑码规则",
                        "code": "biz/CodeRule/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取详情",
                        "code": "biz/CodeRule/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除码规则",
                        "code": "biz/CodeRule/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/CodeRule/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "码规则审核",
                        "code": "biz/CodeRule/toExamine",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizCreateCode",
                "title": "生码纪录",
                "code": "biz/issue/page",
                "icon": "clock-circle-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/createCode",
                "component": "biz/code/mark/create/index",
                "subs": [
                    {
                        "title": "获取生码列表",
                        "code": "biz/issue/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "导入码包",
                        "code": "biz/issue/importCodeRelation",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "扫码关联批次",
                        "code": "biz/issue/scanCodeRelation",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/issue/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "生码详情",
                        "code": "biz/issue/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "生码",
                        "code": "biz/issue/crtQr",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "码下载",
                        "code": "biz/issue/down",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizOverdue",
                "title": "报损纪录",
                "code": "biz/overdue/page",
                "icon": "menu-fold-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/code/mark/overdue",
                "component": "biz/code/mark/overdue/index",
                "subs": [
                    {
                        "title": "导入报损",
                        "code": "biz/overdue/importBadCode",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "扫码报损",
                        "code": "biz/overdue/scanCode",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取报损详情",
                        "code": "biz/overdue/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除报损",
                        "code": "biz/overdue/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "报损列表",
                        "code": "biz/overdue/list",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizCodeSearchIdentification",
                "title": "查询标识",
                "code": "biz/code.searchIdentification/page",
                "icon": "zoom-in-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/code/mark/searchIdentification",
                "component": "biz/code/mark/searchIdentification/index"
            },
            {
                "name": "bizCodePackage",
                "title": "包装层级",
                "code": "biz/package/page",
                "icon": "pic-left-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/code/mark/package",
                "component": "biz/code/mark/package/index",
                "subs": [
                    {
                        "title": "获取包装层级列表",
                        "code": "biz/package/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "获取包装层级详情",
                        "code": "biz/package/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "添加包装层级",
                        "code": "biz/package/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更新包装层级",
                        "code": "biz/package/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "修改包装层级状态",
                        "code": "biz/package/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "删除包装层级",
                        "code": "biz/package/delete",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            }
        ]
    },
    {
        "group_name": "业务",
        "name": "7232651252348227584",
        "title": "业务管控",
        "code": "",
        "icon": "credit-card-outlined",
        "category": "MENU",
        "module": 10000,
        "menu_type": "CATALOG",
        "path": "/7232651252348227584",
        "component": "",
        "subs": [
            {
                "name": "sysIssue",
                "title": "生码管理",
                "code": "sys/issue/page",
                "icon": "logout-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/issue/index",
                "component": "sys/code/mark/create/index"
            },
            {
                "name": "sysLabelStyle",
                "title": "标签样式",
                "code": "sys/LabelStyle/page",
                "icon": "folder-open-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/label/style",
                "component": "sys/code/labelstyle/index",
                "subs": [
                    {
                        "title": "新增通用标样式",
                        "code": "sys/LabelStyle/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "编辑通用标样式",
                        "code": "sys/LabelStyle/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除通用标样式",
                        "code": "sys/LabelStyle/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "更改通用标样式状态",
                        "code": "sys/LabelStyle/status",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取通用标样式详情",
                        "code": "sys/LabelStyle/detail",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "审核通用标样式",
                        "code": "sys/LabelStyle/toExamine",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "报价",
                        "code": "sys/LabelStyle/quotation",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取通用标列表",
                        "code": "sys/LabelStyle/list",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "SysPackage",
                "title": "包装层级",
                "code": "biz/package/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/code/package",
                "component": "sys/code/mark/package/index",
                "subs": [
                    {
                        "title": "新建包装层级",
                        "code": "biz/package/add",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "编辑包装层级",
                        "code": "biz/package/edit",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "删除包装层级",
                        "code": "biz/package/delete",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "更改包装层级状态",
                        "code": "biz/package/state",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取包装层级列表",
                        "code": "biz/package/list",
                        "category": "BUTTON",
                        "module": 10000
                    },
                    {
                        "title": "获取包装层级详情",
                        "code": "biz/package/detail",
                        "category": "BUTTON",
                        "module": 10000
                    }
                ]
            },
            {
                "name": "sysPendingTrial",
                "title": "待审纪录",
                "code": "sys/PendingTrial/page",
                "icon": "issues-close-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/pending/trial",
                "component": "sys/pendingtrial/index"
            },
            {
                "name": "SysOverdue",
                "title": "报损管理",
                "code": "biz/overdue/page",
                "icon": "strikethrough-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/overdue",
                "component": "sys/code/mark/overdue/index",
                "subs": [
                    {
                        "title": "导入报损数据",
                        "code": "biz/overdue/importBadCode",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "扫码报损",
                        "code": "biz/overdue/scanCode",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "起止号报损",
                        "code": "biz/overdue/scanCodeRange",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "获取一条详情",
                        "code": "biz/overdue/detail",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "删除",
                        "code": "biz/overdue/delete",
                        "category": "BUTTON",
                        "module": 0
                    }
                ]
            },
            {
                "name": "sysGeneralizedRule",
                "title": "通码规则",
                "code": "sys/GeneralizedRule/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10000,
                "menu_type": "MENU",
                "path": "/sys/generalized/code/rule",
                "component": "sys/code/mark/rule/index",
                "subs": [
                    {
                        "title": "新建码规则",
                        "code": "sys/GeneralizedRule/add",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "编辑码规则",
                        "code": "sys/GeneralizedRule/edit",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "删除码规则",
                        "code": "sys/GeneralizedRule/delete",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "更改码规则状态",
                        "code": "sys/GeneralizedRule/state",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "获取码规则详情",
                        "code": "sys/GeneralizedRule/detail",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "码规则列表",
                        "code": "sys/GeneralizedRule/list",
                        "category": "BUTTON",
                        "module": 0
                    },
                    {
                        "title": "码规则审核",
                        "code": "sys/GeneralizedRule/toExamine",
                        "category": "BUTTON",
                        "module": 0
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082615054977208320",
        "title": "配置管理",
        "code": "",
        "icon": "form-outlined",
        "category": "MENU",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7082615054977208320",
        "component": "",
        "subs": [
            {
                "name": "bizCheck",
                "title": "检测报告模板",
                "code": "biz/template.check/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/checkTemplate",
                "component": "biz/configue/config/check/index",
                "subs": [
                    {
                        "title": "新增检测报告模板",
                        "code": "biz/template.check/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑检测报告模板",
                        "code": "biz/template.check/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "检测报告模板列表",
                        "code": "biz/template.check/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "检测报告模板删除",
                        "code": "biz/template.check/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改检测报告模板状态",
                        "code": "biz/template.check/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "检测报告模板详情",
                        "code": "biz/template.check/detail",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizInfoTemplate",
                "title": "溯源模板管理",
                "code": "biz/template.info/page",
                "icon": "retweet-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/infoTemplate",
                "component": "biz/configue/config/info/index",
                "subs": [
                    {
                        "title": "新增溯源模板管理",
                        "code": "biz/template.info/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑溯源模板管理",
                        "code": "biz/template.info/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "溯源模板删除",
                        "code": "biz/template.info/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "溯源模板列表",
                        "code": "biz/template.info/list",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改溯源模板状态",
                        "code": "biz/template.info/state",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizApplet",
                "title": "小程序模板管理",
                "code": "biz/appletTemplate/page",
                "icon": "fullscreen-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/config/applet",
                "component": "biz/configue/config/applet/index"
            },
            {
                "name": "bizConfigTemplateInfo",
                "title": "溯源模板配置",
                "code": "biz/infoTemplate/list",
                "icon": "border-outer-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/configue/InfoConfig",
                "component": "biz/configue/config/info/info"
            },
            {
                "name": "bizAppletForm",
                "title": "新增编辑小程序模板",
                "code": "biz/applet/save",
                "icon": "code-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/config/applet/form",
                "component": "biz/configue/config/applet/form"
            },
            {
                "name": "bizCertificate",
                "title": "溯源链证书配置",
                "code": "biz/template.blockchain/detail",
                "icon": "book-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/configue/certificate",
                "component": "biz/configue/config/certificate/index",
                "subs": [
                    {
                        "title": "添加区块链模板",
                        "code": "biz/template.blockchain/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更新区块链模板",
                        "code": "biz/template.blockchain/edit",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082615921826598912",
        "title": "账号管理",
        "code": "",
        "icon": "usergroup-add-outlined",
        "category": "MENU",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7082615921826598912",
        "component": "",
        "subs": [
            {
                "name": "bizAccountMember",
                "title": "成员管理",
                "code": "biz/account/page",
                "icon": "user-add-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/account/member",
                "component": "biz/configue/newAccount/member/index",
                "subs": [
                    {
                        "title": "新增成员",
                        "code": "biz/account/add",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "编辑成员",
                        "code": "biz/account/edit",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "成员详情",
                        "code": "biz/account/detail",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/account/state",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "成员删除",
                        "code": "biz/account/delete",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "成员列表",
                        "code": "biz/account/list",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "bizAccountAuthorization",
                "title": "权限管理",
                "code": "biz/authorization/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/account/authorization",
                "component": "biz/configue/newAccount/member/authorization",
                "subs": [
                    {
                        "title": "分配权限",
                        "code": "biz/authorization/auth",
                        "category": "BUTTON",
                        "module": 10005
                    },
                    {
                        "title": "角色列表",
                        "code": "biz/authorization/getRoleList",
                        "category": "BUTTON",
                        "module": 10005
                    }
                ]
            },
            {
                "name": "configDevelop",
                "title": "开发者管理",
                "code": "biz/develop/page",
                "icon": "console-sql-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/account/api",
                "component": "biz/configue/newAccount/develop/index"
            }
        ]
    },
    {
        "group_name": "其他",
        "name": "7112445948030095360",
        "title": "上链日志",
        "code": "",
        "icon": "api-outlined",
        "category": "MENU",
        "module": 10005,
        "menu_type": "CATALOG",
        "path": "/7112445948030095360",
        "component": "",
        "subs": [
            {
                "name": "bizFisheryBlockChain",
                "title": "上链纪录",
                "code": "biz/BlockChain/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10005,
                "menu_type": "MENU",
                "path": "/biz/fishery/BlockChain",
                "component": "biz/blockChain/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066387589372055552",
        "title": "商户管理",
        "code": "",
        "icon": "shop-outlined",
        "category": "",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7066387589372055552",
        "component": "",
        "subs": [
            {
                "name": "bizLivestockMerchant",
                "title": "商户管理",
                "code": "biz/merchant/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/merchant",
                "component": "biz/merchants/merchant/index",
                "subs": [
                    {
                        "title": "新增商户",
                        "code": "biz/merchant/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑商户",
                        "code": "biz/merchant/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取一个商户详情",
                        "code": "biz/merchant/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除商户",
                        "code": "biz/merchant/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改商户状态",
                        "code": "biz/merchant/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "授权商户子系统",
                        "code": "biz/merchant/authorize",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "商户审核",
                        "code": "biz/merchant/toExamine",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "信息录入规范设置",
                        "code": "biz/merchant/infoType",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取商户列表",
                        "code": "biz/merchant/list",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockSupplier",
                "title": "供应商管理",
                "code": "biz/supplier/page",
                "icon": "user-switch-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/supplier",
                "component": "biz/merchants/supplier/index",
                "subs": [
                    {
                        "title": "获取供应商详情",
                        "code": "biz/supplier/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增供应商",
                        "code": "biz/supplier/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑供应商",
                        "code": "biz/supplier/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除供应商",
                        "code": "biz/supplier/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取供应商列表",
                        "code": "biz/supplier/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改供应商状态",
                        "code": "biz/supplier/state",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockDistributor",
                "title": "经销商管理",
                "code": "biz/distributor/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/distributor",
                "component": "biz/merchants/distributor/index",
                "subs": [
                    {
                        "title": "获取经销商详情",
                        "code": "biz/distributor/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增经销商",
                        "code": "biz/distributor/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑经销商",
                        "code": "biz/distributor/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除经销商",
                        "code": "biz/distributor/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "批量导入经销商",
                        "code": "biz/distributor/importData",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取经销商列表",
                        "code": "biz/distributor/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改经销商状态",
                        "code": "biz/distributor/state",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockEmployees",
                "title": "员工管理",
                "code": "biz/employees/page",
                "icon": "usergroup-add-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/employees",
                "component": "biz/merchants/employees/index",
                "subs": [
                    {
                        "title": "获取员工列表",
                        "code": "biz/employees/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增员工",
                        "code": "biz/employees/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑员工",
                        "code": "biz/employees/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除员工",
                        "code": "biz/employees/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取员工详情",
                        "code": "biz/employees/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改员工关态",
                        "code": "biz/employees/state",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066706762136555520",
        "title": "商品管理",
        "code": "",
        "icon": "picture-outlined",
        "category": "",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7066706762136555520",
        "component": "",
        "subs": [
            {
                "name": "goodsCateLivestock",
                "title": "商品分类",
                "code": "biz/cate/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/cate",
                "component": "biz/goods/cate/index",
                "subs": [
                    {
                        "title": "获取菜单列表",
                        "code": "biz/cate/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取菜单树",
                        "code": "biz/cate/tree",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增菜单",
                        "code": "biz/cate/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑菜单",
                        "code": "biz/cate/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取菜单详情",
                        "code": "biz/cate/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除菜单",
                        "code": "biz/cate/delete",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockGoods",
                "title": "商品管理",
                "code": "biz/goods/page",
                "icon": "gift-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/goods",
                "component": "biz/goods/commodity/index",
                "subs": [
                    {
                        "title": "获取商品详情",
                        "code": "biz/goods/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增商品",
                        "code": "biz/goods/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑商品",
                        "code": "biz/goods/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除商品",
                        "code": "biz/goods/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取商品列表",
                        "code": "biz/goods/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改商品状态",
                        "code": "biz/goods/state",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizGoodsInfo",
                "title": "商品信息配置",
                "code": "biz/batch.goodsInfo/page",
                "icon": "diff-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/batch/goodsInfoConfig",
                "component": "sub/public/trace/goodsInfoConfig"
            },
            {
                "name": "bizLivestockGoodsBrand",
                "title": "品牌管理",
                "code": "biz/brand/page",
                "icon": "trademark-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/brand",
                "component": "biz/goods/brand/index",
                "subs": [
                    {
                        "title": "获取商品品牌详情",
                        "code": "biz/brand/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "商品品牌新增",
                        "code": "biz/brand/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "商品品牌编辑",
                        "code": "biz/brand/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "商品品牌删除",
                        "code": "biz/brand/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改品牌状态",
                        "code": "biz/brand/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取品牌列表",
                        "code": "biz/brand/list",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockRawMaterial",
                "title": "投入品管理",
                "code": "biz/RawMaterial/page",
                "icon": "block-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/inputs",
                "component": "biz/goods/inputs/index",
                "subs": [
                    {
                        "title": "获取投入品列表",
                        "code": "biz/RawMaterial/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取投入品详情",
                        "code": "biz/RawMaterial/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增投入品",
                        "code": "biz/RawMaterial/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑投入品",
                        "code": "biz/RawMaterial/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除投入品",
                        "code": "biz/RawMaterial/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/RawMaterial/state",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockGoodsCerification",
                "title": "商品认证",
                "code": "biz/cerification/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/cerification",
                "component": "biz/goods/cerification/index",
                "subs": [
                    {
                        "title": "商品认证列表",
                        "code": "biz/cerification/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取商品认证详情",
                        "code": "biz/cerification/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增商品认证",
                        "code": "biz/cerification/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑商品认证",
                        "code": "biz/cerification/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除商品认证",
                        "code": "biz/cerification/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/cerification/state",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockShop",
                "title": "店铺管理",
                "code": "biz/shop/page",
                "icon": "shop-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/shop",
                "component": "biz/goods/shop/index",
                "subs": [
                    {
                        "title": "获取店铺列表",
                        "code": "biz/shop/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增店铺",
                        "code": "biz/shop/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑店铺",
                        "code": "biz/shop/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除店铺",
                        "code": "biz/shop/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取店铺详情",
                        "code": "biz/shop/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/shop/state",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizHyperInfo",
                "title": "环节信息配置",
                "code": "biz/batch.hyper/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/batch/hyperInfo",
                "component": "sub/public/trace/hyperInfo",
                "subs": [
                    {
                        "title": "新增环节",
                        "code": "/biz/hyper/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑环节",
                        "code": "/biz/hyper/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除环节",
                        "code": "/biz/hyper/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改状态",
                        "code": "/biz/hyper/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "环节列表",
                        "code": "/biz/hyper/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "环节详情",
                        "code": "/biz/hyper/detail",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7068831409917399040",
        "title": "原料采购",
        "code": "",
        "icon": "login-outlined",
        "category": "",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7068831409917399040",
        "component": "",
        "subs": [
            {
                "name": "bizLivestockProductMateral",
                "title": "投入品采购",
                "code": "biz/product.material/page",
                "icon": "plus-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/product/material",
                "component": "sub/livestock/product/material/index",
                "subs": [
                    {
                        "title": "获取投入品采购列表",
                        "code": "biz/product.material/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取投入品采购详情",
                        "code": "biz/product.material/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增投入品采购",
                        "code": "biz/product.material/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑投入品采购",
                        "code": "biz/product.material/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除投入品采购",
                        "code": "biz/product.material/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "上链",
                        "code": "biz/product.material/upChain",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改投入品采购状态",
                        "code": "biz/product.material/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/product.material/freeze",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockProductMaterialBatch",
                "title": "投入品批次",
                "code": "biz/product.MaterialItem/page",
                "icon": "pic-right-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/product/material/batch",
                "component": "sub/livestock/product/material/batch",
                "subs": [
                    {
                        "title": "获取投入品批次列表",
                        "code": "biz/product.MaterialItem/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取投入品批次详情（预览）",
                        "code": "biz/product.MaterialItem/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改投入品批次状态（冻结）",
                        "code": "biz/product.MaterialItem/freeze",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7072936412537950208",
        "title": "农事管理",
        "code": "",
        "icon": "file-zip-outlined",
        "category": "MENU",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7072936412537950208",
        "component": "",
        "subs": [
            {
                "name": "bizLivestockPlot",
                "title": "牧场管理",
                "code": "biz/planting.plot/page",
                "icon": "radius-bottomright-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/agriculturalManagement/pasture",
                "component": "sub/livestock/agriculturalManagement/plot/index",
                "subs": [
                    {
                        "title": "新增牧场",
                        "code": "biz/planting.plot/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑牧场",
                        "code": "biz/planting.plot/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "牧场详情",
                        "code": "biz/planting.plot/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改牧场状态",
                        "code": "biz/planting.plot/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除牧场",
                        "code": "biz/planting.plot/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "牧场列表",
                        "code": "biz/planting.plot/list",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockPlan",
                "title": "饲养计划",
                "code": "biz/planting.plan/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/agriculturalManagement/feedingPlan",
                "component": "sub/livestock/agriculturalManagement/plan/index",
                "subs": [
                    {
                        "title": "新增计划",
                        "code": "biz/planting.plan/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑计划",
                        "code": "biz/planting.plan/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "计划详情",
                        "code": "biz/planting.plan/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改计划状态",
                        "code": "biz/planting.plan/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除计划",
                        "code": "biz/planting.plan/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "计划列表",
                        "code": "biz/planting.plan/list",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLivestockWork",
                "title": "饲养作业",
                "code": "biz/planting.work/page",
                "icon": "account-book-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/agriculturalManagement/feedingWork",
                "component": "sub/livestock/agriculturalManagement/work/index",
                "subs": [
                    {
                        "title": "新增作业",
                        "code": "biz/planting.work/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑作业",
                        "code": "biz/planting.work/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "作业详情",
                        "code": "biz/planting.work/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改作业状态",
                        "code": "biz/planting.work/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除作业",
                        "code": "biz/planting.work/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "作业列表",
                        "code": "biz/planting.work/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "上链",
                        "code": "biz/planting.work/upChain",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/planting.work/freeze",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizDevice",
                "title": "设备管理",
                "code": "biz/device/page",
                "icon": "alert-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "biz/device",
                "component": "biz/device/index",
                "subs": [
                    {
                        "title": "设备列表",
                        "code": "biz/device/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增设备",
                        "code": "biz/device/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "设备改名",
                        "code": "biz/device/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "设备删除",
                        "code": "biz/device/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "设备播放",
                        "code": "biz/device/play",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "设备抓拍",
                        "code": "biz/device/capture",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizLiveStockDevicePlay",
                "title": "设备播放",
                "code": "biz/device/play",
                "icon": "play-circle-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/device/play",
                "component": "biz/device/play"
            },
            {
                "name": "bizLiveStockFarmingInfo",
                "title": "农事活动配置",
                "code": "biz/batch.farming/page",
                "icon": "contacts-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/batch/farmingInfo",
                "component": "sub/public/trace/farmingInfo"
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7070765572912320512",
        "title": "生产管理",
        "code": "",
        "icon": "scissor-outlined",
        "category": "",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7070765572912320512",
        "component": "",
        "subs": [
            {
                "name": "bizProductLivestockProduceIn",
                "title": "出栏入库",
                "code": "biz/trace.ProduceIn/page",
                "icon": "build-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/product/produce/in",
                "component": "sub/livestock/product/produceIn/index",
                "subs": [
                    {
                        "title": "获取出栏入库信息列表",
                        "code": "biz/trace.ProduceIn/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取出栏入库详情",
                        "code": "biz/trace.ProduceIn/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增出栏入库",
                        "code": "biz/trace.ProduceIn/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑出栏入库",
                        "code": "biz/trace.ProduceIn/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除出栏入库",
                        "code": "biz/trace.ProduceIn/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改出栏入库状态（冻结，解冻）",
                        "code": "biz/trace.ProduceIn/freeze",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.ProduceIn/upChain",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizProductLivestockProduceInBatch",
                "title": "出栏批次",
                "code": "biz/trace.ProduceInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/product/produce/in/batch",
                "component": "sub/livestock/product/produceIn/batch",
                "subs": [
                    {
                        "title": "获取出栏批次列表",
                        "code": "biz/trace.ProduceInItem/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改出栏入库批次状态（冻结，解冻）",
                        "code": "biz/trace.ProduceInItem/freeze",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取出栏入库批次详情",
                        "code": "biz/trace.ProduceInItem/detail",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7075384936848429056",
        "title": "检验检疫",
        "code": "",
        "icon": "bug-outlined",
        "category": "MENU",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7075384936848429056",
        "component": "",
        "subs": [
            {
                "name": "bizLivestockQuarantineReport",
                "title": "检验检疫",
                "code": "biz/QuarantineReport/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/QuarantineReport",
                "component": "biz/quarantineReport/index",
                "subs": [
                    {
                        "title": "获取检验检疫列表",
                        "code": "biz/QuarantineReport/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增检验检疫",
                        "code": "biz/QuarantineReport/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑检验检疫",
                        "code": "biz/QuarantineReport/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除检验检疫",
                        "code": "biz/QuarantineReport/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取检验检疫详情",
                        "code": "biz/QuarantineReport/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "激活检验检疫",
                        "code": "biz/QuarantineReport/freeze",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "上链",
                        "code": "biz/QuarantineReport/upChain",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7070754898417029120",
        "title": "出库管理",
        "code": "",
        "icon": "minus-square-outlined",
        "category": "",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7070754898417029120",
        "component": "",
        "subs": [
            {
                "name": "bizLivestockProductStockOut",
                "title": "出库管理",
                "code": "biz/trace.StockOut/page",
                "icon": "rollback-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/livestock/product/stock/out",
                "component": "sub/livestock/product/stockOut/index",
                "subs": [
                    {
                        "title": "获取商品出库信息列表",
                        "code": "biz/trace.StockOut/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取商品出库详情",
                        "code": "biz/trace.StockOut/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "添加商品出库",
                        "code": "biz/trace.StockOut/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑出库信息",
                        "code": "biz/trace.StockOut/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除出库信息",
                        "code": "biz/trace.StockOut/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改出库状态",
                        "code": "biz/trace.StockOut/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockOut/upChain",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/trace.StockOut/freeze",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082620809352581120",
        "title": "渠道管理",
        "code": "",
        "icon": "swap-outlined",
        "category": "MENU",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7082620809352581120",
        "component": "",
        "subs": [
            {
                "name": "bizSuspectedSmuggling",
                "title": "疑似窜货",
                "code": "/biz/channel/page",
                "icon": "strikethrough-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/distribution/channel/suspectedSmuggling",
                "component": "biz/distribution/channel/suspectedSmuggling/index"
            },
            {
                "name": "bizStatistics",
                "title": "窜货统计",
                "code": "biz/channel.statistics/page",
                "icon": "project-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/distribution/channel/statistics",
                "component": "biz/distribution/channel/statistics/index"
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082621258570928128",
        "title": "客群洞察",
        "code": "",
        "icon": "branches-outlined",
        "category": "MENU",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7082621258570928128",
        "component": "",
        "subs": [
            {
                "name": "bizDataDashboard",
                "title": "客群洞察",
                "code": "biz/data.dashboard/page",
                "icon": "deployment-unit-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/distribution/data/dashboard",
                "component": "biz/distribution/data/dashboard/index"
            },
            {
                "name": "bizDataScanQr",
                "title": "扫码统计",
                "code": "biz/data.scanQr/page",
                "icon": "scan-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/distribution/data/scanQr",
                "component": "biz/distribution/data/scanQr/index"
            },
            {
                "name": "bizDataSellTrend",
                "title": "销售趋势",
                "code": "biz/data.sellTrend/page",
                "icon": "monitor-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/distribution/data/sellTrend",
                "component": "biz/distribution/data/sellTrend/index"
            }
        ]
    },
    {
        "group_name": "码",
        "name": "7082617276670676992",
        "title": "标识管理",
        "code": "",
        "icon": "qrcode-outlined",
        "category": "MENU",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7082617276670676992",
        "component": "",
        "subs": [
            {
                "name": "bizCodeRule",
                "title": "生码管理",
                "code": "biz/codeRule/page",
                "icon": "holder-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/code/mark/rule",
                "component": "biz/code/mark/rule/index",
                "subs": [
                    {
                        "title": "码规则列表",
                        "code": "biz/CodeRule/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "新增码规则",
                        "code": "biz/CodeRule/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑码规则",
                        "code": "biz/CodeRule/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取详情",
                        "code": "biz/CodeRule/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除码规则",
                        "code": "biz/CodeRule/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/CodeRule/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "码规则审核",
                        "code": "biz/CodeRule/toExamine",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizCreateCode",
                "title": "生码纪录",
                "code": "biz/issue/page",
                "icon": "clock-circle-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/createCode",
                "component": "biz/code/mark/create/index",
                "subs": [
                    {
                        "title": "获取生码列表",
                        "code": "biz/issue/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "导入码包",
                        "code": "biz/issue/importCodeRelation",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "扫码关联批次",
                        "code": "biz/issue/scanCodeRelation",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/issue/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "生码详情",
                        "code": "biz/issue/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "生码",
                        "code": "biz/issue/crtQr",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "码下载",
                        "code": "biz/issue/down",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizOverdue",
                "title": "报损纪录",
                "code": "biz/overdue/page",
                "icon": "menu-fold-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/code/mark/overdue",
                "component": "biz/code/mark/overdue/index",
                "subs": [
                    {
                        "title": "导入报损",
                        "code": "biz/overdue/importBadCode",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "扫码报损",
                        "code": "biz/overdue/scanCode",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取报损详情",
                        "code": "biz/overdue/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除报损",
                        "code": "biz/overdue/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "报损列表",
                        "code": "biz/overdue/list",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizCodeSearchIdentification",
                "title": "查询标识",
                "code": "biz/code.searchIdentification/page",
                "icon": "zoom-in-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/code/mark/searchIdentification",
                "component": "biz/code/mark/searchIdentification/index"
            },
            {
                "name": "bizCodePackage",
                "title": "包装层级",
                "code": "biz/package/page",
                "icon": "pic-left-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/code/mark/package",
                "component": "biz/code/mark/package/index",
                "subs": [
                    {
                        "title": "获取包装层级列表",
                        "code": "biz/package/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "获取包装层级详情",
                        "code": "biz/package/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "添加包装层级",
                        "code": "biz/package/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更新包装层级",
                        "code": "biz/package/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "修改包装层级状态",
                        "code": "biz/package/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "删除包装层级",
                        "code": "biz/package/delete",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082619549824061440",
        "title": "配置管理",
        "code": "",
        "icon": "form-outlined",
        "category": "MENU",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7082619549824061440",
        "component": "",
        "subs": [
            {
                "name": "bizCheck",
                "title": "检测报告模板",
                "code": "biz/template.check/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/checkTemplate",
                "component": "biz/configue/config/check/index",
                "subs": [
                    {
                        "title": "新增检测报告模板",
                        "code": "biz/template.check/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑检测报告模板",
                        "code": "biz/template.check/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "检测报告模板列表",
                        "code": "biz/template.check/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "检测报告模板删除",
                        "code": "biz/template.check/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改检测报告模板状态",
                        "code": "biz/template.check/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "检测报告模板详情",
                        "code": "biz/template.check/detail",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizInfoTemplate",
                "title": "溯源模板管理",
                "code": "biz/template.info/page",
                "icon": "retweet-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/infoTemplate",
                "component": "biz/configue/config/info/index",
                "subs": [
                    {
                        "title": "新增溯源模板管理",
                        "code": "biz/template.info/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑溯源模板管理",
                        "code": "biz/template.info/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "溯源模板删除",
                        "code": "biz/template.info/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "溯源模板列表",
                        "code": "biz/template.info/list",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改溯源模板状态",
                        "code": "biz/template.info/state",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizApplet",
                "title": "小程序模板管理",
                "code": "biz/appletTemplate/page",
                "icon": "fullscreen-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/config/applet",
                "component": "biz/configue/config/applet/index"
            },
            {
                "name": "bizConfigTemplateInfo",
                "title": "溯源模板配置",
                "code": "biz/infoTemplate/list",
                "icon": "border-outer-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/configue/InfoConfig",
                "component": "biz/configue/config/info/info"
            },
            {
                "name": "bizAppletForm",
                "title": "新增编辑小程序模板",
                "code": "biz/applet/save",
                "icon": "code-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/config/applet/form",
                "component": "biz/configue/config/applet/form"
            },
            {
                "name": "bizCertificate",
                "title": "溯源链证书配置",
                "code": "biz/template.blockchain/detail",
                "icon": "book-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/configue/certificate",
                "component": "biz/configue/config/certificate/index",
                "subs": [
                    {
                        "title": "添加区块链模板",
                        "code": "biz/template.blockchain/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更新区块链模板",
                        "code": "biz/template.blockchain/edit",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082620401745924096",
        "title": "账号管理",
        "code": "",
        "icon": "usergroup-add-outlined",
        "category": "MENU",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7082620401745924096",
        "component": "",
        "subs": [
            {
                "name": "bizAccountMember",
                "title": "成员管理",
                "code": "biz/account/page",
                "icon": "user-add-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/account/member",
                "component": "biz/configue/newAccount/member/index",
                "subs": [
                    {
                        "title": "新增成员",
                        "code": "biz/account/add",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "编辑成员",
                        "code": "biz/account/edit",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "成员详情",
                        "code": "biz/account/detail",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/account/state",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "成员删除",
                        "code": "biz/account/delete",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "成员列表",
                        "code": "biz/account/list",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "bizAccountAuthorization",
                "title": "权限管理",
                "code": "biz/authorization/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/account/authorization",
                "component": "biz/configue/newAccount/member/authorization",
                "subs": [
                    {
                        "title": "分配权限",
                        "code": "biz/authorization/auth",
                        "category": "BUTTON",
                        "module": 10006
                    },
                    {
                        "title": "角色列表",
                        "code": "biz/authorization/getRoleList",
                        "category": "BUTTON",
                        "module": 10006
                    }
                ]
            },
            {
                "name": "configDevelop",
                "title": "开发者管理",
                "code": "biz/develop/page",
                "icon": "console-sql-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/account/api",
                "component": "biz/configue/newAccount/develop/index"
            }
        ]
    },
    {
        "group_name": "其他",
        "name": "7112446385982541824",
        "title": "上链日志",
        "code": "",
        "icon": "api-outlined",
        "category": "MENU",
        "module": 10006,
        "menu_type": "CATALOG",
        "path": "/7112446385982541824",
        "component": "",
        "subs": [
            {
                "name": "bizLivestockBlockChain",
                "title": "上链纪录",
                "code": "biz/BlockChain/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10006,
                "menu_type": "MENU",
                "path": "/biz/livestock/BlockChain",
                "component": "biz/blockChain/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066387589372055552",
        "title": "商户管理",
        "code": "",
        "icon": "shop-outlined",
        "category": "",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7066387589372055552",
        "component": "",
        "subs": [
            {
                "name": "bizRetailMerchant",
                "title": "商户管理",
                "code": "biz/merchant/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/merchant",
                "component": "biz/merchants/merchant/index",
                "subs": [
                    {
                        "title": "新增商户",
                        "code": "biz/merchant/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑商户",
                        "code": "biz/merchant/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取一个商户详情",
                        "code": "biz/merchant/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除商户",
                        "code": "biz/merchant/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取商户列表",
                        "code": "biz/merchant/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改商户状态",
                        "code": "biz/merchant/state",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "授权商户子系统",
                        "code": "biz/merchant/authorize",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "商户审核",
                        "code": "biz/merchant/toExamine",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "信息录入规范设置",
                        "code": "biz/merchant/infoType",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizRetailSupplier",
                "title": "供应商管理",
                "code": "biz/supplier/page",
                "icon": "user-switch-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/supplier",
                "component": "biz/merchants/supplier/index",
                "subs": [
                    {
                        "title": "获取供应商详情",
                        "code": "biz/supplier/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "新增供应商",
                        "code": "biz/supplier/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑供应商",
                        "code": "biz/supplier/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除供应商",
                        "code": "biz/supplier/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取供应商列表",
                        "code": "biz/supplier/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改供应商状态",
                        "code": "biz/supplier/state",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizRetailDistributor",
                "title": "经销商管理",
                "code": "biz/distributor/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/distributor",
                "component": "biz/merchants/distributor/index",
                "subs": [
                    {
                        "title": "获取经销商详情",
                        "code": "biz/distributor/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "新增经销商",
                        "code": "biz/distributor/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑经销商",
                        "code": "biz/distributor/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除经销商",
                        "code": "biz/distributor/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "批量导入经销商",
                        "code": "biz/distributor/importData",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取经销商列表",
                        "code": "biz/distributor/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改经销商状态",
                        "code": "biz/distributor/state",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7066706762136555520",
        "title": "商品管理",
        "code": "",
        "icon": "picture-outlined",
        "category": "",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7066706762136555520",
        "component": "",
        "subs": [
            {
                "name": "goodsCateRetail",
                "title": "商品分类",
                "code": "biz/cate/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/cate",
                "component": "biz/goods/cate/index",
                "subs": [
                    {
                        "title": "获取菜单列表",
                        "code": "biz/cate/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取菜单树",
                        "code": "biz/cate/tree",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "新增菜单",
                        "code": "biz/cate/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑菜单",
                        "code": "biz/cate/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取菜单详情",
                        "code": "biz/cate/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除菜单",
                        "code": "biz/cate/delete",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizRetailGoods",
                "title": "商品管理",
                "code": "biz/goods/page",
                "icon": "gift-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/goods",
                "component": "biz/goods/commodity/index",
                "subs": [
                    {
                        "title": "获取商品详情",
                        "code": "biz/goods/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "新增商品",
                        "code": "biz/goods/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑商品",
                        "code": "biz/goods/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除商品",
                        "code": "biz/goods/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取商品列表",
                        "code": "biz/goods/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改商品状态",
                        "code": "biz/goods/state",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizGoodsInfo",
                "title": "商品信息配置",
                "code": "biz/batch.goodsInfo/page",
                "icon": "diff-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/batch/goodsInfoConfig",
                "component": "sub/public/trace/goodsInfoConfig"
            },
            {
                "name": "bizRetailGoodsBrand",
                "title": "品牌管理",
                "code": "biz/brand/page",
                "icon": "trademark-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/brand",
                "component": "biz/goods/brand/index",
                "subs": [
                    {
                        "title": "获取商品品牌详情",
                        "code": "biz/brand/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "商品品牌新增",
                        "code": "biz/brand/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "商品品牌编辑",
                        "code": "biz/brand/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "商品品牌删除",
                        "code": "biz/brand/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改品牌状态",
                        "code": "biz/brand/state",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取品牌列表",
                        "code": "biz/brand/list",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizRetailGoodsCerification",
                "title": "商品认证",
                "code": "biz/cerification/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/cerification",
                "component": "biz/goods/cerification/index",
                "subs": [
                    {
                        "title": "商品认证列表",
                        "code": "biz/cerification/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取商品认证详情",
                        "code": "biz/cerification/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "新增商品认证",
                        "code": "biz/cerification/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑商品认证",
                        "code": "biz/cerification/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除商品认证",
                        "code": "biz/cerification/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/cerification/state",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizRetailShop",
                "title": "店铺管理",
                "code": "biz/shop/page",
                "icon": "shop-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/shop",
                "component": "biz/goods/shop/index",
                "subs": [
                    {
                        "title": "获取店铺列表",
                        "code": "biz/shop/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "新增店铺",
                        "code": "biz/shop/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑店铺",
                        "code": "biz/shop/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除店铺",
                        "code": "biz/shop/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取店铺详情",
                        "code": "biz/shop/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/shop/state",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizHyperInfo",
                "title": "环节信息配置",
                "code": "biz/batch.hyper/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/batch/hyperInfo",
                "component": "sub/public/trace/hyperInfo",
                "subs": [
                    {
                        "title": "新增环节",
                        "code": "/biz/hyper/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑环节",
                        "code": "/biz/hyper/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除环节",
                        "code": "/biz/hyper/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改状态",
                        "code": "/biz/hyper/state",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "环节列表",
                        "code": "/biz/hyper/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "环节详情",
                        "code": "/biz/hyper/detail",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7070752739495514112",
        "title": "入库管理",
        "code": "",
        "icon": "plus-square-outlined",
        "category": "",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/biz/trace/stock/in",
        "component": "",
        "subs": [
            {
                "name": "bizRetailProductStockIn",
                "title": "入库管理",
                "code": "biz/trace.StockIn/page",
                "icon": "arrow-right-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/product/stock/in",
                "component": "sub/retail/product/stockIn/index",
                "subs": [
                    {
                        "title": "获取商品入库信息列表",
                        "code": "biz/trace.StockIn/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取商品入库详情",
                        "code": "biz/trace.StockIn/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "添加商品入库",
                        "code": "biz/trace.StockIn/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑商品入库",
                        "code": "biz/trace.StockIn/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除入库信息",
                        "code": "biz/trace.StockIn/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockIn/upChain",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改商品入库状态（冻结，解冻）",
                        "code": "biz/trace.StockIn/freeze",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizRetailProductStockInBatch",
                "title": "入库批次",
                "code": "biz/trace.StockInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/product/stock/in/batch",
                "component": "sub/retail/product/stockIn/batch",
                "subs": [
                    {
                        "title": "获取入库批次列表",
                        "code": "biz/trace.StockInItem/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改商品入库批次状态（冻结，解冻）",
                        "code": "biz/trace.StockInItem/freeze",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取入库批次详情",
                        "code": "biz/trace.StockInItem/detail",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7075385029198614528",
        "title": "检验检疫",
        "code": "",
        "icon": "bug-outlined",
        "category": "MENU",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7075385029198614528",
        "component": "",
        "subs": [
            {
                "name": "bizRetailQuarantineReport",
                "title": "检验检疫",
                "code": "biz/QuarantineReport/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/QuarantineReport",
                "component": "biz/quarantineReport/index",
                "subs": [
                    {
                        "title": "获取检验检疫列表",
                        "code": "biz/QuarantineReport/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "新增检验检疫",
                        "code": "biz/QuarantineReport/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑检验检疫",
                        "code": "biz/QuarantineReport/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除检验检疫",
                        "code": "biz/QuarantineReport/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取检验检疫详情",
                        "code": "biz/QuarantineReport/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "激活检验检疫",
                        "code": "biz/QuarantineReport/freeze",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "上链",
                        "code": "biz/QuarantineReport/upChain",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7070754898417029120",
        "title": "出库管理",
        "code": "",
        "icon": "minus-square-outlined",
        "category": "",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7070754898417029120",
        "component": "",
        "subs": [
            {
                "name": "bizRetailProductStockOut",
                "title": "出库管理",
                "code": "biz/trace.StockOut/page",
                "icon": "rollback-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/retail/product/stock/out",
                "component": "sub/retail/product/stockOut/index",
                "subs": [
                    {
                        "title": "获取商品出库信息列表",
                        "code": "biz/trace.StockOut/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取商品出库详情",
                        "code": "biz/trace.StockOut/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "添加商品出库",
                        "code": "biz/trace.StockOut/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑出库信息",
                        "code": "biz/trace.StockOut/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除出库信息",
                        "code": "biz/trace.StockOut/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改出库状态",
                        "code": "biz/trace.StockOut/state",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockOut/upChain",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/trace.StockOut/freeze",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082624652828217344",
        "title": "渠道管理",
        "code": "",
        "icon": "swap-outlined",
        "category": "MENU",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7082624652828217344",
        "component": "",
        "subs": [
            {
                "name": "bizSuspectedSmuggling",
                "title": "疑似窜货",
                "code": "/biz/channel/page",
                "icon": "strikethrough-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/distribution/channel/suspectedSmuggling",
                "component": "biz/distribution/channel/suspectedSmuggling/index"
            },
            {
                "name": "bizStatistics",
                "title": "窜货统计",
                "code": "biz/channel.statistics/page",
                "icon": "project-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/distribution/channel/statistics",
                "component": "biz/distribution/channel/statistics/index"
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7082625059247886336",
        "title": "客群洞察",
        "code": "",
        "icon": "branches-outlined",
        "category": "MENU",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7082625059247886336",
        "component": "",
        "subs": [
            {
                "name": "bizDataDashboard",
                "title": "客群洞察",
                "code": "biz/data.dashboard/page",
                "icon": "deployment-unit-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/distribution/data/dashboard",
                "component": "biz/distribution/data/dashboard/index"
            },
            {
                "name": "bizDataScanQr",
                "title": "扫码统计",
                "code": "biz/data.scanQr/page",
                "icon": "scan-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/distribution/data/scanQr",
                "component": "biz/distribution/data/scanQr/index"
            },
            {
                "name": "bizDataSellTrend",
                "title": "销售趋势",
                "code": "biz/data.sellTrend/page",
                "icon": "monitor-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/distribution/data/sellTrend",
                "component": "biz/distribution/data/sellTrend/index"
            }
        ]
    },
    {
        "group_name": "码",
        "name": "7082622089500299264",
        "title": "标识管理",
        "code": "",
        "icon": "qrcode-outlined",
        "category": "MENU",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7082622089500299264",
        "component": "",
        "subs": [
            {
                "name": "bizCodeRule",
                "title": "生码管理",
                "code": "biz/codeRule/page",
                "icon": "holder-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/code/mark/rule",
                "component": "biz/code/mark/rule/index",
                "subs": [
                    {
                        "title": "码规则列表",
                        "code": "biz/CodeRule/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "新增码规则",
                        "code": "biz/CodeRule/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑码规则",
                        "code": "biz/CodeRule/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取详情",
                        "code": "biz/CodeRule/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除码规则",
                        "code": "biz/CodeRule/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/CodeRule/state",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "码规则审核",
                        "code": "biz/CodeRule/toExamine",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizCreateCode",
                "title": "生码纪录",
                "code": "biz/issue/page",
                "icon": "clock-circle-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/createCode",
                "component": "biz/code/mark/create/index",
                "subs": [
                    {
                        "title": "获取生码列表",
                        "code": "biz/issue/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "导入码包",
                        "code": "biz/issue/importCodeRelation",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "扫码关联批次",
                        "code": "biz/issue/scanCodeRelation",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/issue/state",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "生码详情",
                        "code": "biz/issue/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "生码",
                        "code": "biz/issue/crtQr",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "码下载",
                        "code": "biz/issue/down",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizOverdue",
                "title": "报损纪录",
                "code": "biz/overdue/page",
                "icon": "menu-fold-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/code/mark/overdue",
                "component": "biz/code/mark/overdue/index",
                "subs": [
                    {
                        "title": "导入报损",
                        "code": "biz/overdue/importBadCode",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "扫码报损",
                        "code": "biz/overdue/scanCode",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取报损详情",
                        "code": "biz/overdue/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除报损",
                        "code": "biz/overdue/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "报损列表",
                        "code": "biz/overdue/list",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizCodeSearchIdentification",
                "title": "查询标识",
                "code": "biz/code.searchIdentification/page",
                "icon": "zoom-in-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/code/mark/searchIdentification",
                "component": "biz/code/mark/searchIdentification/index"
            },
            {
                "name": "bizCodePackage",
                "title": "包装层级",
                "code": "biz/package/page",
                "icon": "pic-left-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/code/mark/package",
                "component": "biz/code/mark/package/index",
                "subs": [
                    {
                        "title": "获取包装层级列表",
                        "code": "biz/package/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "获取包装层级详情",
                        "code": "biz/package/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "添加包装层级",
                        "code": "biz/package/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更新包装层级",
                        "code": "biz/package/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "修改包装层级状态",
                        "code": "biz/package/state",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "删除包装层级",
                        "code": "biz/package/delete",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082623115699687424",
        "title": "配置管理",
        "code": "",
        "icon": "form-outlined",
        "category": "MENU",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7082623115699687424",
        "component": "",
        "subs": [
            {
                "name": "bizCheck",
                "title": "检测报告模板",
                "code": "biz/template.check/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/checkTemplate",
                "component": "biz/configue/config/check/index",
                "subs": [
                    {
                        "title": "新增检测报告模板",
                        "code": "biz/template.check/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑检测报告模板",
                        "code": "biz/template.check/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "检测报告模板列表",
                        "code": "biz/template.check/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "检测报告模板删除",
                        "code": "biz/template.check/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改检测报告模板状态",
                        "code": "biz/template.check/state",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "检测报告模板详情",
                        "code": "biz/template.check/detail",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizInfoTemplate",
                "title": "溯源模板管理",
                "code": "biz/template.info/page",
                "icon": "retweet-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/infoTemplate",
                "component": "biz/configue/config/info/index",
                "subs": [
                    {
                        "title": "新增溯源模板管理",
                        "code": "biz/template.info/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑溯源模板管理",
                        "code": "biz/template.info/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "溯源模板删除",
                        "code": "biz/template.info/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "溯源模板列表",
                        "code": "biz/template.info/list",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改溯源模板状态",
                        "code": "biz/template.info/state",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizApplet",
                "title": "小程序模板管理",
                "code": "biz/appletTemplate/page",
                "icon": "fullscreen-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/config/applet",
                "component": "biz/configue/config/applet/index"
            },
            {
                "name": "bizConfigTemplateInfo",
                "title": "溯源模板配置",
                "code": "biz/infoTemplate/list",
                "icon": "border-outer-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/configue/InfoConfig",
                "component": "biz/configue/config/info/info"
            },
            {
                "name": "bizAppletForm",
                "title": "新增编辑小程序模板",
                "code": "biz/applet/save",
                "icon": "code-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/config/applet/form",
                "component": "biz/configue/config/applet/form"
            },
            {
                "name": "bizCertificate",
                "title": "溯源链证书配置",
                "code": "biz/template.blockchain/detail",
                "icon": "book-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/configue/certificate",
                "component": "biz/configue/config/certificate/index",
                "subs": [
                    {
                        "title": "添加区块链模板",
                        "code": "biz/template.blockchain/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更新区块链模板",
                        "code": "biz/template.blockchain/edit",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7082624236375773184",
        "title": "账号管理",
        "code": "",
        "icon": "usergroup-add-outlined",
        "category": "MENU",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7082624236375773184",
        "component": "",
        "subs": [
            {
                "name": "bizAccountMember",
                "title": "成员管理",
                "code": "biz/account/page",
                "icon": "user-add-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/account/member",
                "component": "biz/configue/newAccount/member/index",
                "subs": [
                    {
                        "title": "新增成员",
                        "code": "biz/account/add",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "编辑成员",
                        "code": "biz/account/edit",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "成员详情",
                        "code": "biz/account/detail",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/account/state",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "成员删除",
                        "code": "biz/account/delete",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "成员列表",
                        "code": "biz/account/list",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "bizAccountAuthorization",
                "title": "权限管理",
                "code": "biz/authorization/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/account/authorization",
                "component": "biz/configue/newAccount/member/authorization",
                "subs": [
                    {
                        "title": "分配权限",
                        "code": "biz/authorization/auth",
                        "category": "BUTTON",
                        "module": 10007
                    },
                    {
                        "title": "角色列表",
                        "code": "biz/authorization/getRoleList",
                        "category": "BUTTON",
                        "module": 10007
                    }
                ]
            },
            {
                "name": "configDevelop",
                "title": "开发者管理",
                "code": "biz/develop/page",
                "icon": "console-sql-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/account/api",
                "component": "biz/configue/newAccount/develop/index"
            }
        ]
    },
    {
        "group_name": "其他",
        "name": "7112446810089590784",
        "title": "上链纪录",
        "code": "",
        "icon": "api-outlined",
        "category": "MENU",
        "module": 10007,
        "menu_type": "CATALOG",
        "path": "/7112446810089590784",
        "component": "",
        "subs": [
            {
                "name": "bizRetailBlockChain",
                "title": "上链纪录",
                "code": "biz/BlockChain/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10007,
                "menu_type": "MENU",
                "path": "/biz/retail/BlockChain",
                "component": "biz/BlockChain/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7109384704742133760",
        "title": "商户管理",
        "code": null,
        "icon": "shop-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109384704742133760",
        "component": null,
        "subs": [
            {
                "name": "bizSeedMerchant",
                "title": "商户管理",
                "code": "biz/merchant/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/merchant",
                "component": "biz/merchants/merchant/index",
                "subs": [
                    {
                        "title": "新增商户",
                        "code": "biz/merchant/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑商户",
                        "code": "biz/merchant/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取一个商户详情",
                        "code": "biz/merchant/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除商户",
                        "code": "biz/merchant/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取商户列表",
                        "code": "biz/merchant/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改商户状态",
                        "code": "biz/merchant/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "授权商户子系统",
                        "code": "biz/merchant/authorize",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "商户审核",
                        "code": "biz/merchant/toExamine",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "信息录入规范设置",
                        "code": "biz/merchant/infoType",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizSeedSupplier",
                "title": "供应商管理",
                "code": "biz/supplier/page",
                "icon": "user-switch-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/supplier",
                "component": "biz/merchants/supplier/index",
                "subs": [
                    {
                        "title": "获取供应商详情",
                        "code": "biz/supplier/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增供应商",
                        "code": "biz/supplier/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑供应商",
                        "code": "biz/supplier/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除供应商",
                        "code": "biz/supplier/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取供应商列表",
                        "code": "biz/supplier/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改供应商状态",
                        "code": "biz/supplier/state",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizSeedDistributor",
                "title": "经销商管理",
                "code": "biz/distributor/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/distributor",
                "component": "biz/merchants/distributor/index",
                "subs": [
                    {
                        "title": "获取经销商详情",
                        "code": "biz/distributor/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增经销商",
                        "code": "biz/distributor/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑经销商",
                        "code": "biz/distributor/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除经销商",
                        "code": "biz/distributor/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "批量导入经销商",
                        "code": "biz/distributor/importData",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取经销商列表",
                        "code": "biz/distributor/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改经销商状态",
                        "code": "biz/distributor/state",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizSeedEmployees",
                "title": "员工管理",
                "code": "biz/employees/page",
                "icon": "usergroup-add-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/employees",
                "component": "biz/merchants/employees/index",
                "subs": [
                    {
                        "title": "获取员工列表",
                        "code": "biz/employees/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增员工",
                        "code": "biz/employees/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑员工",
                        "code": "biz/employees/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除员工",
                        "code": "biz/employees/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取员工详情",
                        "code": "biz/employees/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改员工关态",
                        "code": "biz/employees/state",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7109385860570681344",
        "title": "商品管理",
        "code": null,
        "icon": "picture-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109385860570681344",
        "component": null,
        "subs": [
            {
                "name": "goodsCateSeed",
                "title": "商品分类",
                "code": "biz/cate/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/cate",
                "component": "biz/goods/cate/index",
                "subs": [
                    {
                        "title": "获取菜单列表",
                        "code": "biz/cate/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取菜单树",
                        "code": "biz/cate/tree",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增菜单",
                        "code": "biz/cate/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑菜单",
                        "code": "biz/cate/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取菜单详情",
                        "code": "biz/cate/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除菜单",
                        "code": "biz/cate/delete",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizSeedGoods",
                "title": "商品管理",
                "code": "biz/goods/page",
                "icon": "gift-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/goods",
                "component": "biz/seed/goods/commodity/index",
                "subs": [
                    {
                        "title": "获取商品详情",
                        "code": "biz/goods/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增商品",
                        "code": "biz/goods/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑商品",
                        "code": "biz/goods/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除商品",
                        "code": "biz/goods/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取商品列表",
                        "code": "biz/goods/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改商品状态",
                        "code": "biz/goods/state",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizGoodsInfo",
                "title": "商品信息配置",
                "code": "biz/batch.goodsInfo/page",
                "icon": "diff-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/batch/goodsInfoConfig",
                "component": "sub/public/trace/goodsInfoConfig",
                "subs": [
                    {
                        "title": "新增环节",
                        "code": "/biz/hyper/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑环节",
                        "code": "/biz/hyper/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除环节",
                        "code": "/biz/hyper/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "/biz/hyper/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "环节列表",
                        "code": "/biz/hyper/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "环节详情",
                        "code": "/biz/hyper/detail",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizSeedGoodsBrand",
                "title": "品牌管理",
                "code": "biz/brand/page",
                "icon": "trademark-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/brand",
                "component": "biz/goods/brand/index",
                "subs": [
                    {
                        "title": "获取商品品牌详情",
                        "code": "biz/brand/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "商品品牌新增",
                        "code": "biz/brand/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "商品品牌编辑",
                        "code": "biz/brand/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "商品品牌删除",
                        "code": "biz/brand/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改品牌状态",
                        "code": "biz/brand/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取品牌列表",
                        "code": "biz/brand/list",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizSeedRawMaterial",
                "title": "投入品管理",
                "code": "biz/RawMaterial/page",
                "icon": "block-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/inputs",
                "component": "biz/goods/inputs/index",
                "subs": [
                    {
                        "title": "获取投入品列表",
                        "code": "biz/RawMaterial/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取投入品详情",
                        "code": "biz/RawMaterial/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增投入品",
                        "code": "biz/RawMaterial/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑投入品",
                        "code": "biz/RawMaterial/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除投入品",
                        "code": "biz/RawMaterial/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/RawMaterial/state",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizSeedGoodsCerification",
                "title": "商品认证",
                "code": "biz/cerification/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/cerification",
                "component": "biz/goods/cerification/index",
                "subs": [
                    {
                        "title": "商品认证列表",
                        "code": "biz/cerification/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取商品认证详情",
                        "code": "biz/cerification/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增商品认证",
                        "code": "biz/cerification/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑商品认证",
                        "code": "biz/cerification/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除商品认证",
                        "code": "biz/cerification/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/cerification/state",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizSeedShop",
                "title": "店铺管理",
                "code": "biz/shop/page",
                "icon": "shop-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/shop",
                "component": "biz/goods/shop/index",
                "subs": [
                    {
                        "title": "获取店铺列表",
                        "code": "biz/shop/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增店铺",
                        "code": "biz/shop/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑店铺",
                        "code": "biz/shop/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除店铺",
                        "code": "biz/shop/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取店铺详情",
                        "code": "biz/shop/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/shop/state",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7109387373753929728",
        "title": "原料采购",
        "code": null,
        "icon": "login-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109387373753929728",
        "component": null,
        "subs": [
            {
                "name": "bizSeedProductMateral",
                "title": "投入品采购",
                "code": "biz/product.material/page",
                "icon": "plus-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/product/material",
                "component": "sub/seed/product/material/index",
                "subs": [
                    {
                        "title": "获取投入品采购列表",
                        "code": "biz/product.material/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取投入品采购详情",
                        "code": "biz/product.material/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增投入品采购",
                        "code": "biz/product.material/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑投入品采购",
                        "code": "biz/product.material/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除投入品采购",
                        "code": "biz/product.material/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "上链",
                        "code": "biz/product.material/upChain",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改投入品采购状态",
                        "code": "biz/product.material/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/product.material/freeze",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizSeedProductMaterialBatch",
                "title": "投入品批次",
                "code": "biz/product.MaterialItem/page",
                "icon": "pic-right-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/product/material/batch",
                "component": "sub/seed/product/material/batch",
                "subs": [
                    {
                        "title": "获取投入品批次列表",
                        "code": "biz/product.MaterialItem/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取投入品批次详情（预览）",
                        "code": "biz/product.MaterialItem/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改投入品批次状态（冻结）",
                        "code": "biz/product.MaterialItem/freeze",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7109388047405289472",
        "title": "农事管理",
        "code": null,
        "icon": "file-zip-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109388047405289472",
        "component": null,
        "subs": [
            {
                "name": "bizPlot",
                "title": "地块管理",
                "code": "biz/planting.plot/page",
                "icon": "radius-bottomright-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/agriculturalManagement/plot",
                "component": "sub/seed/agriculturalManagement/plot/index",
                "subs": [
                    {
                        "title": "新增地块",
                        "code": "biz/planting.plot/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑地块",
                        "code": "biz/planting.plot/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "地块详情",
                        "code": "biz/planting.plot/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/planting.plot/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除地块",
                        "code": "biz/planting.plot/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "地块列表",
                        "code": "biz/planting.plot/list",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizPlan",
                "title": "种植计划",
                "code": "biz/planting.plan/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/agriculturalManagement/plantPlan",
                "component": "sub/seed/agriculturalManagement/plan/index",
                "subs": [
                    {
                        "title": "新增计划",
                        "code": "biz/planting.plan/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑计划",
                        "code": "biz/planting.plan/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "计划详情",
                        "code": "biz/planting.plan/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改计划状态",
                        "code": "biz/planting.plan/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除计划",
                        "code": "biz/planting.plan/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "计划列表",
                        "code": "biz/planting.plan/list",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizWork",
                "title": "种植作业",
                "code": "biz/planting.work/page",
                "icon": "account-book-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/agriculturalManagement/farmWork",
                "component": "sub/seed/agriculturalManagement/work/index",
                "subs": [
                    {
                        "title": "新增作业",
                        "code": "biz/planting.work/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑作业",
                        "code": "biz/planting.work/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改作业",
                        "code": "biz/planting.work/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "作业详情",
                        "code": "biz/planting.work/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除作业",
                        "code": "biz/planting.work/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "作业列表",
                        "code": "biz/planting.work/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "上链",
                        "code": "biz/planting.work/upChain",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/planting.work/freeze",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizDevice",
                "title": "设备管理",
                "code": "biz/device/page",
                "icon": "alert-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "biz/seed/device",
                "component": "biz/device/index",
                "subs": [
                    {
                        "title": "设备列表",
                        "code": "biz/device/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增设备",
                        "code": "biz/device/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "设备改名",
                        "code": "biz/device/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "设备删除",
                        "code": "biz/device/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "设备播放",
                        "code": "biz/device/play",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "设备抓拍",
                        "code": "biz/device/capture",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizDevicePlay",
                "title": "设备播放",
                "code": "biz/device/play",
                "icon": "play-circle-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/seed/device/play",
                "component": "biz/device/play"
            },
            {
                "name": "bizFarmingInfo",
                "title": "农事活动配置",
                "code": "biz/batch.farming/page",
                "icon": "contacts-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/batch/farmingInfo",
                "component": "sub/public/trace/farmingInfo"
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7109389567853072384",
        "title": "生产管理",
        "code": null,
        "icon": "scissor-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109389567853072384",
        "component": null,
        "subs": [
            {
                "name": "bizProductSeedProduceIn",
                "title": "采收入库",
                "code": "biz/trace.ProduceIn/page",
                "icon": "build-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/product/produce/in",
                "component": "sub/seed/product/produceIn/index",
                "subs": [
                    {
                        "title": "获取采收入库信息列表",
                        "code": "biz/trace.ProduceIn/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取采收入库详情",
                        "code": "biz/trace.ProduceIn/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增采收入库",
                        "code": "biz/trace.ProduceIn/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑采收入库",
                        "code": "biz/trace.ProduceIn/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除采收入库",
                        "code": "biz/trace.ProduceIn/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改采收入库状态（冻结，解冻）",
                        "code": "biz/trace.ProduceIn/freeze",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.ProduceIn/upChain",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizProductSeedProduceInBatch",
                "title": "采收批次",
                "code": "biz/trace.ProduceInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/product/produce/in/batch",
                "component": "sub/seed/product/produceIn/batch",
                "subs": [
                    {
                        "title": "获取采收批次列表",
                        "code": "biz/trace.ProduceInItem/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改采收入库批次状态（冻结，解冻）",
                        "code": "biz/trace.ProduceInItem/freeze",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取采收入库批次详情",
                        "code": "biz/trace.ProduceInItem/detail",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7109391145343389696",
        "title": "检验检疫",
        "code": null,
        "icon": "bug-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109391145343389696",
        "component": null,
        "subs": [
            {
                "name": "bizSeedQuarantineReport",
                "title": "检验检疫",
                "code": "biz/QuarantineReport/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/QuarantineReport",
                "component": "biz/quarantineReport/index",
                "subs": [
                    {
                        "title": "获取检验检疫列表",
                        "code": "biz/QuarantineReport/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增检验检疫",
                        "code": "biz/QuarantineReport/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑检验检疫",
                        "code": "biz/QuarantineReport/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除检验检疫",
                        "code": "biz/QuarantineReport/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取检验检疫详情",
                        "code": "biz/QuarantineReport/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "激活检验检疫",
                        "code": "biz/QuarantineReport/freeze",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "上链",
                        "code": "biz/QuarantineReport/upChain",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7109391476009734144",
        "title": "出库管理",
        "code": null,
        "icon": "minus-square-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109391476009734144",
        "component": null,
        "subs": [
            {
                "name": "bizSeedProductStockOut",
                "title": "出库管理",
                "code": "biz/trace.StockOut/page",
                "icon": "rollback-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/seed/product/stock/out",
                "component": "sub/seed/product/stockOut/index",
                "subs": [
                    {
                        "title": "获取商品出库信息列表",
                        "code": "biz/trace.StockOut/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取商品出库详情",
                        "code": "biz/trace.StockOut/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "添加商品出库",
                        "code": "biz/trace.StockOut/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑出库信息",
                        "code": "biz/trace.StockOut/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除出库信息",
                        "code": "biz/trace.StockOut/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改出库状态",
                        "code": "biz/trace.StockOut/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockOut/upChain",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/trace.StockOut/freeze",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7109391867422183424",
        "title": "渠道管理",
        "code": null,
        "icon": "swap-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109391867422183424",
        "component": null,
        "subs": [
            {
                "name": "bizSuspectedSmuggling",
                "title": "疑似窜货",
                "code": "biz/FleeingGoods/page",
                "icon": "strikethrough-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/distribution/channel/suspectedSmuggling",
                "component": "biz/distribution/channel/suspectedSmuggling/index",
                "subs": [
                    {
                        "title": "疑似窜货列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "窜货详情",
                        "code": "biz/FleeingGoods/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除窜货",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizStatistics",
                "title": "窜货统计",
                "code": "biz/FleeingGoods/page",
                "icon": "project-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/distribution/channel/statistics",
                "component": "biz/distribution/channel/statistics/index",
                "subs": [
                    {
                        "title": "窜货统计列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除窜货统计",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7109392348349468672",
        "title": "客群洞察",
        "code": null,
        "icon": "branches-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109392348349468672",
        "component": null,
        "subs": [
            {
                "name": "bizDataDashboard",
                "title": "客群洞察",
                "code": "biz/data.dashboard/page",
                "icon": "deployment-unit-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/distribution/data/dashboard",
                "component": "biz/distribution/data/dashboard/index"
            },
            {
                "name": "bizDataScanQr",
                "title": "扫码统计",
                "code": "biz/ScanLogs/page",
                "icon": "scan-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/distribution/data/scanQr",
                "component": "biz/distribution/data/scanQr/index",
                "subs": [
                    {
                        "title": "扫码统计列表",
                        "code": "biz/ScanLogs/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/ScanLogs/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除扫码统计",
                        "code": "biz/ScanLogs/delete",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizDataSellTrend",
                "title": "销售趋势",
                "code": "biz/data.sellTrend/page",
                "icon": "monitor-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/distribution/data/sellTrend",
                "component": "biz/distribution/data/sellTrend/index"
            }
        ]
    },
    {
        "group_name": "码",
        "name": "7109392986533793792",
        "title": "标识管理",
        "code": null,
        "icon": "qrcode-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109392986533793792",
        "component": null,
        "subs": [
            {
                "name": "bizCodeRule",
                "title": "生码管理",
                "code": "biz/codeRule/page",
                "icon": "holder-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/code/mark/rule",
                "component": "biz/code/mark/rule/index",
                "subs": [
                    {
                        "title": "码规则列表",
                        "code": "biz/CodeRule/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "新增码规则",
                        "code": "biz/CodeRule/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑码规则",
                        "code": "biz/CodeRule/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取详情",
                        "code": "biz/CodeRule/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除码规则",
                        "code": "biz/CodeRule/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/CodeRule/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "码规则审核",
                        "code": "biz/CodeRule/toExamine",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizCreateCode",
                "title": "生码纪录",
                "code": "biz/issue/page",
                "icon": "clock-circle-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/createCode",
                "component": "biz/code/mark/create/index",
                "subs": [
                    {
                        "title": "获取生码列表",
                        "code": "biz/issue/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "导入码包",
                        "code": "biz/issue/importCodeRelation",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "扫码关联批次",
                        "code": "biz/issue/scanCodeRelation",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/issue/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "生码详情",
                        "code": "biz/issue/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "生码",
                        "code": "biz/issue/crtQr",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "码下载",
                        "code": "biz/issue/down",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizOverdue",
                "title": "报损纪录",
                "code": "biz/overdue/page",
                "icon": "menu-fold-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/code/mark/overdue",
                "component": "biz/code/mark/overdue/index",
                "subs": [
                    {
                        "title": "导入报损",
                        "code": "biz/overdue/importBadCode",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "扫码报损",
                        "code": "biz/overdue/scanCode",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取报损详情",
                        "code": "biz/overdue/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除报损",
                        "code": "biz/overdue/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "报损列表",
                        "code": "biz/overdue/list",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizCodeSearchIdentification",
                "title": "查询标识",
                "code": "biz/code.searchIdentification/page",
                "icon": "zoom-in-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/code/mark/searchIdentification",
                "component": "biz/code/mark/searchIdentification/index"
            },
            {
                "name": "bizCodePackage",
                "title": "包装层级",
                "code": "biz/package/page",
                "icon": "pic-left-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/code/mark/package",
                "component": "biz/code/mark/package/index",
                "subs": [
                    {
                        "title": "获取包装层级列表",
                        "code": "biz/package/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "获取包装层级详情",
                        "code": "biz/package/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "添加包装层级",
                        "code": "biz/package/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更新包装层级",
                        "code": "biz/package/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "修改包装层级状态",
                        "code": "biz/package/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "删除包装层级",
                        "code": "biz/package/delete",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7109393962992930816",
        "title": "配置管理",
        "code": null,
        "icon": "form-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109393962992930816",
        "component": null,
        "subs": [
            {
                "name": "bizCheck",
                "title": "检测报告模板",
                "code": "biz/template.check/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/checkTemplate",
                "component": "biz/configue/config/check/index",
                "subs": [
                    {
                        "title": "新增检测报告模板",
                        "code": "biz/template.check/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑检测报告模板",
                        "code": "biz/template.check/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "检测报告模板列表",
                        "code": "biz/template.check/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "检测报告模板删除",
                        "code": "biz/template.check/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改检测报告模板状态",
                        "code": "biz/template.check/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "检测报告模板详情",
                        "code": "biz/template.check/detail",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizInfoTemplate",
                "title": "溯源模板管理",
                "code": "biz/template.info/page",
                "icon": "retweet-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/infoTemplate",
                "component": "biz/configue/config/info/index",
                "subs": [
                    {
                        "title": "新增溯源模板管理",
                        "code": "biz/template.info/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑溯源模板管理",
                        "code": "biz/template.info/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "溯源模板删除",
                        "code": "biz/template.info/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "溯源模板列表",
                        "code": "biz/template.info/list",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改溯源模板状态",
                        "code": "biz/template.info/state",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizApplet",
                "title": "小程序模板管理",
                "code": "biz/appletTemplate/page",
                "icon": "fullscreen-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/config/applet",
                "component": "biz/configue/config/applet/index"
            },
            {
                "name": "bizConfigTemplateInfo",
                "title": "溯源模板配置",
                "code": "biz/infoTemplate/list",
                "icon": "border-outer-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/configue/InfoConfig",
                "component": "biz/configue/config/info/info"
            },
            {
                "name": "bizAppletForm",
                "title": "新增编辑小程序模板",
                "code": "biz/applet/save",
                "icon": "code-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/config/applet/form",
                "component": "biz/configue/config/applet/form"
            },
            {
                "name": "bizCertificate",
                "title": "溯源链证书配置",
                "code": "biz/template.blockchain/detail",
                "icon": "book-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/configue/certificate",
                "component": "biz/configue/config/certificate/index",
                "subs": [
                    {
                        "title": "添加区块链模板",
                        "code": "biz/template.blockchain/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更新区块链模板",
                        "code": "biz/template.blockchain/edit",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7109395416277323776",
        "title": "账号管理",
        "code": null,
        "icon": "usergroup-add-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7109395416277323776",
        "component": null,
        "subs": [
            {
                "name": "bizAccountMember",
                "title": "成员管理",
                "code": "biz/account/page",
                "icon": "user-add-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/account/member",
                "component": "biz/configue/newAccount/member/index",
                "subs": [
                    {
                        "title": "新增成员",
                        "code": "biz/account/add",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "编辑成员",
                        "code": "biz/account/edit",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "成员详情",
                        "code": "biz/account/detail",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/account/state",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "成员删除",
                        "code": "biz/account/delete",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "成员列表",
                        "code": "biz/account/list",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "bizAccountAuthorization",
                "title": "权限管理",
                "code": "biz/authorization/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/account/authorization",
                "component": "biz/configue/newAccount/member/authorization",
                "subs": [
                    {
                        "title": "分配权限",
                        "code": "biz/authorization/auth",
                        "category": "BUTTON",
                        "module": 10008
                    },
                    {
                        "title": "角色列表",
                        "code": "biz/authorization/getRoleList",
                        "category": "BUTTON",
                        "module": 10008
                    }
                ]
            },
            {
                "name": "configDevelop",
                "title": "开发者管理",
                "code": "biz/develop/page",
                "icon": "console-sql-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/account/api",
                "component": "biz/configue/newAccount/develop/index"
            }
        ]
    },
    {
        "group_name": "其他",
        "name": "7112447595728867328",
        "title": "上链日志",
        "code": null,
        "icon": "api-outlined",
        "category": "MENU",
        "module": 10008,
        "menu_type": "CATALOG",
        "path": "/7112447595728867328",
        "component": null,
        "subs": [
            {
                "name": "bizSeedBlockChain",
                "title": "上链纪录",
                "code": "biz/BlockChain/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10008,
                "menu_type": "MENU",
                "path": "/biz/seed/BlockChain",
                "component": "biz/blockChain/index"
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7109438040879140864",
        "title": "商户管理",
        "code": null,
        "icon": "credit-card-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109438040879140864",
        "component": null,
        "subs": [
            {
                "name": "bizPesticideMerchant",
                "title": "商户管理",
                "code": "biz/merchant/page",
                "icon": "appstore-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/merchant",
                "component": "biz/merchants/merchant/index",
                "subs": [
                    {
                        "title": "新增商户",
                        "code": "biz/merchant/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑商户",
                        "code": "biz/merchant/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取一个商户详情",
                        "code": "biz/merchant/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除商户",
                        "code": "biz/merchant/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取商户列表",
                        "code": "biz/merchant/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改商户状态",
                        "code": "biz/merchant/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "授权商户子系统",
                        "code": "biz/merchant/authorize",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "商户审核",
                        "code": "biz/merchant/toExamine",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "信息录入规范设置",
                        "code": "biz/merchant/infoType",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideSupplier",
                "title": "供应商管理",
                "code": "biz/supplier/page",
                "icon": "user-switch-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/supplier",
                "component": "biz/merchants/supplier/index",
                "subs": [
                    {
                        "title": "获取供应商详情",
                        "code": "biz/supplier/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增供应商",
                        "code": "biz/supplier/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑供应商",
                        "code": "biz/supplier/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除供应商",
                        "code": "biz/supplier/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取供应商列表",
                        "code": "biz/supplier/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改供应商状态",
                        "code": "biz/supplier/state",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideDistributor",
                "title": "经销商管理",
                "code": "biz/distributor/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/distributor",
                "component": "biz/merchants/distributor/index",
                "subs": [
                    {
                        "title": "获取经销商详情",
                        "code": "biz/distributor/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增经销商",
                        "code": "biz/distributor/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑经销商",
                        "code": "biz/distributor/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除经销商",
                        "code": "biz/distributor/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "批量导入经销商",
                        "code": "biz/distributor/importData",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取经销商列表",
                        "code": "biz/distributor/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改经销商状态",
                        "code": "biz/distributor/state",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "基础",
        "name": "7109438164107792384",
        "title": "商品管理",
        "code": null,
        "icon": "picture-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109438164107792384",
        "component": null,
        "subs": [
            {
                "name": "goodsCatePesticide",
                "title": "商品分类",
                "code": "biz/cate/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/cate",
                "component": "biz/goods/cate/index",
                "subs": [
                    {
                        "title": "获取菜单列表",
                        "code": "biz/cate/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取菜单树",
                        "code": "biz/cate/tree",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取菜单详情",
                        "code": "biz/cate/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增菜单",
                        "code": "biz/cate/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑菜单",
                        "code": "biz/cate/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除菜单",
                        "code": "biz/cate/delete",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideGoods",
                "title": "商品管理",
                "code": "biz/goods/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/goods",
                "component": "biz/pesticide/goods/commodity/index",
                "subs": [
                    {
                        "title": "获取商品详情",
                        "code": "biz/goods/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增商品",
                        "code": "biz/goods/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑商品",
                        "code": "biz/goods/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除商品",
                        "code": "biz/goods/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取商品列表",
                        "code": "biz/goods/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改商品状态",
                        "code": "biz/goods/state",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizGoodsInfo",
                "title": "商品信息配置",
                "code": "biz/batch.goodsInfo/page",
                "icon": "diff-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/batch/goodsInfoConfig",
                "component": "sub/public/trace/goodsInfoConfig",
                "subs": [
                    {
                        "title": "新增环节",
                        "code": "/biz/hyper/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑环节",
                        "code": "/biz/hyper/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除环节",
                        "code": "/biz/hyper/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "/biz/hyper/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "环节列表",
                        "code": "/biz/hyper/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "环节详情",
                        "code": "/biz/hyper/detail",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideBrand",
                "title": "品牌管理",
                "code": "biz/brand/page",
                "icon": "trademark-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/brand",
                "component": "biz/goods/brand/index",
                "subs": [
                    {
                        "title": "获取商品品牌详情",
                        "code": "biz/brand/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "商品品牌新增",
                        "code": "biz/brand/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "商品品牌编辑",
                        "code": "biz/brand/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "商品品牌删除",
                        "code": "biz/brand/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改品牌状态",
                        "code": "biz/brand/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取品牌列表",
                        "code": "biz/brand/list",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideMaterial",
                "title": "原料管理",
                "code": "biz/RawMaterial/page",
                "icon": "block-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/material",
                "component": "biz/goods/material/index",
                "subs": [
                    {
                        "title": "获取原料列表",
                        "code": "biz/RawMaterial/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取原料详情",
                        "code": "biz/RawMaterial/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增原料",
                        "code": "biz/RawMaterial/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑原料",
                        "code": "biz/RawMaterial/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除原料",
                        "code": "biz/RawMaterial/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/RawMaterial/state",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideCerification",
                "title": "商品认证",
                "code": "biz/cerification/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/cerification",
                "component": "biz/goods/cerification/index",
                "subs": [
                    {
                        "title": "商品认证列表",
                        "code": "biz/cerification/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取商品认证详情",
                        "code": "biz/cerification/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增商品认证",
                        "code": "biz/cerification/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑商品认证",
                        "code": "biz/cerification/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除商品认证",
                        "code": "biz/cerification/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/cerification/state",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideBom",
                "title": "物料清单",
                "code": "biz/bom/page",
                "icon": "ordered-list-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/bom",
                "component": "biz/goods/bom/index",
                "subs": [
                    {
                        "title": "获取物料清单列表",
                        "code": "biz/bom/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取物料清单详情",
                        "code": "biz/bom/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增物料清单",
                        "code": "biz/bom/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑物料清单",
                        "code": "biz/bom/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除物料清单",
                        "code": "biz/bom/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/bom/state",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideShop",
                "title": "店铺管理",
                "code": "biz/shop/page",
                "icon": "shop-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/shop",
                "component": "biz/goods/shop/index",
                "subs": [
                    {
                        "title": "获取店铺列表",
                        "code": "biz/shop/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增店铺",
                        "code": "biz/shop/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑店铺",
                        "code": "biz/shop/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除店铺",
                        "code": "biz/shop/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取店铺详情",
                        "code": "biz/shop/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/shop/state",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7109438256114044928",
        "title": "原料采购",
        "code": "",
        "icon": "login-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109438256114044928",
        "component": "",
        "subs": [
            {
                "name": "bizPesticideMaterial",
                "title": "原料采购",
                "code": "biz/product.material/page",
                "icon": "plus-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/product/material",
                "component": "sub/pesticide/product/material/index",
                "subs": [
                    {
                        "title": "获取原料采购列表",
                        "code": "biz/product.material/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取原料采购详情",
                        "code": "biz/product.material/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增原料采购",
                        "code": "biz/product.material/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑原料采购",
                        "code": "biz/product.material/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除原料采购",
                        "code": "biz/product.material/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "上链",
                        "code": "biz/product.material/upChain",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改原料采购状态",
                        "code": "biz/product.material/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/product.material/freeze",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideBatch",
                "title": "原料批次",
                "code": "biz/product.MaterialItem/page",
                "icon": "pic-right-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/product/material/batch",
                "component": "sub/pesticide/product/material/batch",
                "subs": [
                    {
                        "title": "获取原料批次列表",
                        "code": "biz/product.MaterialItem/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取原料批次详情（预览）",
                        "code": "biz/product.MaterialItem/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改原料批次状态（冻结）",
                        "code": "biz/product.MaterialItem/freeze",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7109438378570944512",
        "title": "生产管理",
        "code": "",
        "icon": "scissor-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109438378570944512",
        "component": "",
        "subs": [
            {
                "name": "bizPesticideProduceIn",
                "title": "生产入库",
                "code": "biz/trace.ProduceIn/page",
                "icon": "build-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/product/produce/in",
                "component": "sub/pesticide/product/produceIn/index",
                "subs": [
                    {
                        "title": "获取生产入库信息列表",
                        "code": "biz/trace.ProduceIn/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取生产入库详情",
                        "code": "biz/trace.ProduceIn/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增生产入库",
                        "code": "biz/trace.ProduceIn/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑生产入库",
                        "code": "biz/trace.ProduceIn/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除生产入库",
                        "code": "biz/trace.ProduceIn/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改生产入库状态（冻结，解冻）",
                        "code": "biz/trace.ProduceIn/freeze",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.ProduceIn/upChain",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideProduceIn",
                "title": "生产批次",
                "code": "biz/trace.ProduceInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/product/produce/in/batch",
                "component": "sub/pesticide/product/produceIn/batch",
                "subs": [
                    {
                        "title": "获取生产批次列表",
                        "code": "biz/trace.ProduceInItem/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改生产入库批次状态（冻结，解冻）",
                        "code": "biz/trace.ProduceInItem/freeze",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取生产入库批次详情",
                        "code": "biz/trace.ProduceInItem/detail",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7109440584586104832",
        "title": "入库管理",
        "code": "",
        "icon": "plus-square-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109440584586104832",
        "component": "",
        "subs": [
            {
                "name": "bizPesticideStockIn",
                "title": "入库管理",
                "code": "biz/trace.StockIn/page",
                "icon": "arrow-right-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/product/stock/in",
                "component": "sub/pesticide/product/stockIn/index",
                "subs": [
                    {
                        "title": "获取商品入库信息列表",
                        "code": "biz/trace.StockIn/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取商品入库详情",
                        "code": "biz/trace.StockIn/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "添加商品入库",
                        "code": "biz/trace.StockIn/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑商品入库",
                        "code": "biz/trace.StockIn/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除入库信息",
                        "code": "biz/trace.StockIn/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockIn/upChain",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改商品入库状态（冻结，解冻）",
                        "code": "biz/trace.StockIn/freeze",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizPesticideStockInBatch",
                "title": "入库批次",
                "code": "biz/trace.StockInItem/page",
                "icon": "menu-unfold-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/product/stock/in/batch",
                "component": "sub/pesticide/product/stockIn/batch",
                "subs": [
                    {
                        "title": "获取入库批次列表",
                        "code": "biz/trace.StockInItem/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改商品入库批次状态（冻结，解冻）",
                        "code": "biz/trace.StockInItem/freeze",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取入库批次详情",
                        "code": "biz/trace.StockInItem/detail",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "生产",
        "name": "7109440726592655360",
        "title": "检验检疫",
        "code": "",
        "icon": "bug-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109440726592655360",
        "component": "",
        "subs": [
            {
                "name": "bizPesticideQuarantineReport",
                "title": "检验检疫",
                "code": "biz/QuarantineReport/page",
                "icon": "safety-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/QuarantineReport",
                "component": "biz/quarantineReport/index",
                "subs": [
                    {
                        "title": "获取检验检疫列表",
                        "code": "biz/QuarantineReport/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增检验检疫",
                        "code": "biz/QuarantineReport/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑检验检疫",
                        "code": "biz/QuarantineReport/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除检验检疫",
                        "code": "biz/QuarantineReport/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取检验检疫详情",
                        "code": "biz/QuarantineReport/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "激活检验检疫",
                        "code": "biz/QuarantineReport/freeze",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "上链",
                        "code": "biz/QuarantineReport/upChain",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7109440848470740992",
        "title": "出库管理",
        "code": "",
        "icon": "minus-square-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109440848470740992",
        "component": "",
        "subs": [
            {
                "name": "bizPesticideStockOut",
                "title": "出库管理",
                "code": "biz/trace.StockOut/page",
                "icon": "rollback-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/pesticide/product/stock/out",
                "component": "sub/pesticide/product/stockOut/index",
                "subs": [
                    {
                        "title": "获取商品出库信息列表",
                        "code": "biz/trace.StockOut/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取商品出库详情",
                        "code": "biz/trace.StockOut/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "添加商品出库",
                        "code": "biz/trace.StockOut/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑出库信息",
                        "code": "biz/trace.StockOut/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除出库信息",
                        "code": "biz/trace.StockOut/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改出库状态",
                        "code": "biz/trace.StockOut/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "激活/冻结",
                        "code": "biz/trace.StockOut/freeze",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "上链",
                        "code": "biz/trace.StockOut/upChain",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7109440967270207488",
        "title": "渠道管理",
        "code": "",
        "icon": "swap-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109440967270207488",
        "component": "",
        "subs": [
            {
                "name": "bizSuspectedSmuggling",
                "title": "疑似窜货",
                "code": "biz/FleeingGoods/page",
                "icon": "strikethrough-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/distribution/channel/suspectedSmuggling",
                "component": "biz/distribution/channel/suspectedSmuggling/index",
                "subs": [
                    {
                        "title": "疑似窜货列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "窜货详情",
                        "code": "biz/FleeingGoods/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除窜货",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizStatistics",
                "title": "窜货统计",
                "code": "biz/channel.statistics/page",
                "icon": "project-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/distribution/channel/statistics",
                "component": "biz/distribution/channel/statistics/index",
                "subs": [
                    {
                        "title": "窜货统计列表",
                        "code": "biz/FleeingGoods/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/FleeingGoods/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除窜货统计",
                        "code": "biz/FleeingGoods/delete",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "经销",
        "name": "7109441061310697472",
        "title": "客群洞察",
        "code": "",
        "icon": "branches-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109441061310697472",
        "component": "",
        "subs": [
            {
                "name": "bizDataDashboard",
                "title": "客群洞察",
                "code": "biz/data.dashboard/page",
                "icon": "deployment-unit-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/distribution/data/dashboard",
                "component": "biz/distribution/data/dashboard/index"
            },
            {
                "name": "bizDataScanQr",
                "title": "扫码统计",
                "code": "biz/data.scanQr/page",
                "icon": "scan-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/distribution/data/scanQr",
                "component": "biz/distribution/data/scanQr/index"
            },
            {
                "name": "bizDataSellTrend",
                "title": "销售趋势",
                "code": "biz/data.sellTrend/page",
                "icon": "monitor-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/distribution/data/sellTrend",
                "component": "biz/distribution/data/sellTrend/index"
            }
        ]
    },
    {
        "group_name": "码",
        "name": "7109441153442779136",
        "title": "标识管理",
        "code": "",
        "icon": "qrcode-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109441153442779136",
        "component": "",
        "subs": [
            {
                "name": "bizCodeRule",
                "title": "生码管理",
                "code": "biz/codeRule/page",
                "icon": "holder-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/code/mark/rule",
                "component": "biz/code/mark/rule/index",
                "subs": [
                    {
                        "title": "码规则列表",
                        "code": "biz/CodeRule/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "新增码规则",
                        "code": "biz/CodeRule/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑码规则",
                        "code": "biz/CodeRule/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取详情",
                        "code": "biz/CodeRule/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除码规则",
                        "code": "biz/CodeRule/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/CodeRule/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "码规则审核",
                        "code": "biz/CodeRule/toExamine",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizCreateCode",
                "title": "生码纪录",
                "code": "biz/issue/page",
                "icon": "clock-circle-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/createCode",
                "component": "biz/code/mark/create/index",
                "subs": [
                    {
                        "title": "获取生码列表",
                        "code": "biz/issue/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "导入码包",
                        "code": "biz/issue/importCodeRelation",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "扫码关联批次",
                        "code": "biz/issue/scanCodeRelation",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/issue/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "生码详情",
                        "code": "biz/issue/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "生码",
                        "code": "biz/issue/crtQr",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "码下载",
                        "code": "biz/issue/down",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizOverdue",
                "title": "报损纪录",
                "code": "biz/overdue/page",
                "icon": "menu-fold-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/code/mark/overdue",
                "component": "biz/code/mark/overdue/index",
                "subs": [
                    {
                        "title": "导入报损",
                        "code": "biz/overdue/importBadCode",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "扫码报损",
                        "code": "biz/overdue/scanCode",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取报损详情",
                        "code": "biz/overdue/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除报损",
                        "code": "biz/overdue/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "报损列表",
                        "code": "biz/overdue/list",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizCodeSearchIdentification",
                "title": "查询标识",
                "code": "biz/code.searchIdentification/page",
                "icon": "zoom-in-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/code/mark/searchIdentification",
                "component": "biz/code/mark/searchIdentification/index"
            },
            {
                "name": "bizCodePackage",
                "title": "包装层级",
                "code": "biz/package/page",
                "icon": "pic-left-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/code/mark/package",
                "component": "biz/code/mark/package/index",
                "subs": [
                    {
                        "title": "获取包装层级列表",
                        "code": "biz/package/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "获取包装层级详情",
                        "code": "biz/package/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "添加包装层级",
                        "code": "biz/package/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更新包装层级",
                        "code": "biz/package/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "修改包装层级状态",
                        "code": "biz/package/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "删除包装层级",
                        "code": "biz/package/delete",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7109441266676404224",
        "title": "配置管理",
        "code": "",
        "icon": "form-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109441266676404224",
        "component": "",
        "subs": [
            {
                "name": "bizCheck",
                "title": "检测报告模板",
                "code": "biz/template.check/page",
                "icon": "form-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/checkTemplate",
                "component": "biz/configue/config/check/index",
                "subs": [
                    {
                        "title": "新增检测报告模板",
                        "code": "biz/template.check/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑检测报告模板",
                        "code": "biz/template.check/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "检测报告模板列表",
                        "code": "biz/template.check/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "检测报告模板删除",
                        "code": "biz/template.check/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改检测报告模板状态",
                        "code": "biz/template.check/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "检测报告模板详情",
                        "code": "biz/template.check/detail",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizInfoTemplate",
                "title": "溯源模板管理",
                "code": "biz/template.info/page",
                "icon": "retweet-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/infoTemplate",
                "component": "biz/configue/config/info/index",
                "subs": [
                    {
                        "title": "新增溯源模板管理",
                        "code": "biz/template.info/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑溯源模板管理",
                        "code": "biz/template.info/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "溯源模板删除",
                        "code": "biz/template.info/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "溯源模板列表",
                        "code": "biz/template.info/list",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改溯源模板状态",
                        "code": "biz/template.info/state",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizApplet",
                "title": "小程序模板管理",
                "code": "biz/appletTemplate/page",
                "icon": "fullscreen-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/config/applet",
                "component": "biz/configue/config/applet/index"
            },
            {
                "name": "bizConfigTemplateInfo",
                "title": "溯源模板配置",
                "code": "biz/infoTemplate/list",
                "icon": "border-outer-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/configue/InfoConfig",
                "component": "biz/configue/config/info/info"
            },
            {
                "name": "bizAppletForm",
                "title": "新增编辑小程序模板",
                "code": "biz/applet/save",
                "icon": "code-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/config/applet/form",
                "component": "biz/configue/config/applet/form"
            },
            {
                "name": "bizCertificate",
                "title": "溯源链证书配置",
                "code": "biz/template.blockchain/detail",
                "icon": "book-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/configue/certificate",
                "component": "biz/configue/config/certificate/index",
                "subs": [
                    {
                        "title": "添加区块链模板",
                        "code": "biz/template.blockchain/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更新区块链模板",
                        "code": "biz/template.blockchain/edit",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            }
        ]
    },
    {
        "group_name": "配置",
        "name": "7109441358762348544",
        "title": "账号管理",
        "code": "",
        "icon": "team-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7109441358762348544",
        "component": "",
        "subs": [
            {
                "name": "bizAccountMember",
                "title": "成员管理",
                "code": "biz/account/page",
                "icon": "user-add-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/account/member",
                "component": "biz/configue/newAccount/member/index",
                "subs": [
                    {
                        "title": "新增成员",
                        "code": "biz/account/add",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "编辑成员",
                        "code": "biz/account/edit",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "成员详情",
                        "code": "biz/account/detail",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "更改状态",
                        "code": "biz/account/state",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "成员删除",
                        "code": "biz/account/delete",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "成员列表",
                        "code": "biz/account/list",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "bizAccountAuthorization",
                "title": "权限管理",
                "code": "biz/authorization/page",
                "icon": "apartment-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/account/authorization",
                "component": "biz/configue/newAccount/member/authorization",
                "subs": [
                    {
                        "title": "分配权限",
                        "code": "biz/authorization/auth",
                        "category": "BUTTON",
                        "module": 10009
                    },
                    {
                        "title": "角色列表",
                        "code": "biz/authorization/getRoleList",
                        "category": "BUTTON",
                        "module": 10009
                    }
                ]
            },
            {
                "name": "configDevelop",
                "title": "开发者管理",
                "code": "biz/develop/page",
                "icon": "console-sql-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/account/api",
                "component": "biz/configue/newAccount/develop/index"
            }
        ]
    },
    {
        "group_name": "其他",
        "name": "7112447960545234944",
        "title": "上链日志",
        "code": "",
        "icon": "api-outlined",
        "category": "MENU",
        "module": 10009,
        "menu_type": "CATALOG",
        "path": "/7112447960545234944",
        "component": "",
        "subs": [
            {
                "name": "bizPesticideBlockChain",
                "title": "上链记录",
                "code": "biz/BlockChain/page",
                "icon": "api-outlined",
                "category": "MENU",
                "module": 10009,
                "menu_type": "MENU",
                "path": "/biz/pesticide/BlockChain",
                "component": "biz/blockChain/index"
            }
        ]
    }
]';
            PhinxExtend::write2menu(json_decode($json, true) ?: []);
        }
    }
}