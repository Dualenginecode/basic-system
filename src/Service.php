<?php

declare(strict_types=1);

namespace app\sys;

use app\sys\command\Auto;
use app\sys\command\Clear;
use app\sys\command\Fans;
use think\admin\extend\CodeExtend;
use think\admin\Plugin;
use think\admin\service\AutoService;
use think\admin\service\PaymentService;
use think\Request;

/**
 * 插件服务注册
 * @class Service
 * @package app\sys
 */
class Service extends Plugin
{
    /**
     * 定义插件名称
     * @var string
     */
    protected $appName = '基础系统';

    /**
     * 定义安装包名
     * @var string
     */
    protected $package = 'truthful-team/basic-system';

    /**
     * 注册组件服务
     * @return void
     */
    public function register(): void
    {
        // 注册模块指令
        $this->commands([Fans::class, Auto::class, Clear::class]);

        // 注册粉丝关注事件
        $this->app->event->listen('WechatFansSubscribe', static function ($openid) {
            AutoService::register($openid);
        });

        // 注册支付通知路由
        $this->app->route->any('/plugin-wxpay-notify/:vars', static function (Request $request) {
            try {
                $data = json_decode(CodeExtend::deSafe64($request->param('vars')), true);
                return PaymentService::notify($data);
            } catch (\Exception|\Error $exception) {
                return "Error: {$exception->getMessage()}";
            }
        });
    }
    /**
     * 定义插件中心菜单
     * @return array
     */
    public static function menu(): array
    {
        return [];
    }
}