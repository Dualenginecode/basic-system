<?php
// +----------------------------------------------------------------------
// | 双擎码防伪溯源系统 [ 致力于通过产品和服务，帮助商家开启标准化全链路商品溯源 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009~2024 https://www.sqm.la All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 双擎科技 <yjw@sqm.la>
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace app\sys\model;

use think\admin\Model;
use think\admin\model\SysDict;

/**
 * 通用标工艺单表模型
 * Class SysLabelProcess
 * @package think\admin\model
 */
class SysLabelProcess extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;
    /**
     * 隐藏字段
     * @var string[]
     */
    protected $hidden = [
        'update_time', 'updated_by', 'sort'
    ];

    /**
     * 定义需要转换的字段
     * @var string[]
     */
    protected $fieldsToConvert = [
        'label_type',
        'labeling_methods',
        'void_type',
        'label_exit_direction',
        'techniques',
        'process_requirements',
        'material_requirements'
    ];

    /**
     * 转换字段值的方法
     * @return void
     */
    public function convertFields()
    {
        foreach ($this->fieldsToConvert as $field) {
            if (isset($this->$field)) {
                // 获取转换后的值
                $convertedValue = SysDict::getLabelsByValues($this->$field,'BIZ');

                // 检查返回值是否为数组，并处理
                if (is_array($convertedValue)) {
                    // 如果是数组，可以选择将其转换为字符串，使用逗号连接
                    $this->$field = implode(', ', $convertedValue);
                } else {
                    // 如果不是数组，直接赋值
                    $this->$field = $convertedValue;
                }
            }
        }
    }


    /**
     * @param $id
     * @return SysLabelProcess|array|mixed|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }
}