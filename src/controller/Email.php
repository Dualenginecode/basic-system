<?php

declare(strict_types=1);

namespace app\sys\controller;

use think\admin\Controller;
use think\admin\model\SysEmail;
use think\admin\service\AlibabaEmailService;
use think\admin\service\LocalEmailService;
use think\admin\service\TencentEmailService;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 邮件发送接口
 * Class Email
 * @package app\sys\controller
 */
class Email extends Controller
{
    /**
     * 邮件发送分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysEmail::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('parent_id,status')->dateBetween('create_time');
        $query->like('name,code,category');
        $query->dataScope('created_by');
        $lists = $query->order('id ASC')->page();
    }

    /**
     * 获取一条邮件发送详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        $this->success('操作成功', SysEmail::detail($this->request->param('id')));
    }

    /**
     * 本地发送文本类邮件
     * @auth true
     * @return void
     */
    public function sendLocalTxt()
    {
        $data = $this->_vali([
            'receiveAccounts.require' => '接收邮箱不能为空！',
            'subject.require' => '邮件主题不能为空！',
            'content.require' => '邮件正文不能为空！',
        ]);
        $to = explode(',', $data['receiveAccounts']);
        $toEmail = LocalEmailService::instance()->sendEmail($to, $data['subject'], $data['content']);
        if ($toEmail) {
            $this->success('邮件发送成功');
        } else {
            $this->error('邮件发送失败');
        }
    }

    /**
     * 本地发送HTML类邮件
     * @auth true
     * @return void
     */
    public function sendLocalHtml()
    {
        $data = $this->_vali([
            'receiveAccounts.require' => '接收邮箱不能为空！',
            'subject.require' => '邮件主题不能为空！',
            'content.require' => '邮件正文不能为空！',
        ]);
        $to = explode(',', $data['receiveAccounts']);
        $toEmail = LocalEmailService::instance()->sendEmail($to, $data['subject'], $data['content']);
        if ($toEmail) {
            $this->success('邮件发送成功');
        } else {
            $this->error('邮件发送失败');
        }
    }

    /**
     * 阿里云发送文本类邮件
     * @auth true
     * @return void
     */
    public function sendAliyunTxt()
    {
        $data = $this->_vali([
            'receiveAccounts.require' => '接收邮箱不能为空！',
            'sendAccount.require' => '发送邮箱不能为空',
            'sendUser.require' => '发送人昵称不能为空',
            'subject.require' => '邮件主题不能为空！',
            'content.require' => '邮件正文不能为空！'
        ]);
        $toEmail = AlibabaEmailService::instance()->singleSendMail($data['receiveAccounts'], $data['sendAccount'], $data['sendUser'], $data['subject'], $data['content']);
        if ($toEmail) {
            $this->success('邮件发送成功');
        } else {
            $this->error('邮件发送失败');
        }
    }

    /**
     * 阿里云发送HTML类邮件
     * @auth true
     * @return void
     */
    public function sendAliyunHtml()
    {
        $data = $this->_vali([
            'receiveAccounts.require' => '接收邮箱不能为空！',
            'sendAccount.require' => '发送邮箱不能为空',
            'sendUser.require' => '发送人昵称不能为空',
            'subject.require' => '邮件主题不能为空！',
            'content.require' => '邮件正文不能为空！'
        ]);
        $toEmail = AlibabaEmailService::instance()->singleSendMail($data['receiveAccounts'], $data['sendAccount'], $data['sendUser'], $data['subject'], $data['content']);
        if ($toEmail) {
            $this->success('邮件发送成功');
        } else {
            $this->error('邮件发送失败');
        }
    }

    /**
     * 阿里云发送模板类邮件
     * @auth true
     * @return void
     */
    public function sendAliyunTmp()
    {
        $data = $this->_vali([
            'receiveAccounts.require' => '接收邮箱不能为空！',
            'sendAccount.require' => '发送邮箱不能为空',
            'tagName.require' => '标签名称不能为空',
            'templateName.require' => '模板名称不能为空！'
        ]);
        $toEmail = AlibabaEmailService::instance()->batchSendEmail($data['receiveAccounts'], $data['sendAccount'], $data['tagName'], $data['templateName']);
        if ($toEmail) {
            $this->success('邮件发送成功');
        } else {
            $this->error('邮件发送失败');
        }
    }

    /**
     * 腾讯云发送文本类邮件
     * @auth true
     * @return void
     */
    public function sendTencentTxt()
    {
        $data = $this->_vali([
            'receiveAccounts.require' => '接收邮箱不能为空！',
            'sendAccount.require' => '发送邮箱不能为空',
            'sendUser.require' => '发送人昵称不能为空',
            'subject.require' => '邮件主题不能为空！',
            'content.require' => '邮件正文不能为空！'
        ]);
        $toEmail = TencentEmailService::instance()->singleSendMail($data['receiveAccounts'], $data['sendAccount'], $data['sendUser'], $data['subject'], $data['content'], false);
        if ($toEmail) {
            $this->success('邮件发送成功');
        } else {
            $this->error('邮件发送失败');
        }
    }

    /**
     * 腾讯云发送HTML类邮件
     * @auth true
     * @return void
     */
    public function sendTencentHtml()
    {
        $data = $this->_vali([
            'receiveAccounts.require' => '接收邮箱不能为空！',
            'sendAccount.require' => '发送邮箱不能为空',
            'sendUser.require' => '发送人昵称不能为空',
            'subject.require' => '邮件主题不能为空！',
            'content.require' => '邮件正文不能为空！'
        ]);
        $toEmail = TencentEmailService::instance()->singleSendMail($data['receiveAccounts'], $data['sendAccount'], $data['sendUser'], $data['subject'], $data['content'], true);
        if ($toEmail) {
            $this->success('邮件发送成功');
        } else {
            $this->error('邮件发送失败');
        }
    }

    /**
     * 腾讯云发送模板类邮件
     * @auth true
     * @return void
     */
    public function sendTencentTmp()
    {
        $data = $this->_vali([
            'receiveAccounts.require' => '接收邮箱不能为空！',
            'sendAccount.require' => '发送邮箱不能为空',
            'subject.require' => '邮件主题',
            'templateName.require' => '模板名称不能为空！',
            'templateParam.default' => ''
        ]);
        $toEmail = TencentEmailService::instance()->batchSendEmail($data['receiveAccounts'], $data['sendAccount'], $data['subject'], $data['templateName'], $data['templateParam']);
        if ($toEmail) {
            $this->success('邮件发送成功');
        } else {
            $this->error('邮件发送失败');
        }
    }

    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('邮件发送', '邮件发送删除成功');
        SysEmail::mSave(['is_deleted' => 1]);
    }
}