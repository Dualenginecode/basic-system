<?php

declare(strict_types=1);

namespace app\sys\controller;

use think\admin\Controller;
use think\admin\model\SysSpa;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;

/**
 * 单页接口
 * Class Spa
 * @package app\sys\controller
 */
class Spa extends Controller
{
    /**
     * 单页分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysSpa::mQuery()->with('Modules');
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('status')->dateBetween('create_time');
        $query->like('title,menu_type');
        $query->dataScope('created_by');
        $lists = $query->order('sort ASC,id ASC')->page();
    }

    /**
     * 获取一条单页详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('单页管理', '单页详情获取成功');
        $this->success('操作成功', SysSpa::detail($this->request->param('id')));
    }

    /**
     * 添加单页
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('单页管理', '单页添加成功');
        SysSpa::mForm('form');
    }

    /**
     * 更新单页
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('单页管理', '单页更新成功');
        SysSpa::mForm('form');
    }

    /**
     * 修改单页状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('单页管理', '单页状态修改成功');
        SysSpa::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('单页管理', '单页删除成功');
        SysSpa::mSave(['is_deleted' => 1]);
    }

    /**
     * 添加表单处理
     * @param array $data
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    protected function _form_filter(array &$data)
    {
        $data['isTop'] === '0' ? $data['isHidden'] = 1 : $data['isHidden'] = 0;
    }
}