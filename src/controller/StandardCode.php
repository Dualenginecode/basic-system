<?php

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\service\syncService;
use app\sys\service\SystemMenuService;
use Exception;
use think\admin\Controller;
use think\admin\model\SysStandardCode;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\exception\HttpResponseException;

/**
 * 标准代码接口
 * Class StandardCode
 * @package app\sys\controller
 */
class StandardCode extends Controller
{
    /**
     * 标准代码分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysStandardCode::mQuery();
        $query->where(['is_deleted' => 0]);
        $query->where(['parent_id' => $this->request->param('parentId', 0)]);
        // 数据列表搜索过滤
        $query->equal('status')->dateBetween('create_time');
        $query->like('cn_value,en_value,group_name,source,desc');
        $lists = $query->order('sort ASC,id ASC')->page();
    }

    /**
     * 标准代码列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function list()
    {
        $this->success('获取成功！', SysStandardCode::list(['group_name' => $this->request->param('groupName')]));
    }

    /**
     * 获取所有标准代码
     * @login true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function all()
    {
        $config = sysvar('sqm-sys-standard-code');
        if (empty($config)) {
            $data = SysStandardCode::mk()->where(['is_deleted' => 0])->field('id,parent_id,code,group_name,cn_value,en_value')->select()->toArray();
            $config = SystemMenuService::instance()->toTree($data);
            sysvar('sqm-sys-standard-code', $config);
        }

        $this->success('获取成功！', $config);
    }

    /**
     * 标准代码新增
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('标准代码管理', '标准代码创建成功');
        SysStandardCode::mForm('form');
    }

    /**
     * 标准代码编辑
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('标准代码管理', '标准代码更新成功');
        SysStandardCode::mForm('form');
    }

    /**
     * 同步标准代码
     * @auth true
     * @return void
     */
    public function sync()
    {
        try {
            $result = syncService::syncData('sync/standard');
            if (empty($result['code'])) $this->error($result['info']);
            set_time_limit(100);
            foreach ($result['data'] as $vo) SysStandardCode::mUpdate(['standard_id' => $vo['standard_id'], 'parent_id' => $vo['parent_id'], 'group_name' => $vo['group_name'], 'source' => $vo['source'], 'code' => $vo['code'], 'en_value' => $vo['en_value'], 'cn_value' => $vo['cn_value'], 'desc' => $vo['desc'], 'ext_json' => $vo['ext_json'], 'is_deleted' => 0], 'standard_id');
            $this->success('同步标准代码成功！');
        } catch (HttpResponseException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            $this->error('同步标准代码数据失败！');
        }
    }

    /**
     * 标准代码删除
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('标准代码管理', '标准代码删除成功');
        SysStandardCode::mSave(['is_deleted' => 1]);
    }

    /**
     * 获取标准代码详情
     * auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('标准代码管理', '标准代码获取成功');
        $this->success('操作成功', SysStandardCode::detail($this->request->param('id')));
    }

    /**
     * 获取标准代码分组
     * @login true
     * @return void
     */
    public function group()
    {
        $data = SysStandardCode::mk()->where(['is_deleted' => 0])->select()->toArray();
        if (empty($data)) $this->error('暂无数据，请先同步');
        $dataTree = SystemMenuService::instance()->toTree($data);
        $this->success('操作成功', $dataTree);
    }

    /**
     * 添加表单处理
     * @param array $data
     * @return void
     */
    protected function _form_filter(array &$data)
    {
        if ($data['parentId'] === 0) {
            $data['groupName'] = 'TOP';
        } else {
            if (empty($data['groupName'])) $data['groupName'] = $this->_getGroupName($data['parentId']);
        }
    }

    /**
     * 获取顶级的组名
     * @param $id
     * @return mixed
     */
    public function _getGroupName($id)
    {
        return SysStandardCode::mk()->where(['id' => $id])->value('code');
    }
}