<?php

declare(strict_types=1);

namespace app\sys\controller;

use think\admin\Controller;
use think\admin\model\SysRobot;



/**
 * 机器人接口
 * Class Robot
 * @package app\sys\controller
 */
class Robot extends Controller
{
    /**
     * 机器人分页列表
     * @login true
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function page()
    {
        $query = SysRobot::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('merchant_id,platform,status,user_id')->dateBetween('created_at');
        $query->like('robot_name');
        $query->dataScope('created_by');
        $lists = $query->order('id ASC')->page();
    }

    /**
     * 列表数据处理
     * @param array $data
     * @return void
     * @throws Exception
     */
    protected function _page_filter(array &$data)
    {
        foreach ($data as &$vo) {
            $vo['project'] = $vo['project'] ? explode(',', $vo['project']) : [];
        }
    }

    /**
     * 获取多条机器人信息
     * @login true
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list()
    {
        $query = SysRobot::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('merchant_id,platform,status,user_id')->dateBetween('created_at');
        $query->like('robot_name');
        //$query->dataScope('created_by');
        $lists = $query->order('id ASC')->page(false,false);
        sysoplog('机器人管理', '机器人列表获取成功');
        $this->success('数据获取成功', $lists['list']);
    }

    /**
     * 获取一条机器人的URL
     * @login true
     * @return void
     */
    public function getWebhookUrl()
    {
        $webhook = SysRobot::detail($this->request->param('userId'),$this->request->param('platform'));
        sysoplog('机器人管理', '机器人获取成功');
        $this->success('操作成功', $webhook);
    }

    /**
     * 添加机器人
     * @login true
     * @return void
     */
    public function add()
    {
        sysoplog('机器人管理', '机器人保存成功');
        SysRobot::mForm('form');
    }

    /**
     * 更新机器人
     * @login true
     * @return void
     */
    public function edit()
    {
        sysoplog('机器人管理', '机器人更新成功');
        SysRobot::mForm('form');
    }


    /**
     * 修改机器人状态
     * @login true
     * @return void
     */
    public function state()
    {
        sysoplog('机器人管理', '机器人状态更改成功');
        SysRobot::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 移到回收站
     * @login true
     * @return void
     */
    public function delete()
    {
        sysoplog('机器人管理', '机器人删除成功');
        SysRobot::mSave(['is_deleted' => 1]);
    }

    /**
     * 设置默认机器人
     * @return void
     */
    public function setDefault()
    {
        $data = $this->_vali([
            'merchantId.require' => '商户标识不能为空！',
            'userId.require'     => '用户ID不能为空！',
            'robotId.require'    => '机器人ID不能为空！'
        ]);
        // 先将所有符合条件的记录的 is_default 设置为 0
        SysRobot::mk()
            ->where(['user_id' => $data['userId'], 'merchant_id' => $data['merchantId']])
            ->update(['is_default' => 0]);

        // 将指定的记录 is_default 设置为 1
        $result = SysRobot::mk()
            ->where(['id' => $data['robotId'], 'user_id' => $data['userId'], 'merchant_id' => $data['merchantId']])
            ->update(['is_default' => 1]);

        if($result){
            $this->success('默认机器人设置成功');
        }else{
            $this->error('设置失败');
        }
    }


}