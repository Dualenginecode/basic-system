<?php
// +----------------------------------------------------------------------
// | 双擎码防伪溯源系统 [ 致力于通过产品和服务，帮助商家开启标准化全链路商品溯源 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009~2024 https://www.sqm.la All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 双擎科技 <yjw@sqm.la>
// +----------------------------------------------------------------------

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\model\SysCodeRule;
use think\admin\Controller;
use think\admin\extend\CodeExtend;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;

/**
 * 通用码规则接口
 * Class GeneralizedRule
 * @package app\biz\controller
 */
class GeneralizedRule extends Controller
{
    /**
     * 获取多条通用码规则信息
     * @login true
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function list()
    {
        $query = SysCodeRule::mQuery();
        $query->where(['is_deleted' => 0]);
        // 数据列表搜索过滤
        $query->equal('status')->dateBetween('create_time');
        $query->like('name,code_rule_status');
        $query->field('id,rule_id,code_rule_name,code_rule,code_demo,code_type,code_rule_status,code_rule_type,info_type');
        $lists = $query->order('id ASC')->page(false, false);
        sysoplog('通用码规则管理', '通用码规则列表获取成功');
        $this->success('数据获取成功', $lists['list']);
    }

    /**
     * 通用码规则分页
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysCodeRule::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('status')->dateBetween('create_time');
        $query->like('name');
        // $query->dataScope('created_by');
        $query->field('id,rule_id,code_rule_name,code_rule,code_demo,code_type,code_rule_status,code_rule_type,create_time');
        $lists = $query->order('id DESC')->page();
        $this->success('数据获取成功', $lists);
    }

    /**
     * 获取一条通用码规则详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('通用码规则管理', '通用码规则获取成功');
        $this->success('操作成功', SysCodeRule::detail($this->request->param('id')));
    }

    /**
     * 添加通用码规则
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('通用码规则管理', '通用码规则保存成功');
        SysCodeRule::mForm('form');
    }

    /**
     * 更新通用码规则
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('通用码规则管理', '通用码规则更新成功');
        SysCodeRule::mForm('form');
    }

    /**
     * 修改通用码规则状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('通用码规则管理', '通用码规则状态更改成功');
        SysCodeRule::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('通用码规则管理', '通用码规则删除成功');
        SysCodeRule::mSave(['is_deleted' => 1]);
    }

    /**
     * 通用码规则审核
     * @auth true
     * @return void
     */
    public function toExamine()
    {
        sysoplog('通用码规则管理', '通用码规则审核');
        SysCodeRule::mSave(['code_rule_status' => $this->request->post('codeRuleStatus')]);
    }

    /**
     * 添加表单处理
     * @param array $data
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    protected function _add_form_filter(array &$data)
    {
        $data['ruleId'] = isset($data['ruleId']) && $data['ruleId'] ? $data['ruleId'] : CodeExtend::snId();
        if (isset($data['cardList']) && $data['cardList']) {
            $data['codeRuleName'] = $data['cardList']['codeRuleName'];
            $data['codeRule'] = $data['cardList']['codeRule'];
            $data['codeDemo'] = $data['cardList']['codeDemo'];
            $data['codeType'] = $data['cardList']['codeType'];
            $data['codeRuleStatus'] = $data['cardList']['codeRuleStatus'];//IN_REVIEW
            $data['codeRuleType'] = $data['cardList']['codeRuleType'];
        }
    }

    /**
     * 添加表单处理
     * @param array $data
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    protected function _edit_form_filter(array &$data)
    {
        $data['ruleId'] = isset($data['ruleId']) && $data['ruleId'] ? $data['ruleId'] : CodeExtend::snId();
        if (isset($data['cardList']) && $data['cardList']) {
            $data['codeRuleName'] = $data['cardList']['codeRuleName'];
            $data['codeRule'] = $data['cardList']['codeRule'];
            $data['codeDemo'] = $data['cardList']['codeDemo'];
            $data['codeType'] = $data['cardList']['codeType'];
            $data['codeRuleType'] = $data['cardList']['codeRuleType'];
        }
        $data['codeRuleStatus'] = 'IN_REVIEW';
    }

}