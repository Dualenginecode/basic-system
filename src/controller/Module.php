<?php

declare(strict_types=1);

namespace app\sys\controller;

use think\admin\Controller;
use think\admin\extend\CodeExtend;
use think\admin\model\SysModule;
use app\sys\service\SystemMenuService;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;

/**
 * 子行业模块接口
 * Class Module
 * @package app\sys\controller
 */
class Module extends Controller
{
    /**
     * 模块分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysModule::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('status')->dateBetween('create_time');
        $query->like('title,code');
        $query->dataScope('created_by');
        $lists = $query->order('sort asc,id asc')->page();
    }

    /**
     * 获取可分配行业模块列表
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function listToMerchant()
    {
        sysoplog('模块管理', '可分配模块列表获取成功');
        $this->success('操作成功', SysModule::mk()->where(['code' => 'BIZ'])->where(['is_deleted' => 0])->field('id as value,title as label')->select()->toArray());
    }

    /**
     * 获取一条行业模块详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('行业模块管理', '行业模块获取成功');
        $this->success('操作成功', SysModule::detail($this->request->param('id')));
    }

    /**
     * 添加模块
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('行业模块管理', '行业模块保存成功');
        SysModule::mForm('form');
    }

    /**
     * 更新行业模块
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('行业模块管理', '行业模块更新成功');
        SysModule::mForm('form');
    }

    /**
     * 修改行业模块状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('行业模块管理', '行业模块状态更改成功');
        SysModule::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('行业模块管理', '行业模块删除成功');
        SysModule::mSave(['is_deleted' => 1]);
    }

    /**
     * 模块树获取
     * @login true
     * @return void
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws \think\db\exception\DbException
     */
    public function tree()
    {
        $modules = SysModule::mk()
            ->where(['is_deleted' => 0, 'status' => 0])
            ->order('id asc')
            ->field('*,parent_id as parentId,title as name')
            ->select()
            ->toArray();
        $lists = SystemMenuService::instance()->toTree($modules);
        sysoplog('模块管理', '模块树获取成功');
        $this->success('数据获取成功', $lists);
    }

    /**
     * 添加表单处理
     * @param array $data
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    protected function _form_filter(array &$data)
    {
        $data['moduleId'] = isset($data['moduleId']) && $data['moduleId'] ? $data['moduleId'] : CodeExtend::snId('md');
    }
}