<?php

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\service\syncService;
use app\sys\service\SystemMenuService;
use Exception;
use think\admin\Controller;
use think\admin\model\SysRegion;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\exception\HttpResponseException;

/**
 * 区域接口
 * Class Area
 * @package app\sys\controller
 */
class Area extends Controller
{
    /**
     * 区域分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysRegion::mQuery();

        // 数据列表搜索过滤
        $query->equal('pid,status')->dateBetween('create_time')->like('name');
        $lists = $query->order('id ASC')->page();
    }

    /**
     * 获取分类选择树
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function tree()
    {
        $trees = SysRegion::mk()
            ->where(['status' => 0])
            ->field('id,pid,name,level,code')
            ->order('id asc')
            ->select()
            ->toArray();
        $lists = SystemMenuService::instance()->toTree($trees, 0, 'id', 'pid');
        $this->success('数据获取成功', $lists);
    }

    /**
     * 获取一条地区详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('地区管理', '地区详情获取成功');
        $this->success('操作成功', SysRegion::mk()->where(['id' => $this->request->param('id')])->where(['status' => 0])->findOrEmpty());
    }

    /**
     * 添加区域
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('区域管理', '区域新增成功');
        SysRegion::mForm('form');
    }

    /**
     * 更新区域
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('区域管理', '区域更新成功');
        SysRegion::mForm('form');
    }

    /**
     * 修改区域状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('区域管理', '区域状态更新成功');
        SysRegion::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 同步区域
     * @auth true
     * @return void
     */
    public function sync()
    {
        try {
            set_time_limit(180);
            $result = syncService::syncAllData('sync/gpc',100);
            $batchSize = 500; // 你可以根据你的数据库性能调整这个值
            $chunks = array_chunk($result, $batchSize);
            foreach ($chunks as $chunk) {
                SysRegion::mk()->insertAll($chunk);
            }
            $this->success('同步区域成功！');
        } catch (HttpResponseException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            $this->error('同步区域数据失败！');
        }
    }

    /**
     * 删除区域
     * @auth true
     * @return void
     */
    public function remove()
    {
        sysoplog('区域管理', '区域删除成功');
        SysRegion::mSave(['is_deleted' => 1]);
    }
}