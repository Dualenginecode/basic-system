<?php

declare(strict_types=1);

namespace app\sys\controller;

use think\admin\Controller;
use think\admin\model\SysMenu;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 菜单按钮接口
 * Class Button
 * @package app\sys\controller
 */
class Button extends Controller
{
    /**
     * 菜单按钮分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysMenu::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('parent_id,status')->dateBetween('create_time');
        $query->like('name,code,category');
        $lists = $query->order('sort ASC,id ASC')->page();
    }

    /**
     * 获取一条菜单按钮详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('菜单按钮', '菜单按钮获取成功');
        $this->success('操作成功', SysMenu::detail($this->request->param('id')));
    }

    /**
     * 添加菜单按钮
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('菜单按钮', '菜单按钮添加成功');
        SysMenu::mForm('form');
    }

    /**
     * 更新菜单按钮
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('菜单按钮', '菜单按钮修改成功');
        SysMenu::mForm('form');
    }

    /**
     * 修改菜单按钮状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('菜单按钮', '菜单按钮状态更新成功');
        SysMenu::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('菜单按钮', '菜单按钮删除成功');
        SysMenu::mSave(['is_deleted' => 1]);
    }
}