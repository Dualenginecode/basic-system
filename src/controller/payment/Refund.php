<?php

declare (strict_types=1);

namespace app\sys\controller\payment;

use think\admin\model\SysWechatFans;
use think\admin\model\SysWechatPaymentRecord;
use think\admin\model\SysWechatPaymentRefund;
use think\admin\Controller;
use think\admin\helper\QueryHelper;
use think\db\Query;

/**
 * 支付退款管理
 * @class Refund
 * @package app\wechat\controller
 */
class Refund extends Controller
{
    /**
     * 支付退款管理
     * @auth true
     * @menu true
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        SysWechatPaymentRefund::mQuery()->antTable(function () {
            $this->title = '支付退款管理';
        }, function (QueryHelper $query) {
            $query->like('code|refund_trade#refund')->withoutField('refund_notify');
            $query->with(['record' => function (Query $query) {
                $query->withoutField('payment_notify');
            }]);
            if (($this->get['order'] ?? '') . ($this->get['nickname'] ?? '') . ($this->get['payment'] ?? '') . ($this->get['refund'] ?? '') !== '') {
                $db1 = SysWechatFans::mQuery()->field('openid')->like('openid|nickname#nickname')->db();
                $db2 = SysWechatPaymentRecord::mQuery()->like('order_code|order_name#order,code|payment_trade#payment');
                $db2->whereRaw("openid in {$db1->buildSql()}");
                $query->whereRaw("record_code in {$db2->field('code')->db()->buildSql()}");
            }
        });
    }
}