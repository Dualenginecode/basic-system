<?php

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\service\LoginService;
use think\admin\Controller;
use think\admin\Exception;
use think\admin\extend\CodeExtend;
use think\exception\HttpResponseException;


/**
 * 三方用户登录注册接口
 * Class Third
 * @package app\auth\controller
 */
class Third extends Controller
{
    /**
     * 生成第三方登录二维码
     * @return void
     */
    public function qrcode()
    {
        try {
            $data = $this->_vali([
                'type.require' => '登录类型必选',
                'path.default' => 'pages/scan/index',
                'describe.default' => '扫码登录',
                'scene.default' => CodeExtend::uniqidNumber(20)
            ]);
            $result = LoginService::qrcode($data['type'], $data['path'], $data['scene']);
            $this->success('生成二维码成功！', $result);
        } catch (HttpResponseException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    /**
     * 检查是否已扫码
     * @return void
     */
    public function checkScan()
    {
        $this->success('扫码', LoginService::query($this->request->param('code')));
    }

    /**
     * 授权登录(扫码)
     * @login true
     */
    public function getAuth()
    {
        $data = $this->_vali([
            'code.require' => '授权号不能为空'
        ]);
        $cacheCode = $this->app->cache->get('code');
        $this->app->cache->delete('code');
        $this->success('扫码成功', $cacheCode === $data['code'] && LoginService::oauthScan(LoginService::gauth($data['code'])));
    }

    /**
     * 接口服务初始化
     * @return void
     * @throws Exception
     */
    protected function initialize()
    {
        $this->config = [
            'appid' => sysconfig('WECHAT_MINI', 'SQM_WECHAT_WXAPP_APPID'),
            'appsecret' => sysconfig('WECHAT_MINI', 'SQM_WECHAT_WXAPP_APPKEY'),
            'cache_path' => $this->app->getRootPath() . 'runtime' . DIRECTORY_SEPARATOR . 'wechat'
        ];
        $this->aliConfig = [
            'appid' => sysconfig('ALIPAY_MINI', 'SQM_ALIPAY_MINI_APPID'),
            'private_key' => sysconfig('ALIPAY_MINI', 'SQM_ALIPAY_MINI_PRIVATE_KEY'),
            'public_key' => sysconfig('ALIPAY_MINI', 'SQM_ALIPAY_MINI_PUBLIC_KEY'),
        ];
    }
}