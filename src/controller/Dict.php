<?php

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\service\SystemMenuService;
use think\admin\Controller;
use think\admin\model\SysDict;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;

/**
 * 字典接口
 * Class Dict
 * @package app\dev\controller
 */
class Dict extends Controller
{
    /**
     * 字典分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysDict::mQuery();
        $query->where(['is_deleted' => 0]);
        // 数据列表搜索过滤
        $query->equal('parent_id,status')->dateBetween('create_time');
        $query->like('dict_label,category');
        $lists = $query->order('sort ASC,id ASC')->page();
    }

    /**
     * 字典列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function list()
    {
        $this->success('获取成功！', SysDict::mk()->where(['is_deleted' => 0])->select()->toArray());
    }

    /**
     * 字典新增
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('字典管理', '字典创建成功');
        SysDict::mForm('form');
    }

    /**
     * 字典编辑
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('字典管理', '字典更新成功');
        SysDict::mForm('form');
    }

    /**
     * 字典删除
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('字典管理', '字典删除成功');
        SysDict::mSave(['is_deleted' => 1]);
    }

    /**
     * 获取字典详情
     * auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('字典管理', '字典获取成功');
        $this->success('操作成功', SysDict::detail($this->request->param('id')));
    }

    /**
     * 字典树
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function tree()
    {
        $category = $this->request->param('category');
        if ($category != '') {
            $map = [['category', '=', $category]];
            $varName = 'sqm-sys-dict-tree-'.$category;
        } else {
            $map = 1;
            $varName = 'sqm-sys-dict-tree-';
        }
        $dictTree = sysvar($varName);

        if (empty($dict)) {
            $dict = SysDict::mk()
                ->where(['is_deleted' => 0, 'status' => 0])
                ->where($map)
                ->order('sort ASC')
                ->field('id,parent_id,dict_label,dict_value,category')
                ->select()
                ->toArray();
            $dictTree = SystemMenuService::instance()->toTree($dict);
            sysvar($varName, $dictTree);
        }

        $this->success('字典树获取成功！', $dictTree);
    }

    /**
     * 添加表单处理
     * @param array $data
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    protected function _form_filter(array &$data)
    {
        // 检查是否有 dict_value 字段
        if (isset($data['dict_value'])) {
            $id = $data['id'] ?? null; // 获取 ID，如果没有则为 null
            if (!SysDict::isUniqueDictValue($data['dict_value'], $id)) {
                $this->error('字典值已存在，请更改');
            }
        }
    }

    /**
     * 重写字典数据
     * @auth true
     */
    public function reset()
    {
        $this->_queue('重写字典数据', "xadmin:sysdict");
    }
}