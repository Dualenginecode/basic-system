<?php
// +----------------------------------------------------------------------
// | 双擎码防伪溯源系统 [ 致力于通过产品和服务，帮助商家开启标准化全链路商品溯源 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009~2024 https://www.sqm.la All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 双擎科技 <yjw@sqm.la>
// +----------------------------------------------------------------------

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\model\SysLabelProcess;
use Exception;
use think\admin\Controller;
use think\admin\extend\CodeExtend;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;

/**
 * 物料标签工艺单接口
 * Class LabelStyle
 * @package app\biz\controller
 */
class LabelStyle extends Controller
{
    /**
     * 获取多条标签工艺单信息
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function list()
    {
        $query = SysLabelProcess::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('process_id,status')->dateBetween('create_time');
        $query->like('name,process_code');
        //$query->dataScope('created_by');
        $lists = $query->order('id DESC')->page(false, false);
        sysoplog('标签工艺单管理', '标签工艺单列表获取成功');
        $this->success('数据获取成功', $lists['list']);
    }

    /**
     * 标签工艺单分页列表
     * @auth true
     * @return void
     * @throws DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws ModelNotFoundException
     */
    public function page()
    {
        $query = SysLabelProcess::mQuery();
        $query->where(['is_deleted' => 0]);

        // 数据列表搜索过滤
        $query->equal('process_id,status')->dateBetween('create_time');
        $query->like('name,process_code');
        //$query->dataScope('created_by');
        $lists = $query->order('id DESC')->page();
    }

    /**
     * 获取一条标签工艺单详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        sysoplog('标签工艺单管理', '标签工艺单获取成功');
        $this->success('操作成功', SysLabelProcess::detail($this->request->param('id')));
    }

    /**
     * 添加标签工艺单
     * @auth true
     * @return void
     */
    public function add()
    {
        sysoplog('标签工艺单管理', '标签工艺单保存成功');
        SysLabelProcess::mForm('form');
    }

    /**
     * 更新标签工艺单
     * @auth true
     * @return void
     */
    public function edit()
    {
        sysoplog('标签工艺单管理', '标签工艺单更新成功');
        SysLabelProcess::mForm('form');
    }

    /**
     * 修改标签工艺单状态
     * @auth true
     * @return void
     */
    public function state()
    {
        sysoplog('标签工艺单管理', '标签工艺单状态更改成功');
        SysLabelProcess::mSave($this->_vali([
            'status.in:0,1' => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 移到回收站
     * @auth true
     * @return void
     */
    public function delete()
    {
        sysoplog('标签工艺单管理', '标签工艺单删除成功');
        SysLabelProcess::mSave(['is_deleted' => 1]);
    }

    /**
     * 列表数据处理
     * @param array $data
     * @throws Exception
     */
    protected function _page_filter(array &$data)
    {
        foreach ($data as &$vo) {
            $vo['ladderPrice'] = $vo['ladderPrice'] ? json_decode(html_entity_decode($vo['ladderPrice'])) : [];
            $vo['techniques'] = $vo['techniques'] ? explode(',', $vo['techniques']) : [];
        }
    }

    /**
     * 添加表单处理
     * @param array $data
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    protected function _form_filter(array &$data)
    {
        $data['processId'] = isset($data['processId']) && $data['processId'] ? $data['processId'] : CodeExtend::snId();
        $data['processCode'] = isset($data['processCode']) && $data['processCode'] ? $data['processCode'] : CodeExtend::uniqidNumber(14, 'PT');
    }

    /**
     * 报价
     * @return void
     */
    public function quotation()
    {
        sysoplog('标签工艺单管理', '标签工艺单报价');
        SysLabelProcess::mSave(['ladder_price' => $this->request->post('ladderPrice')]);
    }

    /**
     * 审核
     * @return void
     */
    public function toExamine()
    {
        sysoplog('标签工艺单管理', '标签工艺单审核');
        SysLabelProcess::mSave(['auth_status' => $this->request->post('authStatus')]);
    }

}