<?php

declare(strict_types=1);

namespace app\sys\controller;

use app\sys\service\NoSqlPageService;
use think\admin\Controller;
use think\facade\Db;

/**
 *数据表接口
 * Class Database
 * @package app\sys\controller
 */
class Database extends Controller
{
    /**
     * 数据表列表
     * @auth true
     * @return void
     */
    public function page()
    {
        $tables = Db::query('show table status');
        // 重组数据
        foreach ($tables as &$table) {
            $table = array_change_key_case($table);
        }
        if ($this->request->param('searchKey')) $tables = searchArrByValue($tables, 'name', trim($this->request->param('searchKey')));
        sysoplog('数据表管理', '查看数据表列表');
        $this->success('数据获取成功', NoSqlPageService::instance()->setPage(intval($this->request->param('page')), intval($this->request->param('pageSize')), $tables));
    }

    /**
     * 数据表详情
     * @auth true
     * @return void
     */
    public function detail()
    {
        $table = $this->request->post('name');
        if (!$table) {
            $this->error('请选择要查看的数据表');
        }
        $detail = array_values(Db::getFields($table));
        sysoplog('数据表管理', '查看数据表详情');
        $this->success('数据获取成功', $detail);
    }

    /**
     * 数据表优化
     * @auth true
     * @return void
     */
    public function optimize()
    {
        $tables = $this->request->post('tables');
        if ($tables) {
            $tables = explode(',', $tables);
        } else {
            $tablesArr = Db::query('show table status');
            $tables = array_column($tablesArr, 'Name');
        }
        foreach ($tables as $table) {
            $this->app->db->query("OPTIMIZE TABLE `{$table}`");
        }
        sysoplog('数据表管理', '数据表优化');
        $this->success('数据表优化成功');
    }

    /**
     * 修复所有数据表
     * @auth true
     * @return void
     */
    public function repair()
    {
        $tables = $this->request->post('tables');
        if ($tables) {
            $tables = explode(',', $tables);
        } else {
            $tablesArr = Db::query('show table status');
            $tables = array_column($tablesArr, 'Name');
        }
        foreach ($tables as $table) {
            $this->app->db->query("REPAIR TABLE `{$table}`");
        }
        sysoplog('数据表管理', '数据表修复');
        $this->success('数据表修复成功');
    }

    /**
     * 清理表碎片
     * @auth true
     * @return void
     */
    public function fragment()
    {
        $tables = $this->request->post('tables');
        if ($tables) {
            $tables = explode(',', $tables);
        } else {
            $tablesArr = Db::query('show table status');
            $tables = array_column($tablesArr, 'Name');
        }
        foreach ($tables as $table) {
            $this->app->db->query("ANALYZE TABLE `{$table}`");
        }
        sysoplog('数据表管理', '数据表碎片清理');
        $this->success('数据表碎片清理成功');
    }

    /**
     * 数据库打包
     * @auth true
     */
    public function packages()
    {
        $this->_queue('数据库打包Migration', "xadmin:package");
    }

    /**
     * 数据库完整备份
     * @auth true
     */
    public function packagesAll()
    {
        $this->_queue('数据库完整备份', "xadmin:package --all");
    }
}